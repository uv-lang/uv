# UV

slowly implementing a language

There is not much here currently but it is working!
Running currently just takes argument 1 as the file from which to read, and outputs the result of the entire file as an expression.
The file-level scope is interpreted as a module, either defined unordered by delimiting with `,` or ordered by delimiting with `;`.
The only intrinsic operations implemented right now are integer addition and multiplication, and if-then-else expressions,
so it would still be quite difficult to do anything non-trivial.
However, with what is present, here is an example with some working features:

```uv
// note: some of these may not typecheck as-is in the future due to linearity requirements

id := x \ x,
const := x \ _ \ x,

mkPair := l \ r \ (l, r),
left := (l, _) \ l,
right := (_, r) \ r,

+ := prim.add_i64,
* := prim.mul_i64,

// builtin addition and multiplication are uncurried
add := x \ y \ x + y,
mul := x \ y \ x * y,
succ := add 1,
double := mul 2,

false := prim.false,
true := prim.true,
not := p \ if p then false else true,
and := (a, b) \ if a then b else false,
or := (a, b) \ if a then true else b,
```

## Current Focuses

- unification
  - type checking
- LLVM codegen
