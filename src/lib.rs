//! Interpreter, and eventually compiler, for the UV programming language.
//!
//! Documentation within this crate strictly regards the compiler and not any
//! insight into the language itself.
//!
//! # General Structure
//!
//! Compilation of UV code goes through roughly the following stages in order:
//! - Lexing from a string to tokens in [`parse`]
//! - Parsing from tokens to token trees (called here as statements) in [`parse`]
//! - Converting from token trees to intermediate representation in [`repr`]
//! - Type-checking and evaluation in [`repr`]
//!
//! In addition, [`log`] covers diagnostic output, and [`ctx`] provides an object
//! overseeing the rest of the compilation, such as for interning strings and
//! reading files.
//!
//! # Notes
//!
//! - Files are assumed to be no longer than [`u32::MAX`] bytes



// a large ASCII art 'UV' comment follows this text

///////////////////////////////////////
//                                   //
//   .           .   .           .   //
//   .           .    .         .    //
//   .           .     .       .     //
//   .           .      .     .      //
//   .           .       .   .       //
//    .         .         . .        //
//      ' . . '            .         //
//                                   //
///////////////////////////////////////



#![feature(allocator_api)]
#![feature(doc_notable_trait)]
#![feature(hash_drain_filter)]
#![feature(hash_set_entry)]
#![feature(if_let_guard)]
#![feature(once_cell)]
#![feature(map_try_insert)]
#![feature(maybe_uninit_uninit_array)]
#![feature(never_type)]
#![feature(new_uninit)]

#![allow(dead_code)]
//#![warn(missing_docs)]

mod cache;
pub mod ctx;
pub mod log;
pub mod parse;
pub mod repr;
