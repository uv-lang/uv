//! Interning, memoizing, and more generally caching of results.



use std::{mem::MaybeUninit, sync::Mutex};

#[derive(Debug, Default)]
struct BlockedCacheData<T, const BLOCK_SIZE: usize> {
	blocks: Vec<Box<[MaybeUninit<T>; BLOCK_SIZE]>>,
	len: usize,
}
/*
impl<T, const BLOCK_SIZE: usize> Drop for BlockedCacheData<T, BLOCK_SIZE> {
	fn drop(&mut self) {
		self.blocks.iter_mut()
			.flat_map(|box_arr| &mut**box_arr)
			.take(self.len)
			.for_each(|elem| unsafe { elem.assume_init_drop() });
	}
}
*/

/// An insert-only collection allocating elements by blocks of a fixed size.
#[derive(Debug, Default)]
pub struct BlockedCache<T, const BLOCK_SIZE: usize> {
	mutex: Mutex<BlockedCacheData<T, BLOCK_SIZE>>,
}

impl<T, const BLOCK_SIZE: usize> BlockedCache<T, BLOCK_SIZE> {
	pub fn new() -> Self {
		debug_assert_ne!(BLOCK_SIZE, 0);
		Self {
			mutex: Mutex::new(BlockedCacheData {
				blocks: Vec::new(),
				len: 0,
			}),
		}
	}

	/// Add a value to the cache and return a mutable reference to it.
	// safety of this allowance is explained as it arises
	// needless to say, this function will return different mutable references each time
	// even given the same argument
	#[allow(clippy::mut_from_ref)]
	pub fn insert(&self, item: T) -> &mut T {
		let mut data = self.mutex.lock().unwrap();
		let loc = data.len % BLOCK_SIZE;
		if loc == 0 {
			data.blocks.push(Box::new(MaybeUninit::uninit_array()));
		}

		// SAFETY:
		// in the only case where `blocks` starts empty (an empty cache), we have just inserted above
		// and `len` is always within range because it is wrapped on `BLOCK_SIZE`
		let elem = unsafe { data.blocks.last_mut().unwrap_unchecked().get_unchecked_mut(loc) };
		let elem = elem.write(item) as *mut _;
		data.len += 1;
		// SAFETY:
		// this is the only way to write to the cache and is never rewinded, so this specific mutable
		// reference will only be returned once and never used internally
		unsafe { &mut*elem }
	}

	pub fn is_empty(&self) -> bool {
		self.mutex.lock().unwrap().len == 0
	}
	pub fn len(&self) -> usize {
		self.mutex.lock().unwrap().len
	}
}



use std::hash::{Hash, Hasher};

/// A pointer bounded by the given lifetime and guaranteed to point to a valid
/// location. Useful for when you would otherwise use references but believe
/// dereferencing should still be made explicit, such as comparing references
/// by location rather than referenced value.
#[derive(Debug)]
#[repr(transparent)]
pub struct LivePtr<'a, T: ?Sized>(&'a T);

impl<'a, T: ?Sized> LivePtr<'a, T> {
	#[inline(always)]
	pub fn new(val: &'a T) -> Self {
		Self(val)
	}

	#[inline(always)]
	pub fn to_ref(self) -> &'a T {
		self.0
	}

	#[inline(always)]
	pub fn to_ptr(self) -> *const T {
		self.0 as *const T
	}
}

impl<'a, T: ?Sized> Clone for LivePtr<'a, T> {
	#[inline(always)]
	fn clone(&self) -> Self {
		LivePtr(self.0)
	}
}

impl<'a, T: ?Sized> Copy for LivePtr<'a, T> {}

impl<'a, T: ?Sized> PartialEq for LivePtr<'a, T> {
	#[inline(always)]
	fn eq(&self, other: &Self) -> bool {
		std::ptr::eq(self.0, other.0)
	}
}
impl<'a, T: ?Sized> Eq for LivePtr<'a, T> {}

impl<'a, T: ?Sized> Hash for LivePtr<'a, T> {
	fn hash<H: Hasher>(&self, state: &mut H) {
		(self.0 as *const T).hash(state)
	}

	fn hash_slice<H: Hasher>(data: &[Self], state: &mut H) {
		// SAFETY:
		// &[&'a T] and &[*const T] have the same representation, and LivePtr<'a, T>
		// is equivalent to &'a T by using repr(transparent).
		unsafe { std::mem::transmute::<&[Self], &[*const T]>(data) }.hash(state)
	}
}
