#![feature(once_cell)]



use std::{
	collections::HashMap,
	time::Instant,
};

use uv::{
	ctx::{
		CompileContext,
		EvaluationContext,
		File,
		Place,
		Span,
	},
	log,
	parse::{
		self,
		RegionInner,
		WrittenStatement,
	},
	repr::{
		BinOp,
		Connective,
		Expr,
		Literal,
		scope::Scope,
		//unify::BoundManager,
	},
};



fn write_header(label: &str) {
	use owo_colors::{OwoColorize, Stream};
	eprintln!();
	eprintln!("{}{}{}",
		"————— ".if_supports_color(Stream::Stderr, |bar| bar.bold()),
		label.if_supports_color(Stream::Stderr, |label| label.bold()),
		" —————".if_supports_color(Stream::Stderr, |bar| bar.bold()),
	);
}



fn main() {
	eprintln!(
"
   .     .  .     .
   .     .   .   .
   .     .    . .
     ' '       '"
	);

	{
		let panic_hook = std::panic::take_hook();
		std::panic::set_hook(Box::new(move |panic_info| {
			use owo_colors::{OwoColorize, Stream};
			println!("{}", "A compiler-internal error was encountered:".if_supports_color(Stream::Stdout, |err| err.bright_red()));
			panic_hook(panic_info)
		}));
	}

	let _: log::Result<()> = CompileContext::new(log::VisualLog::new(), |cctx| {

		let Some(file_name) = std::env::args_os().nth(1) else {
			return Err(log::error(log::frame(String::from("must provide at least one file"))).emit(cctx))
		};
		let Ok(file_name) = file_name.into_string() else {
			return Err(log::error(log::frame(String::from("file name must currently be valid UTF-8"))).emit(cctx))
		};
		/*
		for arg in std::env::args_os().skip(1) {
			if let Ok(string) = arg.into_string() {
				file_name = string;
				break;
			}
		}
		*/

		let call_op = cctx.intern(Box::from(""));

		write_header("read input");
		eprintln!("reading from file `{}`…", file_name);

		let instant_read_start = Instant::now();
		let file = cctx.read_file(file_name).unwrap();
		let instant_read_end = Instant::now();

		// TODO currently VisualLog writes frames without annotations incorrectly
		/*
		log::warn(log::frame(String::from("example warning"))
			.with_annotation(String::from("here"), Span::new(file, 0..4))
			.with_annotation(String::from("and here"), Span::new(file, 2..4))
			.with_note(String::from("this isn't an actual warning"))
			.with_note(String::from("but it's almost worse than one"))
		)
		.with_note(log::frame(String::from("i am going to say some more stuff now"))
		//	.with_annotation(String::from("look at your code. loser."), Span::new(file, 5..18))
			.with_note(String::from("this is not resiliant to me changing the testing file at all"))
		)
		.emit(cctx);
		*/

		let instant_lex_start = Instant::now();
		let (tokens, _file_len): (Vec<_>, _) = cctx.with_file(file, |f| (parse::Tokens::new(cctx, file, &f.string).collect(), f.string.len()));
		let instant_lex_end = Instant::now();
		//write_header("interpreted tokens");
		//eprintln!("{:#?}", tokens);

		let instant_parse_start = Instant::now();

		let (un_precedences, bi_precedences, call_precedence) = {
			use parse::OperatorPrecedence::*;

			// TODO determine precedence without adding here

			let unprec = [
				("->", Hyper1),
				("+", Sign),
				("-", Sign),
				("*", Sign),
				("/", Sign),
				("^", Sign),
				("!", Direct),
			]
			.into_iter()
			.map(|(s, p)| (s.to_string(), p))
			.collect();

			let biprec = [
				(":", Assign),
				("\\", Function),
				("->", Hyper1),
				("+", Hyper1),
				("-", Hyper1),
				("*", Hyper2),
				("/", Hyper2),
				("^", Hyper3),
				(":", Type),
				(".=", Assign),
			]
			.into_iter()
			.map(|(s, p)| (s.to_string(), p))
			.collect();

			(unprec, biprec, Call)
		};

		let stmts = parse::Statements::new(
			cctx,
			file,
			tokens,
			un_precedences, bi_precedences,
			call_precedence, call_op,
		);

		let stmts = stmts.all();
		let instant_parse_end = Instant::now();
		let global = match stmts {
			(RegionInner::Undelimited(None), _) => {
				write_header("no statements, exiting");
				eprintln!();
				return Ok(());
			},
			(RegionInner::Undelimited(Some(stmt)), _) => match *stmt {
				WrittenStatement { head: Some(_), expr: _ } => log::error(log::frame(
					String::from("unsure how to handle single statement at file level"),
				)).emit_fatal(cctx, log::exit_code(20)),
				WrittenStatement { head: None, expr } => {
					expr
				},
			},
			(RegionInner::Delimited(stmts, delim), range) => {
				/*
				write_header("interpreted statements");
				for stmt in stmts.iter() {
					eprintln!("{}{}", stmt, delim);
				}
				*/
				let res = parse::WrittenExpression::BinaryOperation((cctx.intern_cloned(""), 0..0), Box::new((
					parse::WrittenExpression::Ident(cctx.intern_cloned("module"), 0..0),
					parse::WrittenExpression::Region {
						region_inner: RegionInner::Delimited(stmts, delim),
						bracket_kind: parse::BracketKind::CurlyBracket,
						range,
					},
				)));
				/*
				write_header("interpreted AST");
				eprintln!("{}", res);
				*/
				res
			},
		};

		let instant_intermediate_start = Instant::now();

		const PRIM_NAME: &'static str = "prim";
		let prim_place = make_prim(cctx, file, PRIM_NAME);

		let mut supreglobal = HashMap::with_capacity(2);
		supreglobal.insert(cctx.intern(PRIM_NAME.into()), prim_place);
		let supreglobal = Scope::new(supreglobal, None, false, None);

		let global = Expr::from_written(cctx, file, Some(&supreglobal), global);
		//eprintln!("{:#?}", global);

		let instant_intermediate_end = Instant::now();

		//let instant_acyclic_start = Instant::now();
		//cctx.assert_expressions_acyclic().unwrap();
		//let instant_acyclic_end = Instant::now();

		let mut ectx = EvaluationContext::new();
		let ectx = &mut ectx;

		/*
		write_header("synthesized type");

		let mut bounds = BoundManager::new();
		let global_ty = match global.synth_bounds(&mut ectx, None, &mut bounds) {
			None => {
				eprintln!("[failed typechecking]");
				todo!()
			},
			Some(mut ty) => {
				let _ = ty.reduce(&mut ectx);
				ty
			},
		};
		*/
		/*
		let _global_ty = global.synth_bounds(ectx, None, &mut bounds);
		//eprintln!("{:#}", global_ty);

		write_header("type bounds");
		for (i, (expr, soln)) in bounds.unknowns.iter().enumerate() {
			if let Some(e) = expr {
				eprint!("{} : ?{}", e, i);
				if let Some(s) = soln {
					eprint!(" = {}", s);
				}
				eprintln!();
			}
		}
		eprintln!();
		for bound in bounds.bounds.iter() {
			eprintln!("{}", bound);
		}
		eprintln!();

		bounds.solve(ectx).unwrap();
		*/

		/*
		for (i, (expr, soln)) in bounds.unknowns.iter().enumerate() {
			if let Some(e) = expr {
				eprint!("{} : ", e);
			}
			eprint!("?{}", i);
			if let Some(s) = soln {
				eprint!(" = {}", s);
			}
			eprintln!();
		}
		*/

		write_header("normalized value");

		//let global_place = cctx.new_place(Some("<global>"), Some(global), Span::new(file, 0..0));

		//cctx.prepare(QueryKind::SynthType { place: global_place }).unwrap();
		//eprintln!("type: {:#?}", global_place.to_ref().expr.get().unwrap().synth().unwrap());
		//global_place.to_ref().expr.get().unwrap().synth().unwrap();

		//let instant_acyclic_start = Instant::now();
		//cctx.prepare(QueryKind::Evaluate { place: global_place })?;
		//let instant_acyclic_end = Instant::now();

		let instant_evaluate_start = Instant::now();
		//let globalv = global_place.to_ref().expr.get().unwrap().clone().evaluate();
		let mut global = global;
		global.reduce(&mut EvaluationContext::new())?;
		let instant_evaluate_end = Instant::now();

		/*
		match globalv {
			Some(v) => eprintln!("{:#}", v),
			None => {
				eprintln!();
				eprintln!("[could not fully evaluate]");
			},
		}
		*/
		eprintln!("{:#}", global);

		/*
		write_header("created places");
		{
			let places = cctx.places.read().unwrap();
			for (idx, place) in places.iter().enumerate() {
				eprintln!("{} → {:#?}", idx, place);
			}
		}
		*/

		fn write_perf(label: &str, dur: std::time::Duration) {
			use owo_colors::{AnsiColors, OwoColorize, Stream};

			let len = (dur.as_micros() + 1).ilog2() as usize;
			let bar_color = match len {
				0..=4 => AnsiColors::BrightBlue,
				5..=9 => AnsiColors::BrightGreen,
				10..=14 => AnsiColors::BrightYellow,
				15..=19 => AnsiColors::BrightRed,
				_ => AnsiColors::BrightMagenta,
			};

			/*
			eprintln!("{}: {:>10?} │{}",
				label,
				dur,
				"▇".repeat(len).if_supports_color(Stream::Stderr, |bar|
					bar.color(bar_color)
				),
			);
			*/
			eprintln!("{} │{} {:?}",
				label,
				"▇".repeat(len).if_supports_color(Stream::Stderr, |bar|
					bar.color(bar_color)
				),
				dur,
			);
		}

		write_header("performance summary");
		write_perf("      read file", instant_read_end - instant_read_start);
		write_perf("            lex", instant_lex_end - instant_lex_start);
		write_perf(" parsing to AST", instant_parse_end - instant_parse_start);
		write_perf("to intermediate", instant_intermediate_end - instant_intermediate_start);
		//write_perf("  check acyclic", instant_acyclic_end - instant_acyclic_start);
		write_perf("     evaluation", instant_evaluate_end - instant_evaluate_start);
		eprintln!( "╴╴╴╴╴╴╴╴╴╴╴╴╴╴╴╴┤╴╴╴╴╴╴╴╴╴╴╴╴╴╴╴╴╴╴╴╴╴╴");
		write_perf("     end-to-end", instant_evaluate_end - instant_read_start);
		eprintln!();
		eprintln!("places generated: {}", cctx.places.len());
		write_header("have a good one!");
		eprintln!();

		Ok(())
	});
}

fn make_prim<'cctx>(cctx: &'cctx CompileContext<'cctx>, file: File<'cctx>, prim_name: &'cctx str) -> Place<'cctx> {
	let prim_span = Span::new(file, 0..0);
	let mut primitives = HashMap::new();
	primitives.insert(cctx.intern(Box::from("Type")), Expr::Literal(Literal::TypeType));
	primitives.insert(cctx.intern(Box::from("Bool")), Expr::Literal(Literal::BoolType));
	primitives.insert(cctx.intern(Box::from("I64")), Expr::Literal(Literal::I64Type));
	primitives.insert(cctx.intern(Box::from("Function")), Expr::Ascribe {
		ex: Box::new(Expr::Function {
			body: Box::new(Expr::FunctionType(Box::new(Expr::Argument))),
			data: Box::new(Expr::Tuple(Vec::new())),
		}),
		ty: Box::new(Expr::FunctionType(Box::new(Expr::Tuple(vec![
			Expr::UnnamedAggregateType(Connective::Times, vec![
				Expr::Literal(Literal::TypeType),
				Expr::Literal(Literal::TypeType),
			]),
			Expr::Literal(Literal::TypeType),
		])))),
	});
	primitives.insert(cctx.intern(Box::from("add_i64")), Expr::Ascribe {
		ex: Box::new(Expr::Function {
			body: Box::new(Expr::BinaryOp {
				kind: BinOp::IntAdd,
				operands: Box::new(Expr::Argument),
			}),
			data: Box::new(Expr::Tuple(Vec::new())),
		}),
		ty: Box::new(Expr::FunctionType(Box::new(Expr::Tuple(vec![
			Expr::UnnamedAggregateType(Connective::Times, vec![
				Expr::Literal(Literal::I64Type),
				Expr::Literal(Literal::I64Type),
			]),
			Expr::Literal(Literal::I64Type),
		])))),
	});
	primitives.insert(cctx.intern(Box::from("mul_i64")), Expr::Ascribe {
		ex: Box::new(Expr::Function {
			body: Box::new(Expr::BinaryOp {
				kind: BinOp::IntMul,
				operands: Box::new(Expr::Argument),
			}),
			data: Box::new(Expr::Tuple(Vec::new())),
		}),
		ty: Box::new(Expr::FunctionType(Box::new(Expr::Tuple(vec![
			Expr::UnnamedAggregateType(Connective::Times, vec![
				Expr::Literal(Literal::I64Type),
				Expr::Literal(Literal::I64Type),
			]),
			Expr::Literal(Literal::I64Type),
		])))),
	});
	primitives.insert(cctx.intern(Box::from("false")), Expr::Literal(Literal::Bool(false)));
	primitives.insert(cctx.intern(Box::from("true")), Expr::Literal(Literal::Bool(true)));
	let primitives_file = cctx.new_file("<builtin>".to_string(), "".to_string()).unwrap();
	let primitive_span = Span::new(primitives_file, 0..0);
	let primitives = primitives.into_iter().map(|(name, expr)|
		(name, cctx.new_place(Some(name.string()), Some(expr), primitive_span.clone()))
	).collect();
	let prim_expr = Expr::Module { public: primitives, private: Vec::new() };
	cctx.new_place(Some(prim_name), Some(prim_expr), prim_span)
}
