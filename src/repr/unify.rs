//! Unification of values. Used for type checking.
//!
//! Luckily, this is simplified by our function application lacking judgemental
//! equality, and so this is solely a matter of destructuring.


/*

use core::{
	cell::OnceCell,
	fmt,
	hash::Hash,
	mem,
};

use std::collections::{
	HashMap,
	HashSet,
};

use crate::{
	ctx::{
		EvaluationContext,
		Place,
	},
	log::ErrorEmitted,
	repr::{
		Environment,
		Expr,
	},
};



/*
#[derive(Debug, PartialEq)]
pub enum Property<'cctx> {
	ValueOf(Expr<'cctx>),
	TypeOf(Expr<'cctx>),
}
*/

#[derive(Debug)]
pub struct Bound<'cctx> {
	pub lower: Expr<'cctx>,
	pub upper: Expr<'cctx>,
}

enum DirectStatus<'cctx> {
	Impossible,
	Split(Vec<Bound<'cctx>>),
	BoundLower { lower: Expr<'cctx>, upper_id: usize },
	BoundUpper { lower_id: usize, upper: Expr<'cctx> },
	BoundOrder { lower_id: usize, upper_id: usize },
	Done,
}

impl<'cctx> Bound<'cctx> {
	fn into_directed<'expr>(mut self, ectx: &mut EvaluationContext<'cctx>, solns: &mut Unknowns<'cctx, 'expr>) -> DirectStatus<'cctx> {
		self.lower.fill_solutions(ectx, solns);
		self.upper.fill_solutions(ectx, solns);
		let Ok(()) = self.lower.head_reduce(ectx) else { return DirectStatus::Impossible };
		let Ok(()) = self.upper.head_reduce(ectx) else { return DirectStatus::Impossible };
		match (self.lower, self.upper) {
			(Expr::Literal(l1), Expr::Literal(l2)) =>
				if l1 == l2 { DirectStatus::Done } else { DirectStatus::Impossible },

			(Expr::FunctionType(t1), Expr::FunctionType(t2)) => {
				// TODO probably a less-intrusive way to do this
				let mut t1 = *t1;
				let mut t2 = *t2;
				let Ok(()) = t1.head_reduce(ectx) else { return DirectStatus::Impossible };
				let Ok(()) = t2.head_reduce(ectx) else { return DirectStatus::Impossible };
				match (t1, t2) {
					(Expr::Tuple(lower), Expr::Tuple(upper)) => {
						let Ok([lower_in, lower_out]) = <[Expr; 2]>::try_from(lower) else { return DirectStatus::Impossible };
						let Ok([upper_in, upper_out]) = <[Expr; 2]>::try_from(upper) else { return DirectStatus::Impossible };
						DirectStatus::Split(vec![
							Bound { lower: upper_in, upper: lower_in },
							Bound { lower: lower_out, upper: upper_out },
						])
					},
					(Expr::Tuple(lower), Expr::Unknown { id: upper_id }) => {
						let Ok([lower_in, lower_out]) = <[Expr; 2]>::try_from(lower) else { return DirectStatus::Impossible };
						let upper_in = solns.add_unknown(None);
						let upper_out = solns.add_unknown(None);
						DirectStatus::Split(vec![
							Bound { lower: Expr::Unknown { id: upper_in }, upper: lower_in },
							Bound { lower: lower_out, upper: Expr::Unknown { id: upper_out } },
							// bounds for connecting upper to its parts
							todo!()
						])
					},
					(Expr::Unknown { id: _lower_id }, Expr::Tuple(_upper)) => {
						todo!()
					},
					(Expr::Unknown { id: _lower_id }, Expr::Unknown { id: _upper_id }) => {
						todo!()
					},
					_ => todo!(),
				}
			},

			(Expr::Function { body: body_lower, data: data_lower }, Expr::Function { body: body_upper, data: data_upper }) => {
				let mut lower = *body_lower;
				let mut upper = *body_upper;
				lower.apply(ectx, &Environment { caps: *data_lower, arg: None });
				upper.apply(ectx, &Environment { caps: *data_upper, arg: None });
				Bound { lower, upper }.into_directed(ectx, solns)
			},

			(Expr::Argument, Expr::Argument) => DirectStatus::Done,

			(Expr::UnnamedAggregateType(conn_lower, types_lower), Expr::UnnamedAggregateType(conn_upper, types_upper)) => {
				let diff_conn = conn_lower != conn_upper;
				let diff_types = types_lower.len() != types_upper.len();
				if diff_conn || diff_types { return DirectStatus::Impossible }
				let split = types_lower.into_iter().zip(types_upper).map(|(lower, upper)| Bound { lower, upper });
				DirectStatus::Split(split.collect())
			},

			(Expr::Tuple(elems_lower), Expr::Tuple(elems_upper)) => {
				if elems_lower.len() != elems_upper.len() { return DirectStatus::Impossible }
				let split = elems_lower.into_iter().zip(elems_upper).map(|(lower, upper)| Bound { lower, upper });
				DirectStatus::Split(split.collect())
			},

			(Expr::NamedAggregateType(conn_lower, types_lower), Expr::NamedAggregateType(conn_upper, mut types_upper)) => {
				let diff_conn = conn_lower != conn_upper;
				let diff_types = types_lower.len() != types_upper.len();
				if diff_conn || diff_types { return DirectStatus::Impossible }
				let split = types_lower.into_iter().map(|(k, lower)| Some(Bound { lower, upper: types_upper.remove(&k)? }));
				match split.collect() {
					Some(split) => DirectStatus::Split(split),
					None => DirectStatus::Impossible,
				}
			},

			(Expr::Struct(elems_lower), Expr::Struct(mut elems_upper)) => {
				if elems_lower.len() != elems_upper.len() { return DirectStatus::Impossible }
				let split = elems_lower.into_iter().map(|(k, lower)| Some(Bound { lower, upper: elems_upper.remove(&k)? }));
				match split.collect() {
					Some(split) => DirectStatus::Split(split),
					None => DirectStatus::Impossible,
				}
			},

			(Expr::ModuleType(types_lower), Expr::ModuleType(mut types_upper)) => {
				if types_lower.len() != types_upper.len() { return DirectStatus::Impossible }
				let split = types_lower.into_iter().map(|(k, lower)| Some(Bound { lower, upper: types_upper.remove(&k)? }));
				match split.collect() {
					Some(split) => DirectStatus::Split(split),
					None => DirectStatus::Impossible,
				}
			},

			(Expr::ModuleV(elems_lower), Expr::ModuleV(mut elems_upper)) => {
				let split = elems_lower.into_iter().map(|(k, lower)| Some(Bound { lower, upper: elems_upper.remove(&k)? }));
				match split.collect() {
					Some(split) => DirectStatus::Split(split),
					None => DirectStatus::Impossible,
				}
			},

			(Expr::Unknown { id: lower_id }, Expr::Unknown { id: upper_id }) => DirectStatus::BoundOrder { lower_id, upper_id },
			(lower, Expr::Unknown { id: upper_id }) => DirectStatus::BoundLower { lower, upper_id },
			(Expr::Unknown { id: lower_id }, upper) => DirectStatus::BoundUpper { lower_id, upper },

			_ => DirectStatus::Impossible,
		}
	}

	/*
	// reconsider this
	fn reduce<'expr>(&mut self, ectx: &mut EvaluationContext<'cctx>, solns: &mut Unknowns<'cctx, 'expr>) -> ReduceStatus<'cctx> {
		// TODO eventually shouldn't need to reduce
		// e.g. 6 * factorial 5 = 6 * (5 * factorial 4) should be handled shallowly if possible
		self.lower.fill_solutions(ectx, solns);
		self.upper.fill_solutions(ectx, solns);
		let Ok(()) = self.lower.head_reduce(ectx) else { return ReduceStatus::Impossible };
		let Ok(()) = self.upper.head_reduce(ectx) else { return ReduceStatus::Impossible };
		match (&mut self.lower, &mut self.upper) {
			(Expr::Literal(l1), Expr::Literal(l2)) => if l1 == l2 {
				ReduceStatus::Done
			} else {
				ReduceStatus::Impossible
			},

			(Expr::FunctionType(t1), Expr::FunctionType(t2)) => {
				let mut lower = mem::take(&mut **t1);
				let mut upper = mem::take(&mut **t2);
				let Ok(()) = lower.head_reduce(ectx) else { return ReduceStatus::Impossible };
				let Ok(()) = upper.head_reduce(ectx) else { return ReduceStatus::Impossible };
				// have to split for this until variance is working
				let (lower_from, lower_to) = match lower {
					Expr::Tuple(ts) => {
						let Ok([from, to]) = <[Expr; 2]>::try_from(ts) else { todo!() };
						(from, to)
					},
					// TODO this assumes that there are no subtypes of tuples that don't look like tuples
					//   probably find a better way for this
					Expr::Unknown { id } => {
						let from = solns.add_unknown(None);
						let to = solns.add_unknown(None);
						solns.submit_solution(id, Expr::Tuple(vec![Expr::Unknown { id: from }, Expr::Unknown { id: to }]));
						(Expr::Unknown { id: from }, Expr::Unknown { id: to })
					}
					_ => todo!(),
				};
				let (upper_from, upper_to) = match upper {
					Expr::Tuple(ts) => {
						let Ok([from, to]) = <[Expr; 2]>::try_from(ts) else { todo!() };
						(from, to)
					},
					Expr::Unknown { id } => {
						let from = solns.add_unknown(None);
						let to = solns.add_unknown(None);
						solns.submit_solution(id, Expr::Tuple(vec![Expr::Unknown { id: from }, Expr::Unknown { id: to }]));
						(Expr::Unknown { id: from }, Expr::Unknown { id: to })
					}
					_ => todo!(),
				};
				ReduceStatus::Split(vec![
					Bound { lower: upper_from, upper: lower_from },
					Bound { lower: lower_to, upper: upper_to },
				])
			},

			(Expr::Function { body: body_lower, data: data_lower }, Expr::Function { body: body_upper, data: data_upper }) => {
				let mut lower = mem::take(&mut **body_lower);
				let mut upper = mem::take(&mut **body_upper);
				lower.apply(ectx, &Environment { caps: (**data_lower).clone(), arg: None });
				upper.apply(ectx, &Environment { caps: (**data_upper).clone(), arg: None });
				*self = Bound { lower, upper };
				ReduceStatus::ProgressInPlace
			},

			(Expr::Argument, Expr::Argument) => ReduceStatus::Done,

			(Expr::UnnamedAggregateType(c1, t1), Expr::UnnamedAggregateType(c2, t2)) => {
				if c1 != c2 || t1.len() != t2.len() {
					ReduceStatus::Impossible
				} else {
					let t1 = mem::take(t1);
					let t2 = mem::take(t2);
					ReduceStatus::Split(t1.into_iter().zip(t2).map(|(lower, upper)| Bound { lower, upper }).collect())
				}
			},
			(Expr::Tuple(e1), Expr::Tuple(e2)) => {
				if e1.len() != e2.len() {
					ReduceStatus::Impossible
				} else {
					let e1 = mem::take(e1);
					let e2 = mem::take(e2);
					ReduceStatus::Split(e1.into_iter().zip(e2).map(|(lower, upper)| Bound { lower, upper }).collect())
				}
			},

			(Expr::NamedAggregateType(c1, t1), Expr::NamedAggregateType(c2, t2)) => {
				if c1 != c2 || t1.len() != t2.len() {
					ReduceStatus::Impossible
				} else {
					let t1 = mem::take(t1);
					let mut t2 = mem::take(t2);
					let bounds = t1.into_iter().map(|(k, lower)| Some(Bound { lower, upper: t2.remove(&k)? }));
					// None if the types do not have the same member names
					match bounds.collect() {
						Some(bounds) => ReduceStatus::Split(bounds),
						None => ReduceStatus::Impossible,
					}
				}
			},
			(Expr::Struct(e1), Expr::Struct(e2)) => {
				if e1.len() != e2.len() {
					ReduceStatus::Impossible
				} else {
					let e1 = mem::take(e1);
					let mut e2 = mem::take(e2);
					let bounds = e1.into_iter().map(|(k, lower)| Some(Bound { lower, upper: e2.remove(&k)? }));
					// None if the structs do not have the same member names
					match bounds.collect() {
						Some(bounds) => ReduceStatus::Split(bounds),
						None => ReduceStatus::Impossible,
					}
				}
			},

			(Expr::ModuleType(t1), Expr::ModuleType(t2)) => {
				if t1.len() != t2.len() {
					ReduceStatus::Impossible
				} else {
					let t1 = mem::take(t1);
					let mut t2 = mem::take(t2);
					let bounds = t1.into_iter().map(|(k, lower)| Some(Bound { lower, upper: t2.remove(&k)? }));
					// None if the types do not have the same member names
					match bounds.collect() {
						Some(bounds) => ReduceStatus::Split(bounds),
						None => ReduceStatus::Impossible,
					}
				}
			},
			(Expr::ModuleV(e1), Expr::ModuleV(e2)) => {
				if e1.len() != e2.len() {
					ReduceStatus::Impossible
				} else {
					let e1 = mem::take(e1);
					let mut e2 = mem::take(e2);
					let bounds = e1.into_iter().map(|(k, lower)| Some(Bound { lower, upper: e2.remove(&k)? }));
					// None if the structs do not have the same member names
					match bounds.collect() {
						Some(bounds) => ReduceStatus::Split(bounds),
						None => ReduceStatus::Impossible,
					}
				}
			},

			(Expr::Unknown { id: id_lower }, Expr::Unknown { id: id_upper }) => ReduceStatus::DoneOrder(id_lower, id_upper),

			// TODO this assumes that there are no subtypes of tuples that don't look like tuples
			//   probably find a better way for this
			(Expr::Unknown { id }, Expr::FunctionType(_)) => {
				let from = Expr::Unknown { id: solns.add_unknown(None) };
				let to = Expr::Unknown { id: solns.add_unknown(None) };
				solns.submit_solution(*id, Expr::FunctionType(Box::new(Expr::Tuple(vec![
					from, to,
				]))));
				ReduceStatus::ProgressInPlace
			},
			(Expr::FunctionType(_), Expr::Unknown { id }) => {
				let from = Expr::Unknown { id: solns.add_unknown(None) };
				let to = Expr::Unknown { id: solns.add_unknown(None) };
				solns.submit_solution(*id, Expr::FunctionType(Box::new(Expr::Tuple(vec![
					from, to,
				]))));
				ReduceStatus::ProgressInPlace
			},

			// have to be handled non-locally
			(Expr::Unknown { .. }, _) => ReduceStatus::NoProgress,
			(_, Expr::Unknown { .. }) => ReduceStatus::NoProgress,

			_ => ReduceStatus::Impossible,
		}
	}
	*/
}

/*
impl<'cctx> fmt::Display for Property<'cctx> {
	fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
		match self {
			Property::ValueOf(expr) => write!(f, "{}", expr),
			Property::TypeOf(expr) => write!(f, "typeof({})", expr),
		}
	}
}
*/
impl<'cctx> fmt::Display for Bound<'cctx> {
	fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
		write!(f, "{} ≤ {}", self.lower, self.upper)
	}
}



#[derive(Debug, Default)]
pub struct Unknowns<'cctx, 'expr> {
	data: Vec<(Option<&'expr Expr<'cctx>>, Option<Expr<'cctx>>)>,
}

impl<'cctx, 'expr> Unknowns<'cctx, 'expr> {
	pub fn add_unknown(&mut self, over_expr: Option<&'expr Expr<'cctx>>) -> usize {
		let res = self.data.len();
		self.data.push((over_expr, None));
		res
	}

	pub fn len(&self) -> usize {
		self.data.len()
	}

	pub fn iter(&self) -> impl Iterator<Item = (Option<&'expr Expr<'cctx>>, Option<&Expr<'cctx>>)> {
		self.data.iter().map(|(name, soln)| (*name, soln.as_ref()))
	}
	pub fn iter_mut(&mut self) -> impl Iterator<Item = (&mut Option<&'expr Expr<'cctx>>, &mut Option<Expr<'cctx>>)> {
		self.data.iter_mut().map(|(name, soln)| (name, soln))
	}

	pub fn submit_solution(&mut self, id: usize, soln: Expr<'cctx>) {
		self.data[id].1 = Some(soln);
	}

	pub fn solution(&self, id: usize) -> Option<&Expr<'cctx>> {
		self.data[id].1.as_ref()
	}
}

#[derive(Debug, Default)]
pub struct BoundManager<'cctx, 'expr> {
	pub unknowns: Unknowns<'cctx, 'expr>,
	pub bounds: Vec<Bound<'cctx>>,
	place_synths: HashMap<Place<'cctx>, Option<Expr<'cctx>>>,
}

impl<'cctx, 'expr> BoundManager<'cctx, 'expr> {
	pub fn new() -> Self {
		Self::default()
	}

	pub fn new_unknown(&mut self, over_expr: Option<&'expr Expr<'cctx>>) -> usize {
		self.unknowns.add_unknown(over_expr)
	}

	pub fn place_synth(
		&mut self,
		ectx: &mut EvaluationContext<'cctx>,
		env: Option<&Environment<Expr<'cctx>>>,
		place: Place<'cctx>
	) -> Expr<'cctx> {
		match self.place_synths.get(&place) {
			Some(Some(ty)) => ty.clone(),
			None => {
				self.place_synths.insert(place, None);
				let res = place.to_ref().expr.get().unwrap().synth_bounds(ectx, env, self);
				self.place_synths.insert(place, Some(res.clone()));
				res
			},
			// cycle
			Some(None) => todo!(),
		}
		/*
		use std::collections::hash_map::Entry;
		match self.place_synths.entry(place) {
			Entry::Occupied(e) => e.get().clone(),
			Entry::Vacant(e) => {
				let new = self.unknowns.len();
				self.unknowns.push(());
				let res = e.insert(new).clone();
				place.to_ref().expr.get().unwrap().bounds(ectx, self, new);
				res
			},
		}
		*/
	}

	pub fn add_bound(&mut self, bound: Bound<'cctx>) {
		self.bounds.push(bound);
	}

	pub fn solve(mut self, ectx: &mut EvaluationContext<'cctx>) -> Result<(), ErrorEmitted> {
		let mut lower_bounds = HashMap::<usize, Vec<Expr>>::new();
		let mut upper_bounds = HashMap::<usize, Vec<Expr>>::new();
		let mut order_bounds = HashSet::<(usize, usize)>::new();

		while let Some(next_bound) = self.bounds.pop() {
			match next_bound.into_directed(ectx, &mut self.unknowns) {
				DirectStatus::Impossible => todo!(), // eventually want Impossible to hold an ErrorEmitted
				DirectStatus::Split(bounds) => self.bounds.extend(bounds),
				DirectStatus::BoundLower { lower, upper_id } => { lower_bounds.entry(upper_id).or_default().push(lower); },
				DirectStatus::BoundUpper { lower_id, upper } => { upper_bounds.entry(lower_id).or_default().push(upper); },
				DirectStatus::BoundOrder { lower_id, upper_id } => { order_bounds.insert((lower_id, upper_id)); },
				DirectStatus::Done => (),
			}
		}

		let mut unsolved = (0..self.unknowns.len()).into_iter().collect::<Vec<_>>();
		loop {
			let mut made_progress = false;

			for i in unsolved.iter().copied() {
				// these could be cached
				for upper in graph_reachable_forward(&order_bounds, i) {
					//upper_bounds.get
				}
			}

			if !made_progress { todo!() }
		}

		Ok(())

		// the previous iteration (commented out below) is theoretically better in terms of performance.
		// however, critically it has cases where providing more bounds actually makes it unable to solve.
		// this is because it simplifies the bounds by performing local reductions,
		// which rely on the bound graph being somewhat sparse.
		// maybe eventually these could be readded to help with simple cases.
		/*
		loop {
			let mut made_progress = false;
			let mut idx = 0;
			while idx < self.bounds.len() {
				match self.bounds[idx].reduce(ectx, &mut self.unknowns) {
					ReduceStatus::Done => {
						made_progress = true;
						self.bounds.remove(idx);
					},
					ReduceStatus::NoProgress => idx += 1,
					ReduceStatus::ProgressInPlace => made_progress = true,
					ReduceStatus::Split(sub_bounds) => {
						made_progress = true;
						self.bounds.remove(idx);
						self.bounds.extend(sub_bounds);
					},
					ReduceStatus::Impossible => {
						eprintln!("{}", self.bounds[idx]);
						todo!()
					},
				}
			}

			// if an unknown has lower or upper bounds which don't depend on any other unknowns,
			// we may pull it to that side
			let deps = self.bounds.iter().map(|b| {
				let mut lower_deps = HashSet::new();
				let mut upper_deps = HashSet::new();
				b.lower.unknown_deps(ectx, &mut lower_deps);
				b.upper.unknown_deps(ectx, &mut upper_deps);
				(b, lower_deps, upper_deps)
			});
			let deps: Vec<_> = deps.collect();

			for (id, (_, soln)) in self.unknowns.iter_mut().enumerate() {
				if soln.get().is_some() { continue; }

				let mut present_in_lower: Vec<_> = deps.iter().filter(|d| d.1.contains(&id)).collect();
				let meets_conditions = !present_in_lower.is_empty()
					&& present_in_lower.iter().all(|d| d.2.is_empty() && matches!(d.0.lower, Expr::Unknown { .. }));
				if meets_conditions {
					made_progress = true;
					// TODO maybe add initial/terminal types?
					if present_in_lower.len() == 0 {
						todo!();
					}
					let lim = present_in_lower.pop().unwrap().0.upper.clone();
					let lim = present_in_lower.into_iter().fold(lim, |acc, l| limit(&acc, &l.0.upper).unwrap());
					soln.set(lim).unwrap();
					continue;
				}

				let mut present_in_upper: Vec<_> = deps.iter().filter(|d| d.2.contains(&id)).collect();
				let meets_conditions = !present_in_upper.is_empty()
					&& present_in_upper.iter().all(|d| d.1.is_empty() && matches!(d.0.upper, Expr::Unknown { .. }));
				if meets_conditions {
					made_progress = true;
					// TODO maybe add initial/terminal types?
					if present_in_upper.len() == 0 {
						todo!();
					}
					let col = present_in_upper.pop().unwrap().0.lower.clone();
					let col = present_in_upper.into_iter().fold(col, |acc, l| colimit(&acc, &l.0.lower).unwrap());
					soln.set(col).unwrap();
					continue;
				}
			}

			eprintln!("{}", self.bounds.len());
			for bound in self.bounds.iter() {
				eprintln!("{}", bound);
			}
			eprintln!();

			if self.bounds.is_empty() {
				return;
			}

			if !made_progress {
				todo!()
			}
		}
		*/
	}
}

fn graph_reachable_forward<T>(adj: &HashSet<(T, T)>, start: T) -> HashSet<T>
where T: Hash + Eq + Copy {
	let mut reached = HashSet::new();
	let mut adding = HashSet::new();
	adding.insert(start);
	loop {
		let prev_len = reached.len();

		adding = adding.difference(&reached).copied().collect();
		adding = adding.into_iter().map(|x| {
			reached.insert(x);
			adj.iter().cloned().filter_map(move |(s, t)| (s == x).then_some(t))
		}).flatten().collect();

		if reached.len() == prev_len { break }
	}
	reached
}

fn graph_reachable_backward<T>(adj: &HashSet<(T, T)>, start: T) -> HashSet<T>
where T: Hash + Eq + Copy {
	let mut reached = HashSet::new();
	let mut adding = HashSet::new();
	adding.insert(start);
	loop {
		let prev_len = reached.len();

		adding = adding.difference(&reached).copied().collect();
		adding = adding.into_iter().map(|x| {
			reached.insert(x);
			adj.iter().cloned().filter_map(move |(s, t)| (t == x).then_some(s))
		}).flatten().collect();

		if reached.len() == prev_len { break }
	}
	reached
}

fn limit<'cctx>(lhs: &Expr<'cctx>, rhs: &Expr<'cctx>) -> Option<Expr<'cctx>> {
	Some(match (lhs, rhs) {
		(Expr::Literal(l1), Expr::Literal(l2)) => if l1 == l2 {
			Expr::Literal(*l1)
		} else {
			return None;
		},

		(Expr::FunctionType(_t1), Expr::FunctionType(_t2)) => todo!(),
		(Expr::Function { .. }, Expr::Function { .. }) => todo!(),
		(Expr::Argument, Expr::Argument) => Expr::Argument,

		(Expr::UnnamedAggregateType(c1, t1), Expr::UnnamedAggregateType(c2, t2)) => {
			if c1 != c2 || t1.len() != t2.len() {
				return None;
			}
			let zip: Option<_> = t1.iter().zip(t2.iter()).map(|(lhs, rhs)| limit(lhs, rhs)).collect();
			Expr::UnnamedAggregateType(*c1, zip?)
		},
		(Expr::Tuple(e1), Expr::Tuple(e2)) => {
			if e1.len() != e2.len() {
				return None;
			}
			let zip: Option<_> = e1.iter().zip(e2.iter()).map(|(lhs, rhs)| limit(lhs, rhs)).collect();
			Expr::Tuple(zip?)
		},

		(Expr::NamedAggregateType(c1, t1), Expr::NamedAggregateType(c2, t2)) => {
			if c1 != c2 || t1.len() != t2.len() {
				return None;
			}
			let zip: Option<_> = t1.iter().map(|(k, lhs)| Some((*k, limit(lhs, t2.get(k)?)?))).collect();
			Expr::NamedAggregateType(*c1, zip?)
		},
		(Expr::Struct(e1), Expr::Struct(e2)) => {
			if e1.len() != e2.len() {
				return None;
			}
			let zip: Option<_> = e1.iter().map(|(k, lhs)| Some((*k, limit(lhs, e2.get(k)?)?))).collect();
			Expr::Struct(zip?)
		},

		(Expr::ModuleType(t1), Expr::ModuleType(t2)) => {
			if t1.len() != t2.len() {
				return None;
			}
			let zip: Option<_> = t1.iter().map(|(k, lhs)| Some((*k, limit(lhs, t2.get(k)?)?))).collect();
			Expr::ModuleType(zip?)
		},
		(Expr::ModuleV(e1), Expr::ModuleV(e2)) => {
			if e1.len() != e2.len() {
				return None;
			}
			let zip: Option<_> = e1.iter().map(|(k, lhs)| Some((*k, limit(lhs, e2.get(k)?)?))).collect();
			Expr::ModuleV(zip?)
		},

		_ => return None,
	})
}

fn colimit<'cctx>(lhs: &Expr<'cctx>, rhs: &Expr<'cctx>) -> Option<Expr<'cctx>> {
	Some(match (lhs, rhs) {
		(Expr::Literal(l1), Expr::Literal(l2)) => if l1 == l2 {
			Expr::Literal(*l1)
		} else {
			return None;
		},

		(Expr::FunctionType(_t1), Expr::FunctionType(_t2)) => todo!(),
		(Expr::Function { .. }, Expr::Function { .. }) => todo!(),
		(Expr::Argument, Expr::Argument) => Expr::Argument,

		(Expr::UnnamedAggregateType(c1, t1), Expr::UnnamedAggregateType(c2, t2)) => {
			if c1 != c2 || t1.len() != t2.len() {
				return None;
			}
			let zip: Option<_> = t1.iter().zip(t2.iter()).map(|(lhs, rhs)| colimit(lhs, rhs)).collect();
			Expr::UnnamedAggregateType(*c1, zip?)
		},
		(Expr::Tuple(e1), Expr::Tuple(e2)) => {
			if e1.len() != e2.len() {
				return None;
			}
			let zip: Option<_> = e1.iter().zip(e2.iter()).map(|(lhs, rhs)| colimit(lhs, rhs)).collect();
			Expr::Tuple(zip?)
		},

		(Expr::NamedAggregateType(c1, t1), Expr::NamedAggregateType(c2, t2)) => {
			if c1 != c2 || t1.len() != t2.len() {
				return None;
			}
			let zip: Option<_> = t1.iter().map(|(k, lhs)| Some((*k, colimit(lhs, t2.get(k)?)?))).collect();
			Expr::NamedAggregateType(*c1, zip?)
		},
		(Expr::Struct(e1), Expr::Struct(e2)) => {
			if e1.len() != e2.len() {
				return None;
			}
			let zip: Option<_> = e1.iter().map(|(k, lhs)| Some((*k, colimit(lhs, e2.get(k)?)?))).collect();
			Expr::Struct(zip?)
		},

		(Expr::ModuleType(t1), Expr::ModuleType(t2)) => {
			if t1.len() != t2.len() {
				return None;
			}
			let zip: Option<_> = t1.iter().map(|(k, lhs)| Some((*k, colimit(lhs, t2.get(k)?)?))).collect();
			Expr::ModuleType(zip?)
		},
		(Expr::ModuleV(e1), Expr::ModuleV(e2)) => {
			if e1.len() != e2.len() {
				return None;
			}
			let zip: Option<_> = e1.iter().map(|(k, lhs)| Some((*k, colimit(lhs, e2.get(k)?)?))).collect();
			Expr::ModuleV(zip?)
		},

		_ => return None,
	})
}






// (A, B) <: (C, D) → A <: C, B <: D
//





/*

/// Trace through structuring/destructuring to the original definition of an
/// expression.
///
/// For example, if `foo := ("a", 2 + 2, "c")` and `bar := foo._1`, then
/// `resolve` on the expression for `bar` gives the expression `2 + 2`.
fn resolve<'cctx>(expr: &'cctx Expr<'cctx>) -> Option<&'cctx Expr<'cctx>> {
	match expr {
		Expr::TakeTupleElement(tuple, idx) => {
			if let Expr::MakeTuple(places) = resolve(tuple)? {
				Some(places.get(*idx)?.to_ref().expr.get()?)
			} else {
				Some(expr)
			}
		},

		Expr::TakeStructMember(structure, ident) => {
			if let Expr::MakeStruct(places) = resolve(structure)? {
				Some(places.get(ident)?.to_ref().expr.get()?)
			} else {
				Some(expr)
			}
		},

		_ => Some(expr),
	}
}

/// Representation of an arbitraray unification bound for type deduction.
///
/// Deduction is done in a method roughly equivalent to the semantic tableau.
///
/// It may be helpful to think of the bounds here in terms of logical operations on types,
/// even though their use extends further.
///
/// If we have that either `x : A` or `x : B`,
/// for instance if `x := y` and there are two values `y` in scope,
/// then we have that `x : A ⊔ B`.
///
/// If we have that both `x : A` and `x : B`,
/// for instance if `x` is used in a context where an `A` is expected and one where a `B` is expected,
/// then we have that `x : A ⊓ B`.
///
/// If we have that `x : F a` for some `a`,
/// then we have that `x : ∃a (F a)`.
///
/// If we have that `x : F a` for all `a`,
/// then we have that `x : ∀a (F a)`.
///
/// If a constraint is found to be impossible to resolve,
/// then we say that it is `⊥`.
///
/// Then, we permit the following reductions:
/// - `a ⊔ ⊥` → `a`
/// - `a ⊓ ⊥` → `⊥`
/// - `C a ⊔ C b` → `C (a ⊔ b)`
/// - `C a ⊓ C b` → `C (a ⊓ b)`
/// - `C a ⊔ D b` where `C` and `D` are distinct constructors → `⊥`
/// - `C a ⊓ D b` where `C` and `D` are distinct constructors → `⊥`
/// - `∃[x : T] (f x)` → `f (∃[x : T] x)`
/// - `∃[x : T] (f x) ⊔ (f a)` → `∃[x : T] (f x)`
/// - `∃[x : T] (f x) ⊓ (f a)` → `f a`
/// - `∀[x : T] (f x)` → `f (∀[x : T] x)`
/// - `∀[x : T] (f x) ⊔ (f a)` → `f a`
/// - `∀[x : T] (f x) ⊓ (f a)` → `∀[x : T] (f x)`
enum DeductionTree<'cctx> {
	Exact(Expr<'cctx>),
	All(Vec<DeductionTree<'cctx>>),
	Any(Vec<DeductionTree<'cctx>>),
	Uni(Box<DeductionTree<'cctx>>),
	Exi(Box<DeductionTree<'cctx>>),
}


*/

*/
