//! Name resolution and scopes. Used when evaluating names but haven't yet
//! actually built any expressions, so this is only for finding values
//! implicitly in scope, whereas qualified values using the `.` operator require
//! type checking to know if an arbitrary expression has a member.



use std::{
	collections::{
		HashMap,
	},
	sync::Mutex,
};

use crate::{
	ctx::{
		CompileContext,
		Place,
		Identifier,
		Span,
	},
	log,
	repr::{
		Expr,
	},
};



/*
#[derive(Debug)]
pub struct ParentedScope<'cctx, 'scopes> {
	pub scope: &'scopes Scope<'cctx>,
	pub parent: Option<&'scopes ParentedScope<'cctx, 'scopes>>,
}

impl<'cctx, 'scopes> ParentedScope<'cctx, 'scopes> {
	pub fn new(
		scope: &'scopes Scope<'cctx>,
		parent: Option<&'scopes ParentedScope<'cctx, 'scopes>>,
	) -> Self {
		Self { scope, parent }
	}

	pub fn just(scope: &'scopes Scope<'cctx>) -> Self {
		Self { scope, parent: None }
	}

	pub fn with_parent(
		scope: &'scopes Scope<'cctx>,
		parent: &'scopes ParentedScope<'cctx, 'scopes>,
	) -> Self {
		Self { scope, parent: Some(parent) }
	}
}
*/



type Captures<'cctx> = Mutex<(HashMap<Place<'cctx>, Place<'cctx>>, Vec<Place<'cctx>>)>;

#[derive(Debug)]
pub struct Scope<'cctx, 'a> {
	/// Named places within this scope.
	pub places: HashMap<Identifier<'cctx>, Place<'cctx>>,
	/// Values captured from outer scopes if this is a function body.
	pub captures: Option<&'a Captures<'cctx>>,
	/// Whether statements in this scope may move out of the results of previous
	/// ones. Currently unused.
	pub is_ordered: bool,
	/// The parent scope to this one, if any. In the case that `captures` is not
	/// [`None`], then any name only found in parent scopes will be considered a
	/// capture and added.
	pub parent: Option<&'a Scope<'cctx, 'a>>,
}

impl<'cctx, 'a> Scope<'cctx, 'a> {
	/// Create a new scope given its members in order.
	#[inline(always)]
	pub fn new(
		places: HashMap<Identifier<'cctx>, Place<'cctx>>,
		captures: Option<&'a Captures<'cctx>>,
		is_ordered: bool,
		parent: Option<&'a Scope<'cctx, 'a>>,
	) -> Self {
		Self {
			places,
			captures,
			is_ordered,
			parent,
		}
	}
	
	fn get_place(&self, name: Identifier<'cctx>) -> Option<Place<'cctx>> {
		self.places.get(&name).cloned()
	}

	pub fn resolve(&self, cctx: &'cctx CompileContext<'cctx>, name: Identifier<'cctx>, span: Span<'cctx>) -> Option<Place<'cctx>> {
		if let Some(place) = self.places.get(&name) {
			Some(*place)
		} else if let Some(parent) = self.parent {
			let place = parent.resolve(cctx, name, span.clone())?;
			Some( match &self.captures {
				None => place,
				Some(captures) => {
					use std::collections::hash_map::Entry;
					let mut lock = captures.lock().unwrap();
					let len = lock.1.len();
					match lock.0.entry(place) {
						Entry::Occupied(e) => *e.get(),
						Entry::Vacant(e) => {
							let capture_span = place.to_ref().span.clone();
							let res = *e.insert(cctx.place(
								Expr::TakeUnnamedElement {
									ex: Box::new(Expr::Captures),
									idx: len,
								},
								capture_span
							));
							lock.1.push(place);
							res
						},
					}
				},
			})
		} else {
			None
		}
	}

	fn resolve_or_err(&self, cctx: &'cctx CompileContext<'cctx>, name: Identifier<'cctx>, span: Span<'cctx>) -> Place<'cctx> {
		self.resolve(cctx, name, span.clone()).unwrap_or_else(|| {
			log::error(
				log::frame(format!("could not find name `{}` in scope", name.string()))
				.with_annotation(String::from("name expected here"), span.clone())
			).emit(cctx);
			cctx.place(Expr::Error, span)
		})
	}
}

pub trait OptionScopeExt<'cctx> {
	fn resolve(&self, cctx: &'cctx CompileContext<'cctx>, name: Identifier<'cctx>, span: Span<'cctx>) -> Option<Place<'cctx>>;

	fn resolve_or_err(&self, cctx: &'cctx CompileContext<'cctx>, name: Identifier<'cctx>, span: Span<'cctx>) -> Place<'cctx> {
		self.resolve(cctx, name, span.clone()).unwrap_or_else(|| {
			log::error(
				log::frame(format!("could not find name `{}` in scope", name.string()))
				.with_annotation(String::from("name expected here"), span.clone())
			).emit(cctx);
			cctx.place(Expr::Error, span)
		})
	}
}

impl<'cctx, 'a> OptionScopeExt<'cctx> for Option<&'a Scope<'cctx, 'a>> {
	fn resolve(&self, cctx: &'cctx CompileContext<'cctx>, name: Identifier<'cctx>, span: Span<'cctx>) -> Option<Place<'cctx>> {
		self.and_then(|scope| scope.resolve(cctx, name, span))
	}
}
