//! Internal representations of code elements.

// TODO this file is closest to a mess currently



pub mod scope;
pub mod unify;



use std::{
	collections::{
		HashMap,
		hash_map::Entry,
		HashSet,
	},
	fmt::{
		self,
		Display,
	},
	mem,
	ops::Range,
	sync::Mutex,
};

use crate::{
	//count_set::CountSet,
	ctx::{
		CompileContext,
		EvaluationContext,
		File,
		Identifier,
		Place,
		//QueryKind,
		Span,
	},
	log,
	parse::{
		BracketKind,
		Delimiter,
		RegionInner,
		WrittenExpression,
		WrittenStatement,
	},
};

use scope::{
	OptionScopeExt,
	Scope,
};

/*
use unify::{
	Bound,
	BoundManager,
	//Property,
	Unknowns,
};
*/



/*
macro_rules! dep {
	(synth $e:expr) => { $e.synth_dependencies() };
	(eval $e:expr) => { $e.evaluation_dependencies() };
	(codegen $e:expr) => { $e.codegen_dependencies() };
}
macro_rules! deps {
	[] => { HashSet::new() };
	[$ht:ident $he:expr] => { dep!($ht $he) };
	[$head_t:ident $head_e:expr, $($rest_t:ident $rest_e:expr),* $(,)?] => {{
		let mut res = dep!($head_t $head_e);
		$(res.extend(dep!($rest_t $rest_e));)*
		res
	}};
}
*/



/// Literal values for primitive types.
#[derive(Clone, Copy, Debug, Eq, PartialEq)]
pub enum Literal {
	/// The universe type (type of types), available through `prim.Type`.
	TypeType,
	/// The type of booleans, available through `prim.Bool`.
	BoolType,
	/// The type of 64-bit integers, available through `prim.I64`.
	I64Type,
	/// A boolean literal, available through `prim.false` and `prim.true`.
	Bool(bool),
	/// An integer literal, available by digit-only identifiers.
	I64(i64),
}

#[derive(Clone, Copy, Debug, Eq, PartialEq)]
pub enum Connective {
	Plus,
	Times,
	With,
	Par,
}

#[derive(Clone, Copy, Debug, Eq, PartialEq)]
pub enum BinOp {
	IntAdd,
	//Sub,
	IntMul,
	//Div,
	//Rem,
}
impl Display for BinOp {
	fn fmt(&self, f: &mut fmt::Formatter<'_>) -> Result<(), fmt::Error> {
		match self {
			BinOp::IntAdd => write!(f, "+"),
			BinOp::IntMul => write!(f, "*"),
		}
	}
}

/// Forms which evaluate to values. The 'right-hand side' of a statement.
#[derive(Clone, Debug)]
pub enum Expr<'cctx> {
	/// A literal value.
	Literal(Literal),

	/// Ascribe to an expression a specific type
	Ascribe {
		ex: Box<Expr<'cctx>>,
		ty: Box<Expr<'cctx>>,
	},

	/// Take value out of this place
	Use {
		place: Place<'cctx>,
	},

//	/// Allocate a space on the stack
//	MakeStored,
//	/// Take contained value from a stack-stored value
//	TakeStored(Place<'cctx>),
//	/// Place a value into a location on the stack.
//	PutStored(Place<'cctx>, Place<'cctx>),
//	/// Represents the result of the second expression, strictly after mutable
//	/// state has been updated to the result of the first expression.
//	InOrder(Place<'cctx>, Place<'cctx>),

	FunctionType(Box<Expr<'cctx>>),
	Function {
		body: Box<Expr<'cctx>>,
		data: Box<Expr<'cctx>>,
	},
	/// Call the first expression with the second as the argument.
	Call {
		func: Box<Expr<'cctx>>,
		arg: Box<Expr<'cctx>>,
	},
	/// Access the argument within a function.
	Argument,
	/// Access the captures within a closure.
	Captures,

	UnnamedAggregateType(Connective, Vec<Expr<'cctx>>),
	/// Constructing a tuple. `(1, 2, 4)`
	Tuple(Vec<Expr<'cctx>>),

	NamedAggregateType(Connective, HashMap<Identifier<'cctx>, Expr<'cctx>>),
	Struct(HashMap<Identifier<'cctx>, Expr<'cctx>>),

	ModuleType(HashMap<Identifier<'cctx>, Expr<'cctx>>),
	Module {
		public: HashMap<Identifier<'cctx>, Place<'cctx>>,
		private: Vec<Place<'cctx>>,
	},
	ModuleV(HashMap<Identifier<'cctx>, Expr<'cctx>>),

	/// Moving operation of out a tuple's nth member
	TakeUnnamedElement {
		ex: Box<Expr<'cctx>>,
		idx: usize,
	},
	/// Moving operation of out a struct's member under a given name
	TakeNamedElement {
		ex: Box<Expr<'cctx>>,
		idx: Identifier<'cctx>,
	},

	/// A primitive binary operation, taking a 2-tuple.
	BinaryOp {
		kind: BinOp,
		operands: Box<Expr<'cctx>>,
	},
	/// Pattern matching on a boolean value.
	PrimIfThenElse {
		predicate: Box<Expr<'cctx>>,
		true_case: Box<Expr<'cctx>>,
		false_case: Box<Expr<'cctx>>,
	},

	/// An unknown value to be filled in during type/value synthesis. Does not lower.
	Unknown { id: usize },
	/// A tuple type's nth member. Only for type/value synthesis. Does not lower.
	UnnamedTypeElement {
		ty: Box<Expr<'cctx>>,
		idx: usize,
	},
	/// A struct or module type's named member. Only for type/value synthesis. Does not lower.
	NamedTypeElement {
		ty: Box<Expr<'cctx>>,
		idx: Identifier<'cctx>,
	},

	/// A non-fatal error. Should be handled as if it is a correct expression wherever possible
	Error,
}

impl<'cctx> Default for Expr<'cctx> {
	fn default() -> Self {
		Self::Error
	}
}



impl<'cctx> Expr<'cctx> {
	/// Convert from an AST.
	pub fn from_written(
		cctx: &'cctx CompileContext<'cctx>,
		file: File<'cctx>,
		// using stack linked list built in recursion instead of vector in the hope
		// that eventually this can be made asynchronous or otherwise concurrent.
		scopes: Option<&Scope<'cctx, '_>>,
		written_expr: WrittenExpression<'cctx>,
	) -> Self {
		match written_expr {
			// TODO more robust operator handling
			// for instance probably trait-like
			WrittenExpression::UnaryOperation((name, op_range), wex) => {
				//let wex_span = wex.span_in(file);
				Expr::Call {
					func: Box::new(Expr::Use {
						place: scopes.resolve_or_err(cctx, name, Span::new(file, op_range)),
					}),
					arg: Box::new(Expr::from_written(cctx, file, scopes, *wex)),
				}
			},
			WrittenExpression::BinaryOperation((name, op_range), wexs) => match name.string() {
				"" => if let WrittenExpression::Ident(name, range) = wexs.0 {
					match name.string() {
						"module" => Expr::read_module(cctx, file, scopes, wexs.1),
						_ => Expr::Call {
							func: Box::new(Expr::from_written(cctx, file, scopes, WrittenExpression::Ident(name, range))),
							arg: Box::new(Expr::from_written(cctx, file, scopes, wexs.1)),
						},
					}
				} else {
					//let wex0_span = wexs.0.span_in(file);
					//let wex1_span = wexs.1.span_in(file);
					Expr::Call {
						func: Box::new(Expr::from_written(cctx, file, scopes, wexs.0)),
						arg: Box::new(Expr::from_written(cctx, file, scopes, wexs.1)),
					}
				},
				":" => {
					// TODO span
					//let wex0_span = wexs.0.span_in(file);
					//let wex1_span = wexs.1.span_in(file);
					Expr::Ascribe {
						ex: Box::new(Expr::from_written(cctx, file, scopes, wexs.0)),
						ty: Box::new(Expr::from_written(cctx, file, scopes, wexs.1)),
					}
				},
				"\\" => {
					let bind_span = wexs.0.span_in(file);
					//let body_span = wexs.1.span_in(file);
					let binds = Binds::from_written(cctx, file, scopes, wexs.0, Expr::Argument, bind_span.clone());
					let captures = Mutex::new((HashMap::new(), Vec::new()));
					let scope = Scope::new(binds.named, Some(&captures), true, scopes);
					let body = Expr::from_written(cctx, file, Some(&scope), wexs.1);
					// not strictly necessary, this could always be a closure, just probably
					// a nice way to save a ton of small allocations
					// ... except that doesn't currently actually work TODO
					let captures = captures.into_inner().unwrap();
					/*
					if captures.1.is_empty() {
						Expr::ConstantValue(Expr::function(Box::new(body)))
					} else {
						Expr::Function {
							body: cctx.place(body),
							data: cctx.place(Expr::Tuple(captures.1)),
						}
					}
					*/
					let captures = captures.1.into_iter().map(|place| Expr::Use { place }).collect();
					Expr::Function {
						body: Box::new(body),
						data: Box::new(Expr::Tuple(captures)),
					}
				},
				".=" => {
					// TODO mutable assignment
					/*
					if let WrittenExpression::Ident(_name, _span) = wexs.0 {
						//scopes.resolve_or_err(cctx, name)
						//wexs.1;
					} else {
						todo!()
					}
					*/
					todo!()
				},
				_ => {
					//let expr_span = Span::new(file, wexs.0.start()..wexs.1.end());
					//let wex0_span = wexs.0.span_in(file);
					//let wex1_span = wexs.1.span_in(file);
					Expr::Call {
						func: Box::new(Expr::Use {
							place: scopes.resolve_or_err(cctx, name, Span::new(file, op_range)),
						}),
						arg: Box::new(Expr::Tuple(vec![
							//cctx.place(Expr::from_written(cctx, file, scopes, wexs.0), wex0_span),
							//cctx.place(Expr::from_written(cctx, file, scopes, wexs.1), wex1_span),
							Expr::from_written(cctx, file, scopes, wexs.0),
							Expr::from_written(cctx, file, scopes, wexs.1),
						])),
					}
				},
			},

			WrittenExpression::Ident(name, range) => Expr::Use {
				place: scopes.resolve_or_err(cctx, name, Span::new(file, range)),
			},
			WrittenExpression::NameQualified(wex, ident, _) => Expr::TakeNamedElement {
				ex: Box::new(Expr::from_written(cctx, file, scopes, *wex)),
				idx: ident,
			},
			WrittenExpression::IndexQualified(wex, index, _) => Expr::TakeUnnamedElement {
				ex: Box::new(Expr::from_written(cctx, file, scopes, *wex)),
				idx: index,
			},

			WrittenExpression::IntegerLiteral(value, _) => Expr::Literal(Literal::I64(value)),

			WrittenExpression::Region { region_inner, bracket_kind, range } =>
				Expr::from_written_region(cctx, file, scopes, region_inner, bracket_kind, range),

			WrittenExpression::IfThenElse { parts, .. } => {
				let [predicate, true_case, false_case] = *parts;
				//let predicate_span = predicate.span_in(file);
				//let true_case_span = true_case.span_in(file);
				//let false_case_span = false_case.span_in(file);
				Expr::PrimIfThenElse {
					predicate: Box::new(Expr::from_written(cctx, file, scopes, predicate)),
					true_case: Box::new(Expr::from_written(cctx, file, scopes, true_case)),
					false_case: Box::new(Expr::from_written(cctx, file, scopes, false_case)),
				}
			},

			WrittenExpression::Error(_) => Expr::Error,
		}
	}

	fn from_written_region(
		cctx: &'cctx CompileContext<'cctx>,
		file: File<'cctx>,
		scopes: Option<&Scope<'cctx, '_>>,
		region_inner: RegionInner<'cctx>,
		bracket_kind: BracketKind,
		_range: Range<u32>,
	) -> Self {
		match (region_inner, bracket_kind) {
			(RegionInner::Undelimited(None), BracketKind::Parenthesis) =>
				Expr::Tuple(Vec::new()),

			(RegionInner::Undelimited(Some(wex)), BracketKind::Parenthesis) => match wex.head {
				None => Expr::from_written(cctx, file, scopes, wex.expr),
				Some(_head) => todo!(),
			},

			(RegionInner::Delimited(wstates, Delimiter::Comma), BracketKind::Parenthesis) => {
				let wexs = wstates.into_iter().map(|WrittenStatement { head, expr }| {
					if let Some(head) = head {
						log::error(log::frame(String::from("don't understand statement in tuple expression"))
							.with_annotation(String::from("binding here in tuple"), head.span_in(file))
						).emit(cctx);
					}
					//let expr_span = expr.span_in(file);
					//cctx.place(Expr::from_written(cctx, file, scopes, expr), expr_span)
					Expr::from_written(cctx, file, scopes, expr)
				}).collect();
				Expr::Tuple(wexs)
			},
			/*
			(RegionInner::Delimited(exs, Delimiter::Comma), BracketKind::Parenthesis) => Expr::IntrTimes(
				exs.into_iter().map(|ex| Expr::from_written(scopes, ex)).collect()
			),
			*/
			(RegionInner::Delimited(wstates, Delimiter::Comma), BracketKind::CurlyBracket) => {
				/*
				let (each_names, bound_wexs)
					: (Vec<HashMap<_, _>>, Vec<(_, _)>)
					= wstates.into_iter().map(|WrittenStatement { head, expr }| {
						let place = cctx.new_place(None, None, expr.span_in(file));
						if head.is_none() {
							log::error(log::frame(String::from("struct members must be bound"))
								.with_annotation(String::from("expected assignment here"), expr.span_in(file))
							).emit(cctx);
						}
						let Binds { named, drops } = Binds::from_maybe_written(
							cctx, file, scopes, head,
							Expr::Use { place }, expr.span_in(file),
						);
						// TODO what to do with the dropped expressions
						let _drops = drops;
						(named, (place, expr))
					}).unzip();

				let reserving = each_names.iter().map(|ps| ps.len()).sum();
				let mut places = HashMap::with_capacity(reserving);
				// not just using extend for duplicate definition handling
				for ps in each_names {
					for (name, place) in ps {
						places.try_insert(name, place).unwrap_or_else(|err| {
							// TODO eventually not fatal probably, just set to an error place once i
							// create a standard place for that
							log::error(log::frame(format!("duplicate definitions of `{}` in the same scope", name.string()))
								.with_annotation(String::from("encountered here"), place.to_ref().span.clone())
								.with_annotation(format!("`{}` previously defined here", name.string()), err.entry.get().to_ref().span.clone())
								.with_note(String::from("if you wish to shadow an existing variable, you can use an ordered scope"))
							).emit_fatal(cctx, log::exit_code(30))
						});
					}
				}

				//eprintln!("{:?}", places);

				let scope = Scope::new(places, None, false, scopes);
				for (bind, wex) in bound_wexs {
					let expr = Expr::from_written(cctx, file, Some(&scope), wex);
					//eprintln!("{:?}", expr);
					let _ = bind.to_ref().expr.set(expr);
				}

				let exprs = scope.places.into_iter().map(|(name, place)|
					//(name, Expr::Use(place))
					(name, place)
				).collect();
				Expr::Struct(exprs)
				*/
				let mut members = HashMap::new();
				for WrittenStatement { head, expr } in wstates.into_iter() {
					match head {
						Some(WrittenExpression::Ident(ident, ident_range)) => match members.entry(ident) {
							Entry::Vacant(entry) => { entry.insert(Expr::from_written(cctx, file, scopes, expr)); },

							Entry::Occupied(_entry) => log::error(log::frame(format!("duplicate definitions of `{}` in the same scope", ident.string()))
								.with_annotation(String::from("repeat definition encountered here"), Span::new(file, ident_range))
							).emit_fatal(cctx, log::exit_code(30)),
						},
						Some(head) => log::error(log::frame(String::from("assignment in struct declaration must be to member name"))
							.with_annotation(String::from("encountered here"), head.span_in(file))
						).emit_fatal(cctx, log::exit_code(29)),
						None => log::error(log::frame(String::from("expected name for struct member"))
							.with_annotation(String::from("encountered here"), expr.span_in(file))
						).emit_fatal(cctx, log::exit_code(29)),
					}
				}
				Expr::Struct(members)
			},

			(RegionInner::Delimited(mut wstates, Delimiter::Semicolon), BracketKind::CurlyBracket) => {
				let mut scope = Scope::new(HashMap::new(), None, true, scopes);

				let final_state = match wstates.pop() {
					None => {
						log::error(log::frame(String::from("empty blocks are not supported"))
							// TODO annotation
						).emit(cctx);
						return Expr::Error
					},
					Some(state) => state,
				};

				for WrittenStatement { head, expr } in wstates {
					let span = expr.span_in(file);
					let expr = Expr::from_written(cctx, file, Some(&scope), expr);
					let Binds { named, drops } = Binds::from_maybe_written(cctx, file, scopes, head, expr, span);
					scope.places.extend(named);
					// TODO what to do with drops again
					let _drops = drops;
				}

				if final_state.head.is_some() {
					log::error(log::frame(String::from("binding the finished value of a scope is not supported"))
						// TODO annotation
					).emit(cctx);
				}
				Expr::from_written(cctx, file, Some(&scope), final_state.expr)
			},

			_ => todo!(),
		}
	}

	fn read_module(
		cctx: &'cctx CompileContext<'cctx>,
		file: File<'cctx>,
		scopes: Option<&Scope<'cctx, '_>>,
		written_expr: WrittenExpression<'cctx>,
	) -> Self {
		if let WrittenExpression::Region { region_inner, bracket_kind: BracketKind::CurlyBracket, range: _ } = written_expr {
			match region_inner {
				RegionInner::Undelimited(None) => Self::Module {
					public: HashMap::new(),
					private: Vec::new(),
				},
				RegionInner::Undelimited(Some(_wstate)) => todo!(),
				RegionInner::Delimited(wstates, Delimiter::Comma) => {
					let (each_names, bound_wexs)
						: (Vec<HashMap<_, _>>, Vec<(_, _)>)
						= wstates.into_iter().map(|WrittenStatement { head, expr }| {
							let place = cctx.new_place(None, None, expr.span_in(file));
							if head.is_none() {
								log::error(log::frame(String::from("module members must be bound"))
									.with_annotation(String::from("expected assignment here"), expr.span_in(file))
								).emit(cctx);
							}
							let Binds { named, drops } = Binds::from_maybe_written(
								cctx, file, scopes, head,
								Expr::Use { place }, expr.span_in(file),
							);
							// TODO what to do with the dropped expressions
							let _drops = drops;
							(named, (place, expr))
						}).unzip();

					let reserving = each_names.iter().map(|ps| ps.len()).sum();
					let mut places = HashMap::with_capacity(reserving);
					// not just using extend for duplicate definition handling
					for ps in each_names {
						for (name, place) in ps {
							places.try_insert(name, place).unwrap_or_else(|err| {
								// TODO eventually not fatal probably, just set to an error place once i
								// create a standard place for that
								log::error(log::frame(format!("duplicate definitions of `{}` in the same scope", name.string()))
									.with_annotation(String::from("encountered here"), place.to_ref().span.clone())
									.with_annotation(format!("`{}` previously defined here", name.string()), err.entry.get().to_ref().span.clone())
									.with_note(String::from("if you wish to shadow an existing variable, you can use an ordered scope"))
								).emit_fatal(cctx, log::exit_code(30))
							});
						}
					}

					//eprintln!("{:?}", places);

					let scope = Scope::new(places, None, false, scopes);
					for (bind, wex) in bound_wexs {
						let expr = Expr::from_written(cctx, file, Some(&scope), wex);
						//eprintln!("{:?}", expr);
						let _ = bind.to_ref().expr.set(expr);
					}

					let exprs = scope.places.into_iter().map(|(name, place)|
						//(name, Expr::Use(place))
						(name, place)
					).collect();
					Expr::Module { public: exprs, private: Vec::new() }
				},
				RegionInner::Delimited(_wstates, Delimiter::Semicolon) => todo!(),
				_ => todo!(),
			}
		} else {
			todo!()
		}
	}
}



pub struct Environment<T> {
	caps: T,
	arg: Option<T>,
}

/// Runs through entire iterator, collecting the last error if any are returned
fn collect_err<E, I>(iter: I) -> Result<(), E>
where I: IntoIterator<Item = Result<(), E>> {
	let mut err = None;
	for i in iter {
		if let Err(e) = i {
			err = Some(e);
		}
	}
	match err {
		None => Ok(()),
		Some(e) => Err(e),
	}
}

//impl<'cctx, HeadUnnorm, RestUnnorm> Expr<'cctx, HeadUnnorm, RestUnnorm>
//where HeadUnnorm: Proposition, RestUnnorm: Proposition {
impl<'cctx> Expr<'cctx> {
	// TODO scope checking
	/*
	pub fn synth(
		&self,
		ectx: &mut EvaluationContext<'cctx>,
		bounds: &mut Vec<Bound<'cctx>>,
	) -> Option<Expr<'cctx>> {
		Some(match self {
			Expr::Literal(Literal::TypeType | Literal::BoolType | Literal::I64Type)
				=> Expr::Literal(Literal::TypeType),
			Expr::Literal(Literal::Bool(_)) => Expr::Literal(Literal::BoolType),
			Expr::Literal(Literal::I64(_)) => Expr::Literal(Literal::I64Type),

			Expr::Ascribe { ex: _, ty } => {
				// TODO reenable this again once `check` is better
				/*
				if !ex.check(ectx, (**ty).clone()) {
					todo!();
				}
				*/
				/*
				bounds.push(Bound {
					lower: Property::TypeOf((**ex).clone()),
					upper: Property::ValueOf((**ty).clone()),
				});
				*/
				(**ty).clone()
				/*
				((**ty).clone(), vec![Bound {
					lower: Property::TypeOf(Expr::Use(ex)),
					upper: Property::ValueOf(
				*/
			},

			Expr::Use { place } => return ectx.synth(*place, bounds),

//			Expr::MakeStored => todo!(),
//			Expr::TakeStored(_ex) => todo!(),
//			Expr::PutStored(_into, _ex) => todo!(),
//			Expr::InOrder(_first, _second) => todo!(),

			Expr::FunctionType(_) => Expr::Literal(Literal::TypeType),
			//Expr::Function { .. } => todo!(),
			Expr::Function { .. } => return None,
			/*
			Expr::Function { body, data } => {
				let data = data.synth(env);
				let data = match &data {
					Some(d) => Some(d),
					None => None,
				};
				body.synth(data, None)?
			}
			*/
			Expr::Call { func, arg: _ } => {
				let mut func_type = func.synth(ectx, bounds)?;
				func_type.head_reduce(ectx).ok()?;
				let Expr::FunctionType(es) = func_type else { todo!() };
				let mut es = *es;
				es.head_reduce(ectx).ok()?;
				let Expr::Tuple(es) = es else { todo!() };
				let Ok([_from, to]) = <[Expr; 2]>::try_from(es) else { todo!() };
				/*
				bounds.push(Bound {
					lower: Property::TypeOf((**arg).clone()),
					upper: Property::ValueOf(from),
				});
				*/
				to
			},
			Expr::Argument => todo!(),
			Expr::Captures => todo!(),

			Expr::UnnamedAggregateType(_, _) => Expr::Literal(Literal::TypeType),
			//Expr::Tuple(ex) => TypeKind::Tuple(ex.map(|e| e.synth(env)).collect()?),
			Expr::Tuple(exs) => Expr::UnnamedAggregateType(
				Connective::Times,
				//places.iter().map(|p| p.to_ref().expr.get()?.synth(env)).collect::<Option<_>>()?,
				exs.iter().map(|ex| ex.synth(ectx, bounds)).collect::<Option<_>>()?,
			),

			Expr::NamedAggregateType(_, _) => Expr::Literal(Literal::TypeType),
			Expr::Struct(exs) => Expr::NamedAggregateType(
				Connective::Times,
				//places.iter().map(|(n, p)| Some((*n, p.to_ref().expr.get()?.synth(env)?))).collect::<Option<_>>()?
				exs.iter().map(|(n, ex)| Some((*n, ex.synth(ectx, bounds)?))).collect::<Option<_>>()?
			),

			Expr::ModuleType(_) => Expr::Literal(Literal::TypeType),
			Expr::Module { public, .. } => Expr::ModuleType(
				// TODO does this need to go through ectx.synth()?
				public.iter().map(|(n, p)| Some((*n, p.to_ref().expr.get()?.synth(ectx, bounds)?))).collect::<Option<_>>()?
			),
			Expr::ModuleV(exs) => Expr::ModuleType(
				exs.iter().map(|(n, ex)| Some((*n, ex.synth(ectx, bounds)?))).collect::<Option<_>>()?
			),

			Expr::TakeUnnamedElement { ex, idx } => match ex.synth(ectx, bounds)? {
				Expr::UnnamedAggregateType(Connective::Times, mut ts) => ts.remove(*idx),
				_ => todo!(),
			},
			Expr::TakeNamedElement { ex, idx } => match ex.synth(ectx, bounds)? {
				Expr::NamedAggregateType(Connective::Times, mut ts) => ts.remove(idx).unwrap(),
				Expr::ModuleType(mut ts) => ts.remove(idx).unwrap(),
				_ => todo!(),
			},

			Expr::BinaryOp { kind: BinOp::IntAdd, operands: _ } => Expr::Literal(Literal::I64Type),
			Expr::BinaryOp { kind: BinOp::IntMul, operands: _ } => Expr::Literal(Literal::I64Type),

			Expr::PrimIfThenElse { .. } => todo!(),

			Expr::Unknown { .. } => todo!(),
			Expr::UnnamedTypeElement { .. } => todo!(),
			Expr::NamedTypeElement { .. } => todo!(),

			Expr::Error => todo!(),
		})
	}
	*/

	pub fn reduce(&mut self, ectx: &mut EvaluationContext<'cctx>) -> log::Result<()> {
		Ok(match self {
			Expr::Literal(_) => (),
			Expr::Ascribe { ex, ty: _ } => { *self = mem::take(&mut **ex); return self.reduce(ectx) },
			Expr::Use { place } => *self = ectx.reduce(*place)?,

			Expr::FunctionType(exprs) => return exprs.reduce(ectx),
			Expr::Function { body: _, data } => return data.reduce(ectx),
			Expr::Call { func, arg } => {
				collect_err([func.reduce(ectx), arg.reduce(ectx)])?;
				match &mut **func {
					Expr::Function { body, data } => {
						let mut body = mem::take(&mut **body);
						let caps = mem::take(&mut **data);
						let arg = Some(mem::take(&mut **arg));
						body.apply(ectx, &Environment { caps, arg });
						*self = body;
						return self.reduce(ectx)
					},
					_ => todo!(),
				}
			},

			Expr::Argument => (),
			Expr::Captures => (),

			Expr::UnnamedAggregateType(_conn, types) => return collect_err(types.iter_mut().map(|t| t.reduce(ectx))),
			Expr::Tuple(exprs) => return collect_err(exprs.iter_mut().map(|e| e.reduce(ectx))),

			Expr::NamedAggregateType(_conn, types) => return collect_err(types.values_mut().map(|t| t.reduce(ectx))),
			Expr::Struct(exprs) => return collect_err(exprs.values_mut().map(|e| e.reduce(ectx))),

			Expr::ModuleType(types) => return collect_err(types.values_mut().map(|t| t.reduce(ectx))),
			Expr::Module { public, private: _ } => {
				let public: log::Result<_> = public.iter().map(|(k, e)| Ok((*k, ectx.reduce(*e)?))).collect();
				*self = Expr::ModuleV(public?);
			},
			Expr::ModuleV(exprs) => return collect_err(exprs.values_mut().map(|e| e.reduce(ectx))),

			Expr::TakeUnnamedElement { ex, idx } => {
				ex.head_reduce(ectx)?;
				match &mut **ex {
					Expr::Tuple(tuple) => {
						*self = mem::take(tuple.get_mut(*idx).unwrap());
						return self.reduce(ectx)
					},
					_ => todo!(),
				}
			},
			Expr::TakeNamedElement { ex, idx } => {
				ex.head_reduce(ectx)?;
				match &mut **ex {
					Expr::Struct(structure) => {
						*self = mem::take(structure.get_mut(idx).unwrap());
						return self.reduce(ectx)
					},
					Expr::ModuleV(public) => {
						*self = mem::take(public.get_mut(idx).unwrap());
						return self.reduce(ectx)
					},
					_ => todo!(),
				}
			},

			Expr::BinaryOp { kind, operands } => {
				let mut operands = mem::take(&mut **operands);
				operands.reduce(ectx)?;
				let Expr::Tuple(operands) = operands else { unreachable!() };
				let Ok([lhs, rhs]) = <[Expr; 2]>::try_from(operands) else { unreachable!() };
				match kind {
					BinOp::IntAdd => {
						let Expr::Literal(Literal::I64(lhs)) = lhs else { todo!() };
						let Expr::Literal(Literal::I64(rhs)) = rhs else { todo!() };
						*self = Expr::Literal(Literal::I64(lhs + rhs));
					},
					BinOp::IntMul => {
						let Expr::Literal(Literal::I64(lhs)) = lhs else { todo!() };
						let Expr::Literal(Literal::I64(rhs)) = rhs else { todo!() };
						*self = Expr::Literal(Literal::I64(lhs * rhs));
					},
				}
			}
			Expr::PrimIfThenElse { predicate, true_case, false_case } => {
				let mut predicate = mem::take(&mut **predicate);
				predicate.reduce(ectx)?;
				let Expr::Literal(Literal::Bool(predicate)) = predicate else { todo!() };
				if predicate {
					*self = mem::take(&mut **true_case);
					return self.reduce(ectx)
				} else {
					*self = mem::take(&mut **false_case);
					return self.reduce(ectx)
				}
			},

			Expr::Unknown { .. } => (),
			Expr::UnnamedTypeElement { ty, idx } => {
				ty.head_reduce(ectx)?;
				match &mut **ty {
					Expr::UnnamedAggregateType(Connective::Times, types) => {
						*self = mem::take(types.get_mut(*idx).unwrap());
						return self.reduce(ectx)
					}
					_ => todo!(),
				}
			},
			Expr::NamedTypeElement { ty, idx } => {
				ty.head_reduce(ectx)?;
				match &mut **ty {
					Expr::NamedAggregateType(Connective::Times, types) => {
						*self = mem::take(types.get_mut(idx).unwrap());
						return self.reduce(ectx)
					}
					Expr::ModuleType(public) => {
						*self = mem::take(public.get_mut(idx).unwrap());
						return self.reduce(ectx)
					}
					_ => todo!(),
				}
			},

			Expr::Error => (),
		})
	}

	pub fn head_reduce(&mut self, ectx: &mut EvaluationContext<'cctx>) -> log::Result<()> {
		Ok(match self {
			Expr::Literal(_) => (),
			Expr::Ascribe { ex, ty: _ } => { *self = mem::take(&mut **ex); return self.head_reduce(ectx) },
			Expr::Use { place } => *self = ectx.head_reduce(*place)?,

			Expr::FunctionType(_exprs) => (),
			Expr::Function { body: _, data: _ } => (),
			Expr::Call { func, arg } => {
				func.head_reduce(ectx)?;
				match &mut **func {
					Expr::Function { body, data } => {
						let mut body = mem::take(&mut **body);
						let caps = mem::take(&mut **data);
						let arg = Some(mem::take(&mut **arg));
						body.apply(ectx, &Environment { caps, arg });
						*self = body;
						return self.head_reduce(ectx)
					},
					_ => todo!(),
				}
			},

			Expr::Argument => (),
			Expr::Captures => (),

			Expr::UnnamedAggregateType(_conn, _types) => (),
			Expr::Tuple(_exprs) => (),

			Expr::NamedAggregateType(_conn, _types) => (),
			Expr::Struct(_exprs) => (),

			Expr::ModuleType(_types) => (),
			Expr::Module { public, private: _ } =>
				*self = Expr::ModuleV(public.iter().map(|(k, e)| (*k, e.to_ref().expr.get().unwrap().clone())).collect()),
			Expr::ModuleV(_exprs) => (),

			Expr::TakeUnnamedElement { ex, idx } => {
				ex.head_reduce(ectx)?;
				match &mut **ex {
					Expr::Tuple(tuple) => {
						*self = mem::take(tuple.get_mut(*idx).unwrap());
						return self.head_reduce(ectx)
					},
					_ => todo!(),
				}
			},
			Expr::TakeNamedElement { ex, idx } => {
				ex.head_reduce(ectx)?;
				match &mut **ex {
					Expr::Struct(structure) => {
						*self = mem::take(structure.get_mut(idx).unwrap());
						return self.head_reduce(ectx)
					},
					Expr::ModuleV(public) => {
						*self = mem::take(public.get_mut(idx).unwrap());
						return self.head_reduce(ectx)
					},
					_ => todo!(),
				}
			},

			Expr::BinaryOp { kind, operands } => {
				let mut operands = mem::take(&mut **operands);
				operands.reduce(ectx)?;
				let Expr::Tuple(operands) = operands else { unreachable!() };
				let Ok([lhs, rhs]) = <[Expr; 2]>::try_from(operands) else { unreachable!() };
				match kind {
					BinOp::IntAdd => {
						let Expr::Literal(Literal::I64(lhs)) = lhs else { todo!() };
						let Expr::Literal(Literal::I64(rhs)) = rhs else { todo!() };
						*self = Expr::Literal(Literal::I64(lhs + rhs));
					},
					BinOp::IntMul => {
						let Expr::Literal(Literal::I64(lhs)) = lhs else { todo!() };
						let Expr::Literal(Literal::I64(rhs)) = rhs else { todo!() };
						*self = Expr::Literal(Literal::I64(lhs * rhs));
					},
				}
			}
			Expr::PrimIfThenElse { predicate, true_case, false_case } => {
				let mut predicate = mem::take(&mut **predicate);
				predicate.reduce(ectx)?;
				let Expr::Literal(Literal::Bool(predicate)) = predicate else { todo!() };
				if predicate {
					*self = mem::take(&mut **true_case);
					return self.head_reduce(ectx)
				} else {
					*self = mem::take(&mut **false_case);
					return self.head_reduce(ectx)
				}
			},

			Expr::Unknown { .. } => (),
			Expr::UnnamedTypeElement { ty, idx } => {
				ty.head_reduce(ectx)?;
				match &mut **ty {
					Expr::UnnamedAggregateType(Connective::Times, types) => {
						*self = mem::take(types.get_mut(*idx).unwrap());
						return self.head_reduce(ectx)
					}
					_ => todo!(),
				}
			},
			Expr::NamedTypeElement { ty, idx } => {
				ty.head_reduce(ectx)?;
				match &mut **ty {
					Expr::NamedAggregateType(Connective::Times, types) => {
						*self = mem::take(types.get_mut(idx).unwrap());
						return self.head_reduce(ectx)
					}
					Expr::ModuleType(public) => {
						*self = mem::take(public.get_mut(idx).unwrap());
						return self.head_reduce(ectx)
					}
					_ => todo!(),
				}
			},

			Expr::Error => (),
		})
	}

	/*
	pub fn synth_bounds<'expr>(
		&'expr self,
		ectx: &mut EvaluationContext<'cctx>,
		env: Option<&Environment<Expr<'cctx>>>,
		bounds: &mut BoundManager<'cctx, 'expr>,
	) -> Expr<'cctx> {
		match self {
			Expr::Literal(Literal::TypeType | Literal::BoolType | Literal::I64Type)
				=> Expr::Literal(Literal::TypeType),
			Expr::Literal(Literal::Bool(_)) => Expr::Literal(Literal::BoolType),
			Expr::Literal(Literal::I64(_)) => Expr::Literal(Literal::I64Type),

			Expr::Ascribe { ex, ty } => {
				let ty = &**ty;
				let ex_bound = ex.synth_bounds(ectx, env, bounds);
				bounds.add_bound(Bound {
					lower: ex_bound,
					upper: ty.clone(),
				});
				let ty_bound = ty.synth_bounds(ectx, env, bounds);
				bounds.add_bound(Bound {
					lower: ty_bound,
					upper: Expr::Literal(Literal::TypeType),
				});
				ty.clone()
			},
			Expr::Use { place } => bounds.place_synth(ectx, env, *place),

			Expr::FunctionType(exprs) => {
				let inner = exprs.synth_bounds(ectx, env, bounds);
				bounds.add_bound(Bound {
					lower: inner,
					upper: Expr::UnnamedAggregateType(Connective::Times,
						vec![Expr::Literal(Literal::TypeType), Expr::Literal(Literal::TypeType)],
					),
				});
				Expr::Literal(Literal::TypeType)
			},
			Expr::Function { body, data } => {
				let data_bound = data.synth_bounds(ectx, env, bounds);
				let arg_unknown = bounds.new_unknown(None);
				let env = Environment {
					arg: Some(Expr::Unknown { id: arg_unknown }),
					caps: data_bound,
				};
				let body_bound = body.synth_bounds(ectx, Some(&env), bounds);
				// potential issue: body bound can contain local information
				//   once dependent types are available
				Expr::FunctionType(Box::new(Expr::Tuple(vec![
					Expr::Unknown { id: arg_unknown },
					body_bound,
				])))
			},
			Expr::Call { func, arg } => {
				let func_bound = func.synth_bounds(ectx, env, bounds);
				let arg_bound = arg.synth_bounds(ectx, env, bounds);
				let return_unknown = bounds.new_unknown(Some(self));
				bounds.add_bound(Bound {
					lower: func_bound,
					upper: Expr::FunctionType(Box::new(Expr::Tuple(vec![
						arg_bound, Expr::Unknown { id: return_unknown },
					]))),
				});
				Expr::Unknown { id: return_unknown }
			},

			Expr::Argument => env.unwrap().arg.as_ref().unwrap().clone(),
			Expr::Captures => env.unwrap().caps.clone(),

			Expr::UnnamedAggregateType(_conn, types) => {
				for ty in types.iter() {
					let ty_bound = ty.synth_bounds(ectx, env, bounds);
					bounds.add_bound(Bound {
						lower: ty_bound,
						upper: Expr::Literal(Literal::TypeType),
					});
				}
				Expr::Literal(Literal::TypeType)
			},
			Expr::Tuple(exprs) => {
				let exprs_bounds = exprs.iter().map(|e| e.synth_bounds(ectx, env, bounds));
				Expr::UnnamedAggregateType(Connective::Times, exprs_bounds.collect())
			},

			Expr::NamedAggregateType(_conn, types) => {
				for ty in types.values() {
					let ty_bound = ty.synth_bounds(ectx, env, bounds);
					bounds.add_bound(Bound {
						lower: ty_bound,
						upper: Expr::Literal(Literal::TypeType),
					});
				}
				Expr::Literal(Literal::TypeType)
			},
			Expr::Struct(exprs) => {
				let exprs_bounds = exprs.iter().map(|(k, e)| (*k, e.synth_bounds(ectx, env, bounds)));
				Expr::NamedAggregateType(Connective::Times, exprs_bounds.collect())
			},

			Expr::ModuleType(types) => {
				for ty in types.values() {
					let ty_bound = ty.synth_bounds(ectx, env, bounds);
					bounds.add_bound(Bound {
						lower: ty_bound,
						upper: Expr::Literal(Literal::TypeType),
					});
				}
				Expr::Literal(Literal::TypeType)
			},
			Expr::Module { public, private } => {
				private.iter().for_each(|e| { bounds.place_synth(ectx, env, *e); });
				let public_bounds = public.iter().map(|(k, e)| (*k, bounds.place_synth(ectx, env, *e)));
				Expr::ModuleType(public_bounds.collect())
			},
			Expr::ModuleV(exprs) => {
				let exprs_bounds = exprs.iter().map(|(k, e)| (*k, e.synth_bounds(ectx, env, bounds)));
				Expr::ModuleType(exprs_bounds.collect())
			},

			Expr::TakeUnnamedElement { ex, idx } => {
				let ex_bounds = ex.synth_bounds(ectx, env, bounds);
				Expr::UnnamedTypeElement { ty: Box::new(ex_bounds), idx: *idx }
			},
			Expr::TakeNamedElement { ex, idx } => {
				let ex_bounds = ex.synth_bounds(ectx, env, bounds);
				Expr::NamedTypeElement { ty: Box::new(ex_bounds), idx: *idx }
			},

			Expr::BinaryOp { kind, operands } => {
				let operands_bounds = operands.synth_bounds(ectx, env, bounds);
				match kind {
					BinOp::IntAdd | BinOp::IntMul => {
						bounds.add_bound(Bound {
							lower: operands_bounds,
							upper: Expr::UnnamedAggregateType(Connective::Times, vec![
								Expr::Literal(Literal::I64Type), Expr::Literal(Literal::I64Type)
							]),
						});
						Expr::Literal(Literal::I64Type)
					}
				}
			},
			Expr::PrimIfThenElse { predicate, true_case, false_case } => {
				let predicate_bound = predicate.synth_bounds(ectx, env, bounds);
				bounds.add_bound(Bound {
					lower: predicate_bound,
					upper: Expr::Literal(Literal::BoolType),
				});
				let true_case_bound = true_case.synth_bounds(ectx, env, bounds);
				let false_case_bound = false_case.synth_bounds(ectx, env, bounds);
				let branch_unknown = bounds.new_unknown(Some(self));
				bounds.add_bound(Bound {
					lower: true_case_bound,
					upper: Expr::Unknown { id: branch_unknown },
				});
				bounds.add_bound(Bound {
					lower: false_case_bound,
					upper: Expr::Unknown { id: branch_unknown },
				});
				Expr::Unknown { id: branch_unknown }
			},

			Expr::Unknown { .. } => unimplemented!(),
			Expr::UnnamedTypeElement { .. } => unimplemented!(),
			Expr::NamedTypeElement { .. } => unimplemented!(),

			Expr::Error => Expr::Error,
		}
	}
	*/

	pub fn unknown_deps(&self, ectx: &mut EvaluationContext<'cctx>, extend: &mut HashSet<usize>) {
		match self {
			Expr::Literal(_) => (),
			Expr::Ascribe { ex, ty } => { ex.unknown_deps(ectx, extend); ty.unknown_deps(ectx, extend); },
			Expr::Use { place } => return ectx.unknown_deps(*place, extend),

			Expr::FunctionType(ts) => ts.unknown_deps(ectx, extend),
			Expr::Function { body, data } => { body.unknown_deps(ectx, extend); data.unknown_deps(ectx, extend); },
			Expr::Call { func, arg } => { func.unknown_deps(ectx, extend); arg.unknown_deps(ectx, extend); },

			Expr::Argument => (),
			Expr::Captures => (),

			Expr::UnnamedAggregateType(_, tys) => for ty in tys { ty.unknown_deps(ectx, extend); },
			//Expr::Tuple(ex) => TypeKind::Tuple(ex.map(|e| e.synth(env)).collect()?),
			Expr::Tuple(exs) => for ex in exs { ex.unknown_deps(ectx, extend); },

			Expr::NamedAggregateType(_, tys) => for ty in tys.values() { ty.unknown_deps(ectx, extend); },
			Expr::Struct(exs) => for ex in exs.values() { ex.unknown_deps(ectx, extend); },

			Expr::ModuleType(tys) => for ty in tys.values() { ty.unknown_deps(ectx, extend); },
			Expr::Module { public, private } => {
				for p in private { ectx.unknown_deps(*p, extend); }
				for p in public.values() { ectx.unknown_deps(*p, extend); }
			},
			Expr::ModuleV(exs) => for ex in exs.values() { ex.unknown_deps(ectx, extend) },

			Expr::TakeUnnamedElement { ex, idx: _ } => ex.unknown_deps(ectx, extend),
			Expr::TakeNamedElement { ex, idx: _ } => ex.unknown_deps(ectx, extend),

			Expr::BinaryOp { kind: _, operands } => operands.unknown_deps(ectx, extend),

			Expr::PrimIfThenElse { predicate, true_case, false_case } => {
				predicate.unknown_deps(ectx, extend);
				true_case.unknown_deps(ectx, extend);
				false_case.unknown_deps(ectx, extend);
			},

			Expr::Unknown { id } => { extend.insert(*id); },
			Expr::UnnamedTypeElement { ty, idx: _ } => ty.unknown_deps(ectx, extend),
			Expr::NamedTypeElement { ty, idx: _ } => ty.unknown_deps(ectx, extend),

			Expr::Error => (),
		}
	}

	/*
	pub(crate) fn evaluate_ctx(
		self,
		ectx: &mut EvaluationContext<'cctx>,
		env: Option<&Environment<Expr<'cctx>>>,
	) -> Option<Expr<'cctx>> {
		// TODO replace some of the binary op nesting with if-let chains once available
		Some(match self {
			Expr::Literal(lit) => Expr::literal(lit),

			Expr::Ascribe { ex, ty: _ } => {
				// TODO proper checking
				// TODO type ascription
				return ex.evaluate_ctx(ectx, env);
			}

			Expr::Use { place } => return ectx.evaluate(place, env),

//			Expr::MakeStored => todo!(),
//			Expr::TakeStored(_) => todo!(),
//			Expr::PutStored(_, _) => todo!(),
//			Expr::InOrder(_, _) => todo!(),

			Expr::FunctionType(exprs) => if let Expr::Tuple(exs) = exprs.evaluate_ctx(ectx, env)?.expr {
				if let Ok(vs) = <[Expr; 2]>::try_from(exs) {
					/*
					if let [Expr::Type(from_ty), Expr::Type(to_ty)] = vs {
						Expr::Type(TypeKind::Function(Box::new((from_ty, to_ty))))
					} else {
						todo!()
					}
					*/
					Expr::function_type(Box::new(Expr { expr: Expr::Tuple(Vec::from(vs)) }))
				} else {
					todo!()
				}
			} else {
				todo!()
			},

			Expr::Function { body, data } => Expr::function(
				body,
				Box::new(data.evaluate_ctx(ectx, env)?),
			),
			Expr::Call { func, arg } => {
				match func.evaluate_ctx(ectx, env)?.expr {
					//Expr::function(body) => return cctx.require_evaluate(body, None, Some(&new_arg.evaluate_ctx(caps, arg)?)),
					//Expr::Closure { body, data } => return cctx.require_evaluate(body, Some(&data), Some(&(new_arg.evaluate_ctx(caps, arg)?))),
					Expr::Function { body, data } => {
						let env = Environment { caps: Expr { expr: *data }, arg: arg.evaluate_ctx(ectx, env)? };
						return body.evaluate_ctx(ectx, Some(&env))
					},

					v => {
						todo!();
					},
					//_ => todo!(),
				}
			},
			Expr::Argument => env?.arg.clone(),
			Expr::Captures => env?.caps.clone(),

			Expr::UnnamedAggregateType(conn, types) => Expr::unnamed_aggregate_type(
				conn,
				types.into_iter().map(|t| t.evaluate_ctx(ectx, env)).collect::<Option<_>>()?
			),
			Expr::Tuple(exprs) => Expr::tuple(
				//exprs.into_iter().map(|place| place.to_ref().expr.get().unwrap().clone().evaluate_ctx(env)).collect::<Option<_>>()?
				exprs.into_iter().map(|expr| expr.evaluate_ctx(ectx, env)).collect::<Option<_>>()?
			),

			Expr::NamedAggregateType(conn, types) => Expr::named_aggregate_type(
				conn,
				types.into_iter().map(|(n, t)| Some((n, t.evaluate_ctx(ectx, env)?))).collect::<Option<_>>()?
			),
			Expr::Struct(elems) => Expr::structure(
				//elems.into_iter().map(|(name, place)| Some((name, place.to_ref().expr.get().unwrap().clone().evaluate_ctx(env)?))).collect::<Option<_>>()?
				elems.into_iter().map(|(name, expr)| Some((name, expr.evaluate_ctx(ectx, env)?))).collect::<Option<_>>()?
			),

			Expr::ModuleType(types) => Expr::module_type(
				types.into_iter().map(|(n, t)| Some((n, t.evaluate_ctx(ectx, env)?))).collect::<Option<_>>()?
			),
			Expr::Module { public, private: _ } => Expr::module_v(
				public.into_iter().map(|(name, place)|
					Some((name, ectx.evaluate(place, env)?))
				).collect::<Option<_>>()?,
			),
			Expr::ModuleV(elems) => Expr::module_v(
				elems.into_iter().map(|(name, expr)| Some((name, expr.evaluate_ctx(ectx, env)?))).collect::<Option<_>>()?
			),

			Expr::TakeUnnamedElement { ex, idx } => match ex.head_evaluate(ectx, env.map(Expr::expr_env))? {
				Expr::Tuple(tuple) => return tuple.into_iter().nth(idx).unwrap().evaluate_ctx(ectx, env),
				_ => todo!(),
			},
			Expr::TakeNamedElement { ex, idx } => match ex.head_evaluate(ectx, env.map(Expr::expr_env))? {
				Expr::Struct(mut structure) => return structure.remove(&idx).unwrap().evaluate_ctx(ectx, env),
				Expr::ModuleV(mut public) => return public.remove(&idx).unwrap().evaluate_ctx(ectx, env),
				_ => todo!(),
			},

			Expr::BinaryOp { kind, operands } => if let Expr::Tuple(exs) = operands.evaluate_ctx(ectx, env)?.expr {
				if let Ok(vs) = <[Expr; 2]>::try_from(exs) {
					match kind {
						BinOp::IntAdd => if let [Expr::Literal(Literal::I64(lhs)), Expr::Literal(Literal::I64(rhs))] = vs {
							Expr::literal(Literal::I64(lhs + rhs))
						} else {
							todo!()
						},
						BinOp::IntMul => if let [Expr::Literal(Literal::I64(lhs)), Expr::Literal(Literal::I64(rhs))] = vs {
							Expr::literal(Literal::I64(lhs * rhs))
						} else {
							todo!()
						},
					}
				} else {
					unreachable!()
				}
			} else {
				unreachable!()
			},

			Expr::PrimIfThenElse { predicate, true_case, false_case } => match predicate.evaluate_ctx(ectx, env)?.expr {
				Expr::Literal(Literal::Bool(p)) => if p {
					return true_case.evaluate_ctx(ectx, env);
				} else {
					return false_case.evaluate_ctx(ectx, env);
				}

				_ => todo!(),
			},

			Expr::Error => Expr::ERROR,
		})
	}
	/// Normalize an [`Expr`] to a [`Expr`].
	pub fn evaluate(
		self,
	) -> Option<Expr<'cctx>> {
		self.evaluate_ctx(&mut EvaluationContext::new(), None)
	}
	*/

	pub(crate) fn apply(&mut self, ectx: &mut EvaluationContext<'cctx>, env: &Environment<Expr<'cctx>>) {
		match self {
			Expr::Literal(_) => (),
			Expr::Ascribe { ex, ty } => { ex.apply(ectx, env); ty.apply(ectx, env); },
			Expr::Use { place } => *self = ectx.apply(*place, env),

			Expr::FunctionType(exprs) => exprs.apply(ectx, env),
			Expr::Function { body: _, data } => data.apply(ectx, env),
			Expr::Call { func, arg } => { func.apply(ectx, env); arg.apply(ectx, env); },

			Expr::Argument => if let Some(arg) = &env.arg { *self = arg.clone() },
			Expr::Captures => *self = env.caps.clone(),

			Expr::UnnamedAggregateType(_conn, types) => types.iter_mut().for_each(|t| t.apply(ectx, env)),
			Expr::Tuple(exprs) => exprs.iter_mut().for_each(|e| e.apply(ectx, env)),

			Expr::NamedAggregateType(_conn, types) => types.values_mut().for_each(|t| t.apply(ectx, env)),
			Expr::Struct(exprs) => exprs.values_mut().for_each(|e| e.apply(ectx, env)),

			Expr::ModuleType(types) => types.values_mut().for_each(|t| t.apply(ectx, env)),
			Expr::Module { public, private: _ } =>
				*self = Expr::ModuleV(public.iter().map(|(k, e)| (*k, ectx.apply(*e, env))).collect()),
			Expr::ModuleV(exprs) => exprs.values_mut().for_each(|e| e.apply(ectx, env)),

			Expr::TakeUnnamedElement { ex, idx: _ } => ex.apply(ectx, env),
			Expr::TakeNamedElement { ex, idx: _ } => ex.apply(ectx, env),

			Expr::BinaryOp { kind: _, operands } => operands.apply(ectx, env),
			Expr::PrimIfThenElse { predicate, true_case, false_case } => {
				predicate.apply(ectx, env);
				true_case.apply(ectx, env);
				false_case.apply(ectx, env);
			},

			Expr::Unknown { .. } => (),
			Expr::UnnamedTypeElement { ty, idx: _ } => ty.apply(ectx, env),
			Expr::NamedTypeElement { ty, idx: _ } => ty.apply(ectx, env),

			Expr::Error => (),
		}
	}

	/*
	pub(crate) fn fill_solutions<'expr>(&mut self, ectx: &mut EvaluationContext<'cctx>, solns: &Unknowns<'cctx, 'expr>) {
		match self {
			Expr::Literal(_) => (),
			Expr::Ascribe { ex, ty } => { ex.fill_solutions(ectx, solns); ty.fill_solutions(ectx, solns); },
			Expr::Use { place } => *self = ectx.fill_solutions(*place, solns),

			Expr::FunctionType(exprs) => exprs.fill_solutions(ectx, solns),
			Expr::Function { body, data } => { body.fill_solutions(ectx, solns); data.fill_solutions(ectx, solns); },
			Expr::Call { func, arg } => { func.fill_solutions(ectx, solns); arg.fill_solutions(ectx, solns); },

			Expr::Argument => (),
			Expr::Captures => (),

			Expr::UnnamedAggregateType(_conn, types) => types.iter_mut().for_each(|t| t.fill_solutions(ectx, solns)),
			Expr::Tuple(exprs) => exprs.iter_mut().for_each(|e| e.fill_solutions(ectx, solns)),

			Expr::NamedAggregateType(_conn, types) => types.values_mut().for_each(|t| t.fill_solutions(ectx, solns)),
			Expr::Struct(exprs) => exprs.values_mut().for_each(|e| e.fill_solutions(ectx, solns)),

			Expr::ModuleType(types) => types.values_mut().for_each(|t| t.fill_solutions(ectx, solns)),
			Expr::Module { public, private: _ } =>
				*self = Expr::ModuleV(public.iter().map(|(k, e)| (*k, ectx.fill_solutions(*e, solns))).collect()),
			Expr::ModuleV(exprs) => exprs.values_mut().for_each(|e| e.fill_solutions(ectx, solns)),

			Expr::TakeUnnamedElement { ex, idx: _ } => ex.fill_solutions(ectx, solns),
			Expr::TakeNamedElement { ex, idx: _ } => ex.fill_solutions(ectx, solns),

			Expr::BinaryOp { kind: _, operands } => operands.fill_solutions(ectx, solns),
			Expr::PrimIfThenElse { predicate, true_case, false_case } => {
				predicate.fill_solutions(ectx, solns);
				true_case.fill_solutions(ectx, solns);
				false_case.fill_solutions(ectx, solns);
			},

			Expr::Unknown { id } => if let Some(soln) = solns.solution(*id) {
				*self = soln.clone();
				self.fill_solutions(ectx, solns);
			},
			Expr::UnnamedTypeElement { ty, idx: _ } => ty.fill_solutions(ectx, solns),
			Expr::NamedTypeElement { ty, idx: _ } => ty.fill_solutions(ectx, solns),

			Expr::Error => (),
		}
	}
	*/

	/*
	pub(crate) fn head_evaluate(
		self,
		ectx: &mut EvaluationContext<'cctx>,
		env: Option<&Environment<Expr<'cctx>>>,
	) -> Option<Expr<'cctx>> {
		Some(match self {
			Expr::Literal(lit) => Expr::Literal(lit),
			Expr::Ascribe { ex, ty: _ } => return ex.head_evaluate(ectx, env),
			Expr::Use { place } => return ectx.head_evaluate(place, env),

			Expr::FunctionType(exprs) => Expr::FunctionType(exprs),
			Expr::Function { body, data } => Expr::Function { body, data },
			Expr::Call { func, arg } => {
				match func.head_evaluate(ectx, env)? {
					Expr::Function { mut body, data } => {
						// TODO maybe lazy eval for this?
						let env = Environment { caps: *data, arg: *arg };
						//return body.head_evaluate(ectx, Some(&env))
						// does this actually work?
						body.apply(ectx, &env);
						return body.head_evaluate(ectx, None);
					},

					_ => todo!(),
				}
			},

			Expr::Argument => return env?.arg.clone().head_evaluate(ectx, env),
			Expr::Captures => return env?.caps.clone().head_evaluate(ectx, env),

			Expr::UnnamedAggregateType(conn, types) => Expr::UnnamedAggregateType(conn, types),
			Expr::Tuple(exprs) => Expr::Tuple(exprs),

			Expr::NamedAggregateType(conn, types) => Expr::NamedAggregateType(conn, types),
			Expr::Struct(elems) => Expr::Struct(elems),

			Expr::ModuleType(types) => Expr::ModuleType(types),
			// TODO could the access of place data here be used to cause a compiler stall?
			Expr::Module { public, private: _ }
				=> Expr::ModuleV(public.into_iter().map(|(n, p)| (n, p.to_ref().expr.get().unwrap().clone())).collect()),
			Expr::ModuleV(elems) => Expr::ModuleV(elems),

			Expr::TakeUnnamedElement { ex, idx } => match ex.head_evaluate(ectx, env)? {
				Expr::Tuple(tuple) => return tuple.into_iter().nth(idx).unwrap().head_evaluate(ectx, env),
				_ => todo!(),
			},
			Expr::TakeNamedElement { ex, idx } => match ex.head_evaluate(ectx, env)? {
				Expr::Struct(mut structure) => return structure.remove(&idx).unwrap().head_evaluate(ectx, env),
				Expr::ModuleV(mut public) => return public.remove(&idx).unwrap().head_evaluate(ectx, env),
				_ => todo!(),
			},

			Expr::BinaryOp { kind: BinOp::IntAdd, operands } => if let Expr::Tuple(exs) = operands.head_evaluate(ectx, env)? {
				if let Ok([lhs, rhs]) = <[Expr; 2]>::try_from(exs) {
					let lhs = lhs.head_evaluate(ectx, env)?;
					let rhs = rhs.head_evaluate(ectx, env)?;
					if let [Expr::Literal(Literal::I64(lhs)), Expr::Literal(Literal::I64(rhs))] = [lhs, rhs] {
						Expr::Literal(Literal::I64(lhs + rhs))
					} else {
						todo!()
					}
				} else {
					unreachable!()
				}
			} else {
				unreachable!()
			},
			Expr::BinaryOp { kind: BinOp::IntMul, operands } => if let Expr::Tuple(exs) = operands.head_evaluate(ectx, env)? {
				if let Ok([lhs, rhs]) = <[Expr; 2]>::try_from(exs) {
					let lhs = lhs.head_evaluate(ectx, env)?;
					let rhs = rhs.head_evaluate(ectx, env)?;
					if let [Expr::Literal(Literal::I64(lhs)), Expr::Literal(Literal::I64(rhs))] = [lhs, rhs] {
						Expr::Literal(Literal::I64(lhs * rhs))
					} else {
						todo!()
					}
				} else {
					unreachable!()
				}
			} else {
				unreachable!()
			},

			Expr::PrimIfThenElse { predicate, true_case, false_case } => match predicate.head_evaluate(ectx, env)? {
				Expr::Literal(Literal::Bool(p)) => if p {
					return true_case.head_evaluate(ectx, env);
				} else {
					return false_case.head_evaluate(ectx, env);
				}

				_ => todo!(),
			},

			Expr::Error => Expr::Error,
		})
	}
	*/


	/*
	#[cfg(feature = "llvm")]
	pub fn codegen_dependencies(
		&self,
	) -> HashSet<QueryKind<'cctx>> {
		match self {
			Expr::Literal(_) => deps![],

			Expr::Ascribe { ex, ty: _ } => deps![codegen ex],

			Expr::Use { place } => std::iter::once(
				QueryKind::Evaluate { place: *place }
			).collect(),

			//Expr::FunctionType(tys) => deps![codegen tys],
			Expr::FunctionType(_tys) => todo!(),
			Expr::Function { body: _, data } => deps![codegen data],
			Expr::Call { func, arg } => deps![codegen func, codegen arg],
			Expr::Argument => deps![],
			Expr::Captures => deps![],

			Expr::Tuple(places) => places.iter().map(|p|
				QueryKind::Codegen { place: *p }
			).collect(),
			Expr::Struct(places) => places.values().map(|p|
				QueryKind::Codegen { place: *p }
			).collect(),

			Expr::Module { .. } => todo!(),

			Expr::TakeUnnamedElement { ex, idx: _ } => deps![codegen ex],
			Expr::TakeNamedElement { ex, idx: _ } => deps![codegen ex],

			Expr::BinaryOp { kind: _, operands } => deps![codegen operands],

			Expr::PrimIfThenElse { predicate, true_case, false_case } => deps![
				codegen predicate,
				codegen true_case,
				codegen false_case,
			],

			Expr::Error => deps![],
		}
	}
	*/
}



impl<'cctx> Display for Expr<'cctx> {
	fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
		use Expr::*;

		if f.alternate() {
			return write!(f, "{}", DisplayExpressionIndented(self));
		}

		match self {
			Literal(self::Literal::TypeType) => write!(f, "Type"),
			Literal(self::Literal::BoolType) => write!(f, "Bool"),
			Literal(self::Literal::I64Type) => write!(f, "I64"),
			Literal(self::Literal::Bool(v)) => write!(f, "{}", v),
			Literal(self::Literal::I64(v)) => write!(f, "{}", v),

			Ascribe { ex, ty } => write!(f, "({} : {})", ex, ty),

			Use { place } => if let Some(name) = place.to_ref().name.get() {
				write!(f, "{}", name)
			//} else if let Some(expr) = place.to_ref().expr.get() {
			//	write!(f, "{}", expr)
			} else {
				write!(f, "…")
			}

			FunctionType(ts) => {
				if let Expr::Tuple(ts) = &**ts {
					if ts.len() == 2 {
						return write!(f, "({} → {})", ts[0], ts[1]);
					}
				}
				write!(f, "((→) {})", ts)
			},
			Function { .. } => write!(f, "<fn …>"),
			//Function { body, .. } => write!(f, "<fn {}>", body),

			Call { func, arg } => write!(f, "({} {})", func, arg),

			Argument => write!(f, "<arg>"),
			Captures => write!(f, "<ctx>"),

			UnnamedAggregateType(conn, ts) => {
				if matches!(conn, Connective::With | Connective::Par) {
					write!(f, "co ")?;
				}
				let bars = matches!(conn, Connective::Plus | Connective::Par);
				match ts.len() {
					0 => if bars {
						write!(f, "(|)")
					} else {
						write!(f, "()")
					}
					1 => if bars {
						write!(f, "(| {})", ts[0])
					} else {
						write!(f, "({},)", ts[0])
					}
					_ => {
						let delim = if bars { " | " } else { ", " };
						write!(f, "({}", ts[0])?;
						for t in ts.iter().skip(1) {
							write!(f, "{}{}", delim, t)?;
						}
						write!(f, ")")
					},
				}
			},

			Tuple(vs) => {
				write!(f, "(")?;
				let mut vs = vs.iter();
				match vs.next() {
					Some(v) => {
						write!(f, "{},", v)?;
						if let Some(v) = vs.next() {
							write!(f, " {}", v)?;
							for v in vs {
								write!(f, ", {}", v)?;
							}
						}
					},
					None => write!(f, ",")?,
				}
				write!(f, ")")
			},

			NamedAggregateType(conn, ts) => {
				if matches!(conn, Connective::With | Connective::Par) {
					write!(f, "co ")?;
				}
				let bars = matches!(conn, Connective::Plus | Connective::Par);

				let mut ts = ts.iter();
				if let Some((n0, t0)) = ts.next() {
					if let Some((n1, t1)) = ts.next() {
						let delim = if bars { " | " } else { ", " };
						write!(f, "{{ {} : {}{}{} : {}", n0.string(), t0, delim, n1.string(), t1)?;
						for (n, t) in ts {
							write!(f, "{}{} : {}", delim, n.string(), t)?;
						}
						write!(f, " }}")
					} else if bars {
						write!(f, "{{| {} : {}}}", n0.string(), t0)
					} else {
						write!(f, "{{ {} : {} }}", n0.string(), t0)
					}
				} else if bars {
					write!(f, "{{|}}")
				} else {
					write!(f, "{{}}")
				}
			},

			Struct(vs) => {
				let mut vs = vs.iter();
				match vs.next() {
					Some((i, v)) => {
						write!(f, "{{ {} := {}", i.string(), v)?;
						for (i, v) in vs {
							write!(f, ", {} := {}", i.string(), v)?;
						}
						write!(f, " }}")
					},
					None => write!(f, "{{}}"),
				}
			},

			ModuleType(ts) => {
				let mut ts = ts.iter();
				write!(f, "module {{")?;
				if let Some((n0, t0)) = ts.next() {
					if let Some((n1, t1)) = ts.next() {
						write!(f, " {} : {}, {} : {}", n0.string(), t0, n1.string(), t1)?;
						for (n, t) in ts {
							write!(f, ", {} : {}", n.string(), t)?;
						}
						write!(f, " }}")
					} else {
						write!(f, " {} : {} }}", n0.string(), t0)
					}
				} else {
					write!(f, "}}")
				}
			},
			Module { public, private: _ } => {
				write!(f, "module {{ ")?;
				let mut is = public.keys();
				match is.next() {
					Some(i) => {
						write!(f, "{} := …,", i.string())?;
						if let Some(i) = is.next() {
							write!(f, " {} := …", i.string())?;
							for i in is {
								write!(f, ", {} := …", i.string())?;
							}
						}
					},
					None => write!(f, ",")?,
				}
				write!(f, " }}")
			},
			ModuleV(public) => {
				write!(f, "module {{ ")?;
				let mut vs = public.iter();
				match vs.next() {
					Some((i, v)) => {
						write!(f, "{} := {},", i.string(), v)?;
						if let Some((i, v)) = vs.next() {
							write!(f, " {} := {}", i.string(), v)?;
							for (i, v) in vs {
								write!(f, ", {} := {}", i.string(), v)?;
							}
						}
					},
					None => write!(f, ",")?,
				}
				write!(f, " }}")
			},

			TakeUnnamedElement { ex, idx } => write!(f, "{}.{}", ex, idx),
			TakeNamedElement { ex, idx } => write!(f, "{}.{}", ex, idx.string()),

			BinaryOp { kind, operands } => {
				if let Expr::Tuple(exs) = &**operands {
					if exs.len() == 2 {
						return write!(f, "({} {} {})", exs[0], kind, exs[1]);
					}
				}
				write!(f, "(({}) {})", kind, operands)
			},

			PrimIfThenElse { predicate, true_case, false_case }
				=> write!(f, "(if {} then {} else {})", predicate, true_case, false_case),

			Unknown { id } => write!(f, "?{}", id),
			UnnamedTypeElement { ty, idx } => write!(f, "{}.:{}", ty, idx),
			NamedTypeElement { ty, idx } => write!(f, "{}.:{}", ty, idx.string()),

			Error => write!(f, "<error>"),
		}
	}
}

#[repr(transparent)]
struct DisplayExpressionIndented<'cctx, 'a>(pub &'a Expr<'cctx>);

impl<'cctx, 'a> DisplayExpressionIndented<'cctx, 'a> {
	fn write_indented_off(&self, f: &mut fmt::Formatter<'_>, off: usize) -> Result<(), fmt::Error> {
		use Expr::*;

		fn newline(f: &mut fmt::Formatter<'_>, off: usize) -> Result<(), fmt::Error> {
			writeln!(f)?;
			for _ in 0..off {
				write!(f, "    ")?;
			}
			Ok(())
		}

		match self.0 {
			Literal(self::Literal::TypeType) => write!(f, "Type"),
			Literal(self::Literal::BoolType) => write!(f, "Bool"),
			Literal(self::Literal::I64Type) => write!(f, "I64"),
			Literal(self::Literal::Bool(v)) => write!(f, "{}", v),
			Literal(self::Literal::I64(v)) => write!(f, "{}", v),

			Ascribe { ex, ty } => {
				write!(f, "(")?;
				DisplayExpressionIndented(ex).write_indented_off(f, off)?;
				write!(f, " : ")?;
				DisplayExpressionIndented(ty).write_indented_off(f, off)?;
				write!(f, ")")
			},

			Use { place } => if let Some(name) = place.to_ref().name.get() {
				write!(f, "{}", name)
			//} else if let Some(expr) = place.to_ref().expr.get() {
			//	DisplayExpressionIndented(expr).write_indented_off(f, off)
			} else {
				write!(f, "…")
			}

			FunctionType(ts) => {
				if let Expr::Tuple(ts) = &**ts {
					if ts.len() == 2 {
						write!(f, "(")?;
						DisplayExpressionIndented(&ts[0]).write_indented_off(f, off)?;
						write!(f, " → ")?;
						DisplayExpressionIndented(&ts[1]).write_indented_off(f, off)?;
						return write!(f, ")");
					}
				}
				write!(f, "((→) ")?;
				DisplayExpressionIndented(ts).write_indented_off(f, off)?;
				write!(f, ")")
			},
			Function { .. } => write!(f, "<fn …>"),
			/*
			Function { body, .. } => {
				write!(f, "<fn ")?;
				DisplayExpressionIndented(body).write_indented_off(f, off)?;
				write!(f, ">")
			},
			*/

			Call { func, arg } => {
				write!(f, "(")?;
				DisplayExpressionIndented(func).write_indented_off(f, off)?;
				write!(f, " ")?;
				DisplayExpressionIndented(arg).write_indented_off(f, off)?;
				write!(f, ")")
			},

			Argument => write!(f, "<arg>"),
			Captures => write!(f, "<ctx>"),

			UnnamedAggregateType(conn, ts) => {
				if matches!(conn, Connective::With | Connective::Par) {
					write!(f, "co ")?;
				}
				let bars = matches!(conn, Connective::Plus | Connective::Par);
				match ts.len() {
					0 => if bars {
						write!(f, "(|)")
					} else {
						write!(f, "()")
					},
					1 => if bars {
						write!(f, "(| {})", ts[0])
					} else {
						write!(f, "({},)", ts[0])
					},
					2..=5 => {
						let delim = if bars { " | " } else { ", " };
						write!(f, "({}", ts[0])?;
						for t in ts.iter().skip(1) {
							write!(f, "{}{}", delim, t)?;
						}
						write!(f, ")")
					},
					_ => {
						write!(f, "(")?;
						if bars {
							for v in ts.iter() {
								newline(f, off + 1)?;
								write!(f, "| ")?;
								DisplayExpressionIndented(v).write_indented_off(f, off + 1)?;
							}
						} else {
							for v in ts.iter() {
								newline(f, off + 1)?;
								DisplayExpressionIndented(v).write_indented_off(f, off + 1)?;
								write!(f, ",")?;
							}
						}
						newline(f, off)?;
						write!(f, ")")
					},
				}
			},

			Tuple(vs) => {
				match vs.len() {
					0 => write!(f, "()"),
					1 => {
						write!(f, "(")?;
						DisplayExpressionIndented(&vs[0]).write_indented_off(f, off)?;
						write!(f, ",)")
					},
					2..=5 => {
						write!(f, "(")?;
						DisplayExpressionIndented(&vs[0]).write_indented_off(f, off)?;
						for v in vs.iter().skip(1) {
							write!(f, ", ")?;
							DisplayExpressionIndented(v).write_indented_off(f, off)?;
						}
						write!(f, ")")
					},
					_ => {
						write!(f, "(")?;
						for v in vs.iter() {
							newline(f, off + 1)?;
							DisplayExpressionIndented(v).write_indented_off(f, off + 1)?;
							write!(f, ",")?;
						}
						newline(f, off)?;
						write!(f, ")")
					},
				}
			},

			NamedAggregateType(conn, ts) if ts.len() <= 3 => {
				if matches!(conn, Connective::With | Connective::Par) {
					write!(f, "co ")?;
				}
				let bars = matches!(conn, Connective::Plus | Connective::Par);
				let mut ts = ts.iter();
				match ts.next() {
					Some((i, t)) => {
						write!(f, "{{ {} : ", i.string())?;
						DisplayExpressionIndented(t).write_indented_off(f, off)?;
						let delim = if bars { " | " } else { ", " };
						for (i, t) in ts {
							write!(f, "{}{} : ", delim, i.string())?;
							DisplayExpressionIndented(t).write_indented_off(f, off)?;
						}
						write!(f, " }}")
					},
					None => write!(f, "{{}}"),
				}
			},
			NamedAggregateType(conn, ts) => {
				if matches!(conn, Connective::With | Connective::Par) {
					write!(f, "co ")?;
				}
				write!(f, "{{")?;
				let bars = matches!(conn, Connective::Plus | Connective::Par);
				if bars {
					for (n, t) in ts.iter() {
						newline(f, off + 1)?;
						write!(f, "| {} : ", n.string())?;
						DisplayExpressionIndented(t).write_indented_off(f, off + 1)?;
					}
				} else {
					for (n, t) in ts.iter() {
						newline(f, off + 1)?;
						write!(f, "{} : ", n.string())?;
						DisplayExpressionIndented(t).write_indented_off(f, off + 1)?;
						write!(f, ",")?;
					}
				}
				newline(f, off)?;
				write!(f, "}}")
			},

			Struct(vs) if vs.len() <= 3 => {
				let mut vs = vs.iter();
				match vs.next() {
					Some((i, v)) => {
						write!(f, "{{ {} := ", i.string())?;
						DisplayExpressionIndented(v).write_indented_off(f, off)?;
						for (i, v) in vs {
							write!(f, ", {} := ", i.string())?;
							DisplayExpressionIndented(v).write_indented_off(f, off)?;
						}
						write!(f, " }}")
					},
					None => write!(f, "{{}}"),
				}
			},
			Struct(vs) => {
				write!(f, "{{")?;
				for (n, v) in vs.iter() {
					newline(f, off + 1)?;
					write!(f, "{} := ", n.string())?;
					DisplayExpressionIndented(v).write_indented_off(f, off + 1)?;
					write!(f, ",")?;
				}
				newline(f, off)?;
				write!(f, "}}")
			},

			ModuleType(ts) if ts.len() <= 3 => {
				let mut ts = ts.iter();
				match ts.next() {
					Some((i, t)) => {
						write!(f, "module {{ {} : ", i.string())?;
						DisplayExpressionIndented(t).write_indented_off(f, off)?;
						for (i, t) in ts {
							write!(f, ", {} : ", i.string())?;
							DisplayExpressionIndented(t).write_indented_off(f, off)?;
						}
						write!(f, " }}")
					},
					None => write!(f, "{{}}"),
				}
			},
			ModuleType(ts) => {
				write!(f, "module {{")?;
				for (n, t) in ts.iter() {
					newline(f, off + 1)?;
					write!(f, "{} : ", n.string())?;
					DisplayExpressionIndented(t).write_indented_off(f, off + 1)?;
					write!(f, ",")?;
				}
				newline(f, off)?;
				write!(f, "}}")
			},

			Module { public, private: _ } if public.len() <= 3 => {
				let mut ns = public.keys();
				match ns.next() {
					Some(n) => {
						write!(f, "module {{ {} := …", n.string())?;
						for n in ns {
							write!(f, ", {} := …", n.string())?;
						}
						write!(f, " }}")
					},
					None => write!(f, "module {{}}"),
				}
			},
			Module { public, private: _ } => {
				write!(f, "module {{")?;
				for n in public.keys() {
					newline(f, off + 1)?;
					write!(f, "{} := …,", n.string())?;
				}
				newline(f, off)?;
				write!(f, "}}")
			},

			ModuleV(public) if public.len() <= 3 => {
				let mut vs = public.iter();
				match vs.next() {
					Some((i, v)) => {
						write!(f, "module {{ {} := ", i.string())?;
						DisplayExpressionIndented(v).write_indented_off(f, off)?;
						for (i, v) in vs {
							write!(f, ", {} := ", i.string())?;
							DisplayExpressionIndented(v).write_indented_off(f, off)?;
						}
						write!(f, " }}")
					},
					None => write!(f, "module {{}}"),
				}
			},
			ModuleV(public) => {
				write!(f, "module {{")?;
				for (n, v) in public.iter() {
					newline(f, off + 1)?;
					write!(f, "{} := ", n.string())?;
					DisplayExpressionIndented(v).write_indented_off(f, off + 1)?;
					write!(f, ",")?;
				}
				newline(f, off)?;
				write!(f, "}}")
			},

			TakeUnnamedElement { ex, idx } => {
				DisplayExpressionIndented(ex).write_indented_off(f, off)?;
				write!(f, ".{}", idx)
			},
			TakeNamedElement { ex, idx } => {
				DisplayExpressionIndented(ex).write_indented_off(f, off)?;
				write!(f, ".{}", idx.string())
			},

			BinaryOp { kind, operands } => {
				if let Expr::Tuple(exs) = &**operands {
					if exs.len() == 2 {
						write!(f, "(")?;
						DisplayExpressionIndented(&exs[0]).write_indented_off(f, off)?;
						write!(f, " {} ", kind)?;
						DisplayExpressionIndented(&exs[1]).write_indented_off(f, off)?;
						return write!(f, ")");
					}
				}
				write!(f, "(({}) ", kind)?;
				DisplayExpressionIndented(operands).write_indented_off(f, off)?;
				write!(f, ")")
			},

			PrimIfThenElse { predicate, true_case, false_case } => {
				write!(f, "(if ")?;
				DisplayExpressionIndented(predicate).write_indented_off(f, off)?;
				write!(f, " then ")?;
				DisplayExpressionIndented(true_case).write_indented_off(f, off)?;
				write!(f, " else ")?;
				DisplayExpressionIndented(false_case).write_indented_off(f, off)?;
				write!(f, ")")
			},

			Unknown { id } => write!(f, "?{}", id),
			UnnamedTypeElement { ty, idx } => write!(f, "{}.:{}", ty, idx),
			NamedTypeElement { ty, idx } => write!(f, "{}.:{}", ty, idx.string()),

			Error => write!(f, "<error>"),
		}
	}
}

impl<'cctx, 'a> Display for DisplayExpressionIndented<'cctx, 'a> {
	fn fmt(&self, f: &mut fmt::Formatter<'_>) -> Result<(), fmt::Error> {
		self.write_indented_off(f, 0)
	}
}



struct Binds<'cctx> {
	pub named: HashMap<Identifier<'cctx>, Place<'cctx>>,
	pub drops: Vec<Expr<'cctx>>,
}

impl<'cctx> Binds<'cctx> {
	/// Creates expressions for the 'binding side' of an expression, visually on the
	/// left. for instance given `(a, b)` it will give two names, `a` and `b`, and
	/// set them to take the first and second elements from the given place as a
	/// tuple. This would appear for instance in the statement `(a, b) := (1, 2)`.
	fn from_written(
		cctx: &'cctx CompileContext<'cctx>,
		file: File<'cctx>,
		scopes: Option<&Scope<'cctx, '_>>,
		written_expr: WrittenExpression<'cctx>,
		wrap: Expr<'cctx>,
		wrap_span: Span<'cctx>,
	) -> Binds<'cctx> {
		match written_expr {
			WrittenExpression::UnaryOperation(_s, _ex) => todo!(),
			// TODO don't test by string
			WrittenExpression::BinaryOperation(s, exs) if s.0 == cctx.identifiers.colon => {
				// TODO doesn't find types in the same scope as currently building…
				let (ex, ty) = *exs;
				let ty = Expr::from_written(cctx, file, scopes, ty);
				Binds::from_written(cctx, file, scopes, ex, Expr::Ascribe {
					ex: Box::new(wrap),
					ty: Box::new(ty),
				}, wrap_span)
			},
			WrittenExpression::BinaryOperation(_s, _exs) => todo!(),
			// TODO drop
			// TODO check against interned "_" instead
			WrittenExpression::Ident(name, _) if name == cctx.identifiers.underscore => Binds {
				named: HashMap::new(),
				drops: vec![wrap],
			},
			WrittenExpression::Ident(name, range) => {
				let mut named = HashMap::new();
				named.insert(name, cctx.new_place(Some(name.string()), Some(wrap), Span::new(file, range)));
				Binds { named, drops: Vec::new() }
			},
			WrittenExpression::NameQualified(_, _, _) => {
				log::error(log::frame(String::from("don't understand binding to a qualified name"))
					// TODO annotation
				).emit(cctx);
				Binds { named: HashMap::new(), drops: vec![wrap] }
			},
			WrittenExpression::IndexQualified(_, _, _) => {
				log::error(log::frame(String::from("don't understand binding to a qualified name"))
					// TODO annotation
				).emit(cctx);
				Binds { named: HashMap::new(), drops: vec![wrap] }
			},

			WrittenExpression::IntegerLiteral(_v, _) => todo!(),

			WrittenExpression::Region { region_inner: ri, bracket_kind: br, range: region_range } => match (ri, br) {
				(RegionInner::Undelimited(None), BracketKind::Parenthesis) =>
					Binds { named: HashMap::new(), drops: vec![wrap] },
				(RegionInner::Undelimited(Some(wex)), BracketKind::Parenthesis) => match wex.head {
					None => Binds::from_written(cctx, file, scopes, wex.expr, wrap, wrap_span),
					Some(_head) => todo!(),
				},

				(RegionInner::Delimited(wstates, Delimiter::Comma), BracketKind::Parenthesis) => {
					let mut named = HashMap::new();
					let mut drops = Vec::new();

					let tuple = cctx.new_place(Some("<tuple destructure>"), Some(wrap), Span::new(file, region_range));
					for (idx, wstate) in wstates.into_iter().enumerate() {
						let elem_span = wstate.span_in(file);
						let elem = Expr::TakeUnnamedElement {
							ex: Box::new(Expr::Use { place: tuple }),
							idx,
						};
						if wstate.head.is_some() {
							log::error(log::frame(String::from("don't know how to handle statement in tuple destructure"))
								// TODO annotation
							).emit(cctx);
						}
						let wex = wstate.expr;

						let binds = Binds::from_written(cctx, file, scopes, wex, elem, elem_span);
						drops.extend(binds.drops);
						for (name, place) in binds.named {
							if let Err(e) = named.try_insert(name, place) {
								log::error(log::frame(format!("name `{}` bound multiple times in one expression", name.string()))
									// TODO annotation
								).emit(cctx);
								drops.push(Expr::Use { place: e.value });
							}
						}
					}

					Binds { named, drops }
				},

				_ => todo!(),
			},

			WrittenExpression::IfThenElse { .. } => todo!(),

			WrittenExpression::Error(_) => Binds { named: HashMap::new(), drops: Vec::new() },
		}
	}
	#[inline]
	fn from_maybe_written(
		cctx: &'cctx CompileContext<'cctx>,
		file: File<'cctx>,
		scopes: Option<&Scope<'cctx, '_>>,
		written_expr: Option<WrittenExpression<'cctx>>,
		wrap: Expr<'cctx>,
		wrap_span: Span<'cctx>,
	) -> Binds<'cctx> {
		match written_expr {
			// TODO drop
			None => Binds { named: HashMap::new(), drops: Vec::new() },
			Some(wex) => Binds::from_written(cctx, file, scopes, wex, wrap, wrap_span),
		}
	}
}



/*
/// A connection between a compiler type and an in-language type. Used for types
/// which aren't necessarily 'magic', but do need to be specially recognized for
/// certain features.
///
/// Examples where this is used are `Ord` (for size checking), `String` (for
/// literals), and `Plus` (for the builtin `+` operator).
pub trait Latch<'cctx>: Sized {
	/// Unpack a [`Expr`] into a value of this type if possible.
	fn from_value(value: Expr) -> Result<Self, LatchError>;

	/// Assuming type checking has already been done, obtain a value of this type
	/// from a [`Expr`].
	///
	/// # Safety
	///
	/// If [`Expr`] `v` has a type recongized equivalent to the latched type here,
	/// then `Some(Self::from_value_unchecked(v))` must equal
	/// `Self::from_value(v)`.
	unsafe fn from_value_unchecked(value: Expr) -> Self {
		Self::from_value(value).unwrap()
	}

	/// Convert into a `Expr` usable in-language.
	fn to_value(self) -> Expr<'cctx>;
}

/// Eventually all type errors which may result from trying to convert to a
/// compiler value. For now, the details are not important.
#[derive(Debug)]
pub struct LatchError {}
*/
