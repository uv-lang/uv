//! Parsing from strings into tokens into a syntax tree.



use std::{
	collections::HashMap,
	fmt::{
		self,
		Display,
	},
	ops::{
		Range,
	},
};

use crate::{
	ctx::{
		CompileContext,
		File,
		Identifier,
		Span,
	},
	log,
};



/// Matching character pairs which may contain tokens. Each describes a
/// Bidi_Paired_Bracket pair in Unicode.
#[derive(Copy, Clone, Debug, Eq, Hash, PartialEq)]
#[allow(missing_docs)]
pub enum BracketKind {
	Parenthesis,
	SquareBracket,
	CurlyBracket,
	TibetanMarkGugRtagsGy,
	TibetanMarkAngKhangGy,
	OgramFeatherMark,
	SquareBracketWithQuill,
	SuperscriptParenthesis,
	SubscriptParenthesis,
	Ceiling,
	Floor,
	PointingAngleBracket,
	MediumParenthesisOrnament,
	MediumFlattenedParenthesisOrnament,
	MediumPointingAngleBracketOrnament,
	HeavyPointingAngleQuotationMarkOrnament,
	HeavyPointingAngleBracketOrnament,
	LightTortoiseShellBracketOrnament,
	MediumCurlyBracketOrnament,
	SShapedBagDelimiter,
	MathematicalWhiteSquareBracket,
	MathematicalAngleBracket,
	MathematicalDoubleAngleBracket,
	MathematicalWhiteTortoiseShellBracket,
	MathematicalFlattenedParenthesis,
	WhiteCurlyBracket,
	WhiteParenthesis,
	ZNotationImageBracket,
	ZNotationBindingBracket,
	SquareBracketWithUnderbar,
	SquareBracketWithTickInCorner,
	SquareBracketWithTickInReverseCorner,
	AngleBracketWithDot,
	ArcCompareBracket,
	ArcReverseCompareBracket,
	BlackTortoiseShellBracket,
	WigglyFence,
	DoubleWigglyFence,
	PointingCurvedAngleBracket,
	TopHalfBracket,
	BottomHalfBracket,
	SidewaysUBracket,
	DoubleParenthesis,
	AngleBracket,
	DoubleAngleBracket,
	CornerBracket,
	WhiteCornerBracket,
	BlackLenticularBracket,
	TortoiseShellBracket,
	WhiteLenticularBracket,
	WhiteTortoiseShellBracket,
	WhiteSquareBracket,
	SmallParenthesis,
	SmallCurlyBracket,
	SmallTortoiseShellBracket,
	FullwidthParenthesis,
	FullwidthSquareBracket,
	FullwidthCurlyBracket,
	FullwidthWhiteParenthesis,
	HalfwidthCornerBracket,
}
/// Direction of a bracket.
#[derive(Copy, Clone, Debug, Eq, Hash, PartialEq)]
pub enum BracketDirection {
	/// Opens a region, often called 'left'.
	Open,
	/// Closes a region, often called 'right'.
	Close,
}

/// An opener or closer of a syntactic region.
#[derive(Copy, Clone, Debug, Eq, Hash, PartialEq)]
pub struct Bracket(pub BracketKind, pub BracketDirection);

impl Bracket {
	/// Match a character as a potential bracket.
	pub fn new(c: char) -> Option<Self> {
		use BracketKind::*;
		use BracketDirection::*;

		Some( match c {
			'(' => Bracket(Parenthesis, Open),
			')' => Bracket(Parenthesis, Close),
			'[' => Bracket(SquareBracket, Open),
			']' => Bracket(SquareBracket, Close),
			'{' => Bracket(CurlyBracket, Open),
			'}' => Bracket(CurlyBracket, Close),
			'༺' => Bracket(TibetanMarkGugRtagsGy, Open),
			'༻' => Bracket(TibetanMarkGugRtagsGy, Close),
			'༼' => Bracket(TibetanMarkAngKhangGy, Open),
			'༽' => Bracket(TibetanMarkAngKhangGy, Close),
			'᚛' => Bracket(OgramFeatherMark, Open),
			'᚜' => Bracket(OgramFeatherMark, Close),
			'⁅' => Bracket(SquareBracketWithQuill, Open),
			'⁆' => Bracket(SquareBracketWithQuill, Close),
			'⁽' => Bracket(SuperscriptParenthesis, Open),
			'⁾' => Bracket(SuperscriptParenthesis, Close),
			'₍' => Bracket(SubscriptParenthesis, Open),
			'₎' => Bracket(SubscriptParenthesis, Close),
			'⌈' => Bracket(Ceiling, Open),
			'⌉' => Bracket(Ceiling, Close),
			'⌊' => Bracket(Floor, Open),
			'⌋' => Bracket(Floor, Close),
			'〈' => Bracket(PointingAngleBracket, Open),
			'〉' => Bracket(PointingAngleBracket, Close),
			'❨' => Bracket(MediumParenthesisOrnament, Open),
			'❩' => Bracket(MediumParenthesisOrnament, Close),
			'❪' => Bracket(MediumFlattenedParenthesisOrnament, Open),
			'❫' => Bracket(MediumFlattenedParenthesisOrnament, Close),
			'❬' => Bracket(MediumPointingAngleBracketOrnament, Open),
			'❭' => Bracket(MediumPointingAngleBracketOrnament, Close),
			'❮' => Bracket(HeavyPointingAngleQuotationMarkOrnament, Open),
			'❯' => Bracket(HeavyPointingAngleQuotationMarkOrnament, Close),
			'❰' => Bracket(HeavyPointingAngleBracketOrnament, Open),
			'❱' => Bracket(HeavyPointingAngleBracketOrnament, Close),
			'❲' => Bracket(LightTortoiseShellBracketOrnament, Open),
			'❳' => Bracket(LightTortoiseShellBracketOrnament, Close),
			'❴' => Bracket(MediumCurlyBracketOrnament, Open),
			'❵' => Bracket(MediumCurlyBracketOrnament, Close),
			'⟅' => Bracket(SShapedBagDelimiter, Open),
			'⟆' => Bracket(SShapedBagDelimiter, Close),
			'⟦' => Bracket(MathematicalWhiteSquareBracket, Open),
			'⟧' => Bracket(MathematicalWhiteSquareBracket, Close),
			'⟨' => Bracket(MathematicalAngleBracket, Open),
			'⟩' => Bracket(MathematicalAngleBracket, Close),
			'⟪' => Bracket(MathematicalDoubleAngleBracket, Open),
			'⟫' => Bracket(MathematicalDoubleAngleBracket, Close),
			'⟬' => Bracket(MathematicalWhiteTortoiseShellBracket, Open),
			'⟭' => Bracket(MathematicalWhiteTortoiseShellBracket, Close),
			'⟮' => Bracket(MathematicalFlattenedParenthesis, Open),
			'⟯' => Bracket(MathematicalFlattenedParenthesis, Close),
			'⦃' => Bracket(WhiteCurlyBracket, Open),
			'⦄' => Bracket(WhiteCurlyBracket, Close),
			'⦅' => Bracket(WhiteParenthesis, Open),
			'⦆' => Bracket(WhiteParenthesis, Close),
			'⦇' => Bracket(ZNotationImageBracket, Open),
			'⦈' => Bracket(ZNotationImageBracket, Close),
			'⦉' => Bracket(ZNotationBindingBracket, Open),
			'⦊' => Bracket(ZNotationBindingBracket, Close),
			'⦋' => Bracket(SquareBracketWithUnderbar, Open),
			'⦌' => Bracket(SquareBracketWithUnderbar, Close),
			'⦍' => Bracket(SquareBracketWithTickInCorner, Open),
			'⦎' => Bracket(SquareBracketWithTickInCorner, Close),
			'⦏' => Bracket(SquareBracketWithTickInReverseCorner, Open),
			'⦐' => Bracket(SquareBracketWithTickInReverseCorner, Close),
			'⦑' => Bracket(AngleBracketWithDot, Open),
			'⦒' => Bracket(AngleBracketWithDot, Close),
			'⦓' => Bracket(ArcCompareBracket, Open),
			'⦔' => Bracket(ArcCompareBracket, Close),
			'⦕' => Bracket(ArcReverseCompareBracket, Open),
			'⦖' => Bracket(ArcReverseCompareBracket, Close),
			'⦗' => Bracket(BlackTortoiseShellBracket, Open),
			'⦘' => Bracket(BlackTortoiseShellBracket, Close),
			'⧘' => Bracket(WigglyFence, Open),
			'⧙' => Bracket(WigglyFence, Close),
			'⧚' => Bracket(DoubleWigglyFence, Open),
			'⧛' => Bracket(DoubleWigglyFence, Close),
			'⧼' => Bracket(PointingCurvedAngleBracket, Open),
			'⧽' => Bracket(PointingCurvedAngleBracket, Close),
			'⸢' => Bracket(TopHalfBracket, Open),
			'⸣' => Bracket(TopHalfBracket, Close),
			'⸤' => Bracket(BottomHalfBracket, Open),
			'⸥' => Bracket(BottomHalfBracket, Close),
			'⸦' => Bracket(SidewaysUBracket, Open),
			'⸧' => Bracket(SidewaysUBracket, Close),
			'⸨' => Bracket(DoubleParenthesis, Open),
			'⸩' => Bracket(DoubleParenthesis, Close),
			'〈' => Bracket(AngleBracket, Open),
			'〉' => Bracket(AngleBracket, Close),
			'《' => Bracket(DoubleAngleBracket, Open),
			'》' => Bracket(DoubleAngleBracket, Close),
			'「' => Bracket(CornerBracket, Open),
			'」' => Bracket(CornerBracket, Close),
			'『' => Bracket(WhiteCornerBracket, Open),
			'』' => Bracket(WhiteCornerBracket, Close),
			'【' => Bracket(BlackLenticularBracket, Open),
			'】' => Bracket(BlackLenticularBracket, Close),
			'〔' => Bracket(TortoiseShellBracket, Open),
			'〕' => Bracket(TortoiseShellBracket, Close),
			'〖' => Bracket(WhiteLenticularBracket, Open),
			'〗' => Bracket(WhiteLenticularBracket, Close),
			'〘' => Bracket(WhiteTortoiseShellBracket, Open),
			'〙' => Bracket(WhiteTortoiseShellBracket, Close),
			'〚' => Bracket(WhiteSquareBracket, Open),
			'〛' => Bracket(WhiteSquareBracket, Close),
			'﹙' => Bracket(SmallParenthesis, Open),
			'﹚' => Bracket(SmallParenthesis, Close),
			'﹛' => Bracket(SmallCurlyBracket, Open),
			'﹜' => Bracket(SmallCurlyBracket, Close),
			'﹝' => Bracket(SmallTortoiseShellBracket, Open),
			'﹞' => Bracket(SmallTortoiseShellBracket, Close),
			'（' => Bracket(FullwidthParenthesis, Open),
			'）' => Bracket(FullwidthParenthesis, Close),
			'［' => Bracket(FullwidthSquareBracket, Open),
			'］' => Bracket(FullwidthSquareBracket, Close),
			'｛' => Bracket(FullwidthCurlyBracket, Open),
			'｝' => Bracket(FullwidthCurlyBracket, Close),
			'｟' => Bracket(FullwidthWhiteParenthesis, Open),
			'｠' => Bracket(FullwidthWhiteParenthesis, Close),
			'｢' => Bracket(HalfwidthCornerBracket, Open),
			'｣' => Bracket(HalfwidthCornerBracket, Close),
			_ => return None,
		})
	}
}

impl Display for Bracket {
	fn fmt(&self, f: &mut fmt::Formatter<'_>) -> Result<(), fmt::Error> {
		use {BracketKind::*, BracketDirection::*};

		write!(f, "{}",
		match self {
			Bracket(Parenthesis, Open) => '(',
			Bracket(Parenthesis, Close) => ')',
			Bracket(SquareBracket, Open) => '[',
			Bracket(SquareBracket, Close) => ']',
			Bracket(CurlyBracket, Open) => '{',
			Bracket(CurlyBracket, Close) => '}',
			Bracket(TibetanMarkGugRtagsGy, Open) => '༺',
			Bracket(TibetanMarkGugRtagsGy, Close) => '༻',
			Bracket(TibetanMarkAngKhangGy, Open) => '༼',
			Bracket(TibetanMarkAngKhangGy, Close) => '༽',
			Bracket(OgramFeatherMark, Open) => '᚛',
			Bracket(OgramFeatherMark, Close) => '᚜',
			Bracket(SquareBracketWithQuill, Open) => '⁅',
			Bracket(SquareBracketWithQuill, Close) => '⁆',
			Bracket(SuperscriptParenthesis, Open) => '⁽',
			Bracket(SuperscriptParenthesis, Close) => '⁾',
			Bracket(SubscriptParenthesis, Open) => '₍',
			Bracket(SubscriptParenthesis, Close) => '₎',
			Bracket(Ceiling, Open) => '⌈',
			Bracket(Ceiling, Close) => '⌉',
			Bracket(Floor, Open) => '⌊',
			Bracket(Floor, Close) => '⌋',
			Bracket(PointingAngleBracket, Open) => '〈',
			Bracket(PointingAngleBracket, Close) => '〉',
			Bracket(MediumParenthesisOrnament, Open) => '❨',
			Bracket(MediumParenthesisOrnament, Close) => '❩',
			Bracket(MediumFlattenedParenthesisOrnament, Open) => '❪',
			Bracket(MediumFlattenedParenthesisOrnament, Close) => '❫',
			Bracket(MediumPointingAngleBracketOrnament, Open) => '❬',
			Bracket(MediumPointingAngleBracketOrnament, Close) => '❭',
			Bracket(HeavyPointingAngleQuotationMarkOrnament, Open) => '❮',
			Bracket(HeavyPointingAngleQuotationMarkOrnament, Close) => '❯',
			Bracket(HeavyPointingAngleBracketOrnament, Open) => '❰',
			Bracket(HeavyPointingAngleBracketOrnament, Close) => '❱',
			Bracket(LightTortoiseShellBracketOrnament, Open) => '❲',
			Bracket(LightTortoiseShellBracketOrnament, Close) => '❳',
			Bracket(MediumCurlyBracketOrnament, Open) => '❴',
			Bracket(MediumCurlyBracketOrnament, Close) => '❵',
			Bracket(SShapedBagDelimiter, Open) => '⟅',
			Bracket(SShapedBagDelimiter, Close) => '⟆',
			Bracket(MathematicalWhiteSquareBracket, Open) => '⟦',
			Bracket(MathematicalWhiteSquareBracket, Close) => '⟧',
			Bracket(MathematicalAngleBracket, Open) => '⟨',
			Bracket(MathematicalAngleBracket, Close) => '⟩',
			Bracket(MathematicalDoubleAngleBracket, Open) => '⟪',
			Bracket(MathematicalDoubleAngleBracket, Close) => '⟫',
			Bracket(MathematicalWhiteTortoiseShellBracket, Open) => '⟬',
			Bracket(MathematicalWhiteTortoiseShellBracket, Close) => '⟭',
			Bracket(MathematicalFlattenedParenthesis, Open) => '⟮',
			Bracket(MathematicalFlattenedParenthesis, Close) => '⟯',
			Bracket(WhiteCurlyBracket, Open) => '⦃',
			Bracket(WhiteCurlyBracket, Close) => '⦄',
			Bracket(WhiteParenthesis, Open) => '⦅',
			Bracket(WhiteParenthesis, Close) => '⦆',
			Bracket(ZNotationImageBracket, Open) => '⦇',
			Bracket(ZNotationImageBracket, Close) => '⦈',
			Bracket(ZNotationBindingBracket, Open) => '⦉',
			Bracket(ZNotationBindingBracket, Close) => '⦊',
			Bracket(SquareBracketWithUnderbar, Open) => '⦋',
			Bracket(SquareBracketWithUnderbar, Close) => '⦌',
			Bracket(SquareBracketWithTickInCorner, Open) => '⦍',
			Bracket(SquareBracketWithTickInCorner, Close) => '⦎',
			Bracket(SquareBracketWithTickInReverseCorner, Open) => '⦏',
			Bracket(SquareBracketWithTickInReverseCorner, Close) => '⦐',
			Bracket(AngleBracketWithDot, Open) => '⦑',
			Bracket(AngleBracketWithDot, Close) => '⦒',
			Bracket(ArcCompareBracket, Open) => '⦓',
			Bracket(ArcCompareBracket, Close) => '⦔',
			Bracket(ArcReverseCompareBracket, Open) => '⦕',
			Bracket(ArcReverseCompareBracket, Close) => '⦖',
			Bracket(BlackTortoiseShellBracket, Open) => '⦗',
			Bracket(BlackTortoiseShellBracket, Close) => '⦘',
			Bracket(WigglyFence, Open) => '⧘',
			Bracket(WigglyFence, Close) => '⧙',
			Bracket(DoubleWigglyFence, Open) => '⧚',
			Bracket(DoubleWigglyFence, Close) => '⧛',
			Bracket(PointingCurvedAngleBracket, Open) => '⧼',
			Bracket(PointingCurvedAngleBracket, Close) => '⧽',
			Bracket(TopHalfBracket, Open) => '⸢',
			Bracket(TopHalfBracket, Close) => '⸣',
			Bracket(BottomHalfBracket, Open) => '⸤',
			Bracket(BottomHalfBracket, Close) => '⸥',
			Bracket(SidewaysUBracket, Open) => '⸦',
			Bracket(SidewaysUBracket, Close) => '⸧',
			Bracket(DoubleParenthesis, Open) => '⸨',
			Bracket(DoubleParenthesis, Close) => '⸩',
			Bracket(AngleBracket, Open) => '〈',
			Bracket(AngleBracket, Close) => '〉',
			Bracket(DoubleAngleBracket, Open) => '《',
			Bracket(DoubleAngleBracket, Close) => '》',
			Bracket(CornerBracket, Open) => '「',
			Bracket(CornerBracket, Close) => '」',
			Bracket(WhiteCornerBracket, Open) => '『',
			Bracket(WhiteCornerBracket, Close) => '』',
			Bracket(BlackLenticularBracket, Open) => '【',
			Bracket(BlackLenticularBracket, Close) => '】',
			Bracket(TortoiseShellBracket, Open) => '〔',
			Bracket(TortoiseShellBracket, Close) => '〕',
			Bracket(WhiteLenticularBracket, Open) => '〖',
			Bracket(WhiteLenticularBracket, Close) => '〗',
			Bracket(WhiteTortoiseShellBracket, Open) => '〘',
			Bracket(WhiteTortoiseShellBracket, Close) => '〙',
			Bracket(WhiteSquareBracket, Open) => '〚',
			Bracket(WhiteSquareBracket, Close) => '〛',
			Bracket(SmallParenthesis, Open) => '﹙',
			Bracket(SmallParenthesis, Close) => '﹚',
			Bracket(SmallCurlyBracket, Open) => '﹛',
			Bracket(SmallCurlyBracket, Close) => '﹜',
			Bracket(SmallTortoiseShellBracket, Open) => '﹝',
			Bracket(SmallTortoiseShellBracket, Close) => '﹞',
			Bracket(FullwidthParenthesis, Open) => '（',
			Bracket(FullwidthParenthesis, Close) => '）',
			Bracket(FullwidthSquareBracket, Open) => '［',
			Bracket(FullwidthSquareBracket, Close) => '］',
			Bracket(FullwidthCurlyBracket, Open) => '｛',
			Bracket(FullwidthCurlyBracket, Close) => '｝',
			Bracket(FullwidthWhiteParenthesis, Open) => '｟',
			Bracket(FullwidthWhiteParenthesis, Close) => '｠',
			Bracket(HalfwidthCornerBracket, Open) => '｢',
			Bracket(HalfwidthCornerBracket, Close) => '｣',
		})
	}
}

/// Wrapping of `Option<BracketKind>` for display, when `None` is frequently
/// used to mean we reached EOF.
#[repr(transparent)]
#[derive(Debug, Eq, PartialEq)]
struct CloseOrEOF(Option<BracketKind>);
impl Display for CloseOrEOF {
	fn fmt(&self, f: &mut fmt::Formatter<'_>) -> Result<(), fmt::Error> {
		match self.0 {
			Some(br) => write!(f, "`{}`", Bracket (br, BracketDirection::Close)),
			None => write!(f, "EOF"),
		}
	}
}



/// A type of separator between expressions. Each is named after its associated
/// character.
#[derive(Clone, Copy, Debug, Eq, Hash, PartialEq)]
#[allow(missing_docs)]
pub enum Delimiter {
	Comma,
	Semicolon,
	VerticalLine,
}

impl Delimiter {
	/// Match a character as a potential delimiter.
	pub fn new(c: char) -> Option<Self> {
		use Delimiter::*;
		Some( match c {
			',' => Comma,
			';' => Semicolon,
			'|' => VerticalLine,
			_ => return None,
		})
	}
}

impl Display for Delimiter {
	fn fmt(&self, f: &mut fmt::Formatter<'_>) -> Result<(), fmt::Error> {
		use Delimiter::*;

		write!(f, "{}",
		match self {
			Comma => ',',
			Semicolon => ';',
			VerticalLine => '|',
		})
	}
}



/// Operator precedence levels.
#[derive(Clone, Copy, Debug, Eq, Hash, Ord, PartialEq, PartialOrd)]
pub enum OperatorPrecedence {
	/// Assignment e.g. `:=`
	Assign,
	/// Lambda definition e.g. `\`
	Function,
	/// Type assertion `:`
	Type,
	/// First level of hyperoperations e.g.`+`, `-`
	Hyper1,
	/// Second level of hyperoperations e.g.`*`, `/`
	Hyper2,
	/// Unary prefix `+` and `-`.
	Sign,
	/// Third level of hyperoperations e.g.`^`
	Hyper3,
	/// Function calls.
	Call,
	/// Almost immediate unary operators e.g. `!`.
	Direct,
}

impl OperatorPrecedence {
	fn associativity(self) -> Option<OperatorPrecedenceAssociativity> {
		use OperatorPrecedence::*;
		use OperatorPrecedenceAssociativity::*;

		match self {
			Assign => None,
			Function => Some(Right),
			Type => Some(Right),
			Hyper1 => Some(Left),
			Hyper2 => Some(Left),
			Sign => Some(Left),
			Hyper3 => Some(Right),
			Call => Some(Left),
			Direct => Some(Left),
		}
	}
}

#[derive(Clone, Copy, Debug, Eq, Hash, PartialEq)]
pub enum OperatorPrecedenceAssociativity {
	/// Left associativity (`a ∙ b ∙ c` = `(a ∙ b) ∙ c`)
	Left,
	/// Right associativity (`a ∙ b ∙ c` = `a ∙ (b ∙ c)`)
	Right,
}



#[derive(Debug)]
pub enum KeywordExpr {
	Module
}

impl Display for KeywordExpr {
	fn fmt(&self, f: &mut fmt::Formatter<'_>) -> Result<(), fmt::Error> {
		write!(f, "{}", match self {
			KeywordExpr::Module => "module",
		})
	}
}

#[derive(Debug)]
pub enum KeywordStart {
	If,
}

impl Display for KeywordStart {
	fn fmt(&self, f: &mut fmt::Formatter<'_>) -> Result<(), fmt::Error> {
		write!(f, "{}", match self {
			KeywordStart::If => "if",
		})
	}
}

#[derive(Debug)]
pub enum KeywordDelim {
	Then,
	Else,
}

impl Display for KeywordDelim {
	fn fmt(&self, f: &mut fmt::Formatter<'_>) -> Result<(), fmt::Error> {
		write!(f, "{}", match self {
			KeywordDelim::Then => "then",
			KeywordDelim::Else => "else",
		})
	}
}



/// A unit of the first interpreted representation of code.
#[derive(Debug)]
pub enum TokenKind<'cctx> {
	/// An identifier, such as `my_int` or `Type`
	Identifier(Identifier<'cctx>),
	/// A prefix or infix operator, such as `+` or `:=`
	Operator(Identifier<'cctx>),

	/// A string of digits, such as `2` or `1729`
	IntegerLiteral(i64),

	/// A bracket opening or closing a delimited region.
	Bracket(Bracket),
	/// A delimiter separating expressions within a region.
	Delimiter(Delimiter),

	KeywordStart(KeywordStart),
	KeywordDelim(KeywordDelim),
}

impl<'cctx> TokenKind<'cctx> {
	pub fn at(self, range: Range<usize>) -> Token<'cctx> {
		Token {
			kind: self,
			range: (range.start as u32)..(range.end as u32),
		}
	}

	pub fn at_u32(self, range: Range<u32>) -> Token<'cctx> {
		Token {
			kind: self,
			range,
		}
	}
}

pub struct Token<'cctx> {
	pub kind: TokenKind<'cctx>,
	pub range: Range<u32>,
}



/// An iterator parsing tokens from a `&str`.
pub struct Tokens<'cctx, 'a> {
	cctx: &'cctx CompileContext<'cctx>,
	file: File<'cctx>,
	string: &'a str,
	iter: core::iter::Peekable<core::str::CharIndices<'a>>,
}
impl<'cctx, 'a> Tokens<'cctx, 'a> {
	/// Wrap a string slice to read tokens.
	pub fn new(cctx: &'cctx CompileContext<'cctx>, file: File<'cctx>, s: &'a str) -> Self {
		Self {
			cctx,
			file,
			string: s,
			iter: s.char_indices().peekable(),
		}
	}

	// helpers for iteration

	fn read_line_comment(&mut self) {
		loop {
			if matches!(self.iter.next(), None | Some((_, '\n'))) {
				break;
			}
		}
	}

	fn read_block_comment(&mut self, start: u32) {
		let mut nested_levels = 1usize;
		let mut prev = ' ';
		while nested_levels > 0 {
			let next = match self.iter.next() {
				None => {
					log::error(log::frame(String::from("unclosed block comment"))
						.with_annotation(String::from("comment opened here"), Span::new(self.file, start..(start + 2)))
					).emit(self.cctx);
					break;
				},
				Some((_, ch)) => ch,
			};
			match (prev, next) {
				('/', '*') => {
					nested_levels += 1;
					prev = ' ';
				},
				('*', '/') => {
					nested_levels -= 1;
					prev = ' ';
				},
				(_, c) => prev = c,
			}
		}
	}

	fn read_word(&mut self, start: usize, c: char) -> Option<Token<'cctx>> {
		// for comment detection
		let mut just_slash = if c == '/' { Some(start) } else { None };
		let is_alpha = c.is_alphanumeric() || c == '_';
		let end = loop { match self.iter.peek() {
			Some(&(loc, c)) => if c.is_whitespace() || matches!(Delimiter::new(c), Some(_)) || matches!(Bracket::new(c), Some(_)) {
				break loc;
			} else {
				if (c.is_alphanumeric() || c == '_') ^ is_alpha {
					break loc;
				}
				match (just_slash, c) {
					(Some(loc), '/') => {
						self.read_line_comment();
						break loc;
					},
					(Some(loc), '*') => {
						self.iter.next();
						self.read_block_comment(start as u32);
						break loc;
					},
					_ => (),
				}
				just_slash = if c == '/' { Some(loc) } else { None };
				self.iter.next();
			},
			None => break self.string.len(),
		}};

		let word = if end == start {
			// immediate comment oopsie
			return self.next();
		} else {
			unsafe { self.string.get_unchecked(start..end) }
		};

		// TODO move this out,
		// temporary to get example working
		if let Ok(i) = word.parse::<i64>() {
			return Some(TokenKind::IntegerLiteral(i).at(start..end));
		}

		match word {
			"if" => return Some(TokenKind::KeywordStart(KeywordStart::If).at(start..end)),
			"then" => return Some(TokenKind::KeywordDelim(KeywordDelim::Then).at(start..end)),
			"else" => return Some(TokenKind::KeywordDelim(KeywordDelim::Else).at(start..end)),
			_ => (),
		}

		let word = self.cctx.intern(Box::from(word));
		let kind = if is_alpha {
			TokenKind::Identifier(word)
		} else {
			TokenKind::Operator(word)
		};

		Some(kind.at(start..end))
	}
}

impl<'cctx, 'a> Iterator for Tokens<'cctx, 'a> {
	type Item = Token<'cctx>;

	fn next(&mut self) -> Option<Self::Item> {
		Some( match self.iter.next()? {
			(_, c) if c.is_whitespace() => return self.next(),

			(i, c) if let Some(d) = Delimiter::new(c) => TokenKind::Delimiter(d).at(i..(i + c.len_utf8())),
			(i, c) if let Some(b) = Bracket::new(c) => TokenKind::Bracket(b).at(i..(i + c.len_utf8())),

			(start, c) => self.read_word(start, c)?,
		})
	}
}



/// An expression which does not yet have knowledge of the `repr::Type` or
/// `repr::Value` system, referring to identifiers instead of places.
#[derive(Debug)]
pub enum WrittenExpression<'cctx> {
//	/// A wrapper for what is within a file. Used for now while everything is just
//	/// parsed as one AST between `supreglobal` and `global` but eventually may no
//	/// longer be necessary. Regardless, the information about which file these
//	/// expressions are contained within must reach the next compile stage for
//	/// errors so this will do for now.
//	FileBoundary(File<'cctx>, Box<WrittenExpression<'cctx>>),

	/// A unary operation.
	UnaryOperation((Identifier<'cctx>, Range<u32>), Box<WrittenExpression<'cctx>>),
	/// A binary operation, including function calls under the operator name "".
	BinaryOperation((Identifier<'cctx>, Range<u32>), Box<(WrittenExpression<'cctx>, WrittenExpression<'cctx>)>),

	/// A location referred to by an identifier.
	Ident(Identifier<'cctx>, Range<u32>),
	NameQualified(Box<WrittenExpression<'cctx>>, Identifier<'cctx>, Range<u32>),
	IndexQualified(Box<WrittenExpression<'cctx>>, usize, Range<u32>),

	IntegerLiteral(i64, Range<u32>),

	/// A region bounded by brackets and separating one or more statements by a
	/// common delimiter.
	Region {
		region_inner: RegionInner<'cctx>,
		bracket_kind: BracketKind,
		range: Range<u32>,
	},

	IfThenElse {
		parts: Box<[WrittenExpression<'cctx>; 3]>,
		start: u32,
	},

	/// A parsing error which can be propagated to give more feedback.
	Error(Range<u32>),
}

impl<'cctx> WrittenExpression<'cctx> {
	/// The first character associated with this expression
	pub fn start(&self) -> u32 {
		match self {
			WrittenExpression::UnaryOperation((_, range), _) => range.start,
			WrittenExpression::BinaryOperation((_, _), over) => over.0.start(),

			WrittenExpression::Ident(_, range) => range.start,
			WrittenExpression::NameQualified(over, _, _) => over.start(),
			WrittenExpression::IndexQualified(over, _, _) => over.start(),

			WrittenExpression::IntegerLiteral(_, range) => range.start,

			WrittenExpression::Region { range, .. } => range.start,

			WrittenExpression::IfThenElse { start, .. } => *start,

			WrittenExpression::Error(range) => range.start,
		}
	}

	/// The character index at the end of this expression
	pub fn end(&self) -> u32 {
		match self {
			WrittenExpression::UnaryOperation((_, _), over) => over.end(),
			WrittenExpression::BinaryOperation((_, _), over) => over.1.end(),

			WrittenExpression::Ident(_, range) => range.end,
			WrittenExpression::NameQualified(_, _, range) => range.end,
			WrittenExpression::IndexQualified(_, _, range) => range.end,

			WrittenExpression::IntegerLiteral(_, range) => range.end,

			WrittenExpression::Region { range, .. } => range.end,

			WrittenExpression::IfThenElse { parts, .. } => parts[2].end(),

			WrittenExpression::Error(range) => range.end,
		}
	}

	/// The character range covered by this expression
	pub fn range(&self) -> Range<u32> {
		self.start()..self.end()
	}

	/// Get a span covering `self.range()` within the given file
	pub fn span_in(&self, file: File<'cctx>) -> Span<'cctx> {
		Span::new(file, self.range())
	}
}

impl<'cctx> Display for WrittenExpression<'cctx> {
	fn fmt(&self, f: &mut fmt::Formatter<'_>) -> Result<(), fmt::Error> {
		use WrittenExpression::*;
		use RegionInner::*;
		use BracketDirection::*;

		match self {
			UnaryOperation(s, e) => write!(f, "({}{})", s.0.string(), e),
			BinaryOperation(s, es) if s.0.string().is_empty() => write!(f, "({} {})", es.0, es.1),
			BinaryOperation(s, es) => write!(f, "({} {} {})", es.0, s.0.string(), es.1),

			Ident(s, _) => write!(f, "{}", s.string()),
			NameQualified(e, s, _) => write!(f, "{}.{}", e, s.string()),
			IndexQualified(e, s, _) => write!(f, "{}.{}", e, s),

			IntegerLiteral(v, _) => write!(f, "{}", v),

			Region { region_inner: Undelimited(None), bracket_kind, .. } =>
				write!(f, "{}{}", Bracket(*bracket_kind, Open), Bracket(*bracket_kind, Close)),
			Region { region_inner: Undelimited(Some(stmt)), bracket_kind, .. } =>
				write!(f, "{}{}{}", Bracket(*bracket_kind, Open), stmt, Bracket(*bracket_kind, Close)),
			Region { region_inner: Delimited(stmts, delim), bracket_kind, .. } => {
				write!(f, "{}", Bracket(*bracket_kind, Open))?;
				let mut stmts = stmts.iter();
				match stmts.next() {
					Some(stmt) => {
						write!(f, "{}{}", stmt, delim)?;
						if let Some(stmt) = stmts.next() {
							write!(f, " {}", stmt)?;
							for stmt in stmts {
								write!(f, "{} {}", delim, stmt)?;
							}
						}
					},
					None => write!(f, "{}", delim)?,
				}
				write!(f, "{}", Bracket(*bracket_kind, Close))
			},

			IfThenElse { parts: es, .. } => write!(f, "if {} then {} else {}", es[0], es[1], es[2]),

			Error(_) => write!(f, "<error>"),
		}
	}
}

/// Inside of a region
#[derive(Debug)]
pub enum RegionInner<'cctx> {
	/// Within a region without a delimiter i.e. `()` or `(2 * 3)`
	Undelimited(Option<Box<WrittenStatement<'cctx>>>),
	/// Within a region with delimiters i.e. `(6 - 5, 13, 25)`
	Delimited(Vec<WrittenStatement<'cctx>>, Delimiter),
}



/// A statement with an optional LHS and [`WrittenExpression`] RHS.
#[derive(Debug)]
pub struct WrittenStatement<'cctx> {
	/// Binding side. If `None`, this is an implicit drop.
	pub head: Option<WrittenExpression<'cctx>>,
	/// Evaluation side.
	pub expr: WrittenExpression<'cctx>,
}

impl<'cctx> WrittenStatement<'cctx> {
	pub fn span_in(&self, file: File<'cctx>) -> Span<'cctx> {
		match &self.head {
			None => self.expr.span_in(file),
			Some(head) => Span::new(file, head.start()..self.expr.end()),
		}
	}
}

impl<'cctx> Display for WrittenStatement<'cctx> {
	fn fmt(&self, f: &mut fmt::Formatter<'_>) -> Result<(), fmt::Error> {
		match &self.head {
			Some(head) => write!(f, "{} := {}", head, self.expr),
			None => write!(f, "{}", self.expr),
		}
	}
}



enum EndReasonKind {
	Keyword(KeywordDelim),
	Delimiter(Delimiter),
	Bracket(Option<BracketKind>),
}
impl EndReasonKind {
	fn at(self, span: Range<u32>) -> EndReason {
		EndReason {
			kind: self,
			span,
		}
	}
}
struct EndReason {
	kind: EndReasonKind,
	span: Range<u32>,
}

/// Read expressions from an iterator of [`TokenKind`]s.
pub struct Statements<'cctx, I>
where I: Iterator<Item = Token<'cctx>>, {
	cctx: &'cctx CompileContext<'cctx>,
	file: File<'cctx>,
	toks: I,
	unary_prec: HashMap<String, OperatorPrecedence>,
	binary_prec: HashMap<String, OperatorPrecedence>,
	call_prec: OperatorPrecedence,
	call_op: Identifier<'cctx>,
}

impl<'cctx, I> Statements<'cctx, I>
where I: Iterator<Item = Token<'cctx>>, {
	/// New reader over given string with operator precedences.
	pub fn new<II>(
		cctx: &'cctx CompileContext<'cctx>,
		file: File<'cctx>,
		toks: II,
		unary_prec: HashMap<String, OperatorPrecedence>,
		binary_prec: HashMap<String, OperatorPrecedence>,
		call_prec: OperatorPrecedence,
		call_op: Identifier<'cctx>,
	) -> Self
	where II: IntoIterator<Item = Token<'cctx>, IntoIter = I>, {
		Self {
			cctx,
			file,
			toks: toks.into_iter(),
			unary_prec,
			binary_prec,
			call_prec,
			call_op,
		}
	}

	fn next_reasoned(&mut self) -> (Option<WrittenStatement<'cctx>>, EndReason) {
		enum OpKind { Un, Bi }
		enum Elem<'cctx> {
			Ex(WrittenExpression<'cctx>),
			ExEnd(WrittenExpression<'cctx>, EndReason),
			Op(Identifier<'cctx>, Range<u32>),
		}
		enum ElemKind { Ex, Op }
		struct Op<'cctx> {
			kind: OpKind,
			name: Identifier<'cctx>,
			prec: OperatorPrecedence,
			span: Range<u32>,
		}

		let mut head: Option<(WrittenExpression<'cctx>, Range<u32>)> = None;
		let mut ex_stack: Vec<WrittenExpression<'cctx>> = Vec::new();
		let mut op_stack: Vec<Op<'cctx>> = Vec::new();
		let mut prev_ex = None;

		#[inline]
		fn usurp_ops<'cctx>(
			cctx: &'cctx CompileContext<'cctx>,
			file: File<'cctx>,
			ex_stack: &mut Vec<WrittenExpression<'cctx>>,
			op_stack: &mut Vec<Op<'cctx>>,
			prec: OperatorPrecedence,
			name: Identifier<'cctx>,
			span: Range<u32>,
			mut working_expr: WrittenExpression<'cctx>,
		) {
			while let Some(prev) = op_stack.pop() {
				use std::cmp::Ordering;

				let ordering = match prev.prec.cmp(&prec) {
					Ordering::Less => Ordering::Less,
					Ordering::Equal => match prec.associativity() {
						Some(OperatorPrecedenceAssociativity::Left) => Ordering::Greater,
						Some(OperatorPrecedenceAssociativity::Right) => Ordering::Less,
						None => Ordering::Equal,
					},
					Ordering::Greater => Ordering::Greater,
				};
				match ordering {
					Ordering::Less => {
						op_stack.push(prev);
						break;
					},
					Ordering::Equal => {
						log::error(log::frame(format!(
								"operators `{}` and `{}` of same precedence and incompatible associativity",
								prev.name.string(), name.string(),
							))
							.with_annotation(String::from("first operator"), Span::new(file, prev.span.clone()))
							.with_annotation(String::from("second operator"), Span::new(file, span.clone()))
						).emit(cctx);
						op_stack.push(prev);
						break;
					},
					Ordering::Greater => working_expr = match prev.kind {
						OpKind::Un => {
							WrittenExpression::UnaryOperation((prev.name, prev.span), Box::new(working_expr))
						},
						OpKind::Bi => {
							WrittenExpression::BinaryOperation((prev.name, prev.span), Box::new((ex_stack.pop().unwrap(), working_expr)))
						},
					},
				}
			}

			ex_stack.push(working_expr);
			op_stack.push(Op { kind: OpKind::Bi, name, prec, span });
		}

		#[inline]
		fn usurp_all_ops<'cctx>(
			ex_stack: &mut Vec<WrittenExpression<'cctx>>,
			op_stack: &mut Vec<Op<'cctx>>,
			mut working_expr: WrittenExpression<'cctx>,
		) -> WrittenExpression<'cctx> {
			while let Some(op) = op_stack.pop() {
				working_expr = match op.kind {
					OpKind::Un => {
						WrittenExpression::UnaryOperation((op.name, op.span), Box::new(working_expr))
					},
					OpKind::Bi => {
						WrittenExpression::BinaryOperation((op.name, op.span), Box::new((ex_stack.pop().unwrap(), working_expr)))
					},
				};
			}
			working_expr
		}

		let end_reason = loop {
			let next = match self.toks.next() {
				Some(t) => t,
				None => {
					let len = self.cctx.file_len(self.file);
					break EndReasonKind::Bracket(None).at(len..len)
				},
			};
			let elem = match next.kind {
				TokenKind::Identifier(i) => Elem::Ex(WrittenExpression::Ident(i, next.range)),
				TokenKind::Operator(o) => Elem::Op(o, next.range),
				TokenKind::IntegerLiteral(v) => Elem::Ex(WrittenExpression::IntegerLiteral(v, next.range)),
				TokenKind::Bracket(Bracket(bracket_kind, BracketDirection::Open)) => {
					let (region_inner, range) = self.in_region(Some(bracket_kind), next.range.start);
					Elem::Ex(WrittenExpression::Region { region_inner, bracket_kind, range })
				},
				TokenKind::Bracket(Bracket(b, BracketDirection::Close)) => break EndReasonKind::Bracket(Some(b)).at(next.range),
				TokenKind::Delimiter(d) => break EndReasonKind::Delimiter(d).at(next.range),
				TokenKind::KeywordStart(w) => {
					let (expr, end_reason) = self.in_words(w, next.range.start);
					Elem::ExEnd(expr, end_reason)
				},
				TokenKind::KeywordDelim(w) => break EndReasonKind::Keyword(w).at(next.range),
			};
			match elem {
				Elem::Ex(ex) => {
					if let Some(pex) = prev_ex {
						let start = ex.start();
						usurp_ops(
							self.cctx,
							self.file,
							&mut ex_stack,
							&mut op_stack,
							self.call_prec,
							self.call_op,
							(start - 1)..start,
							pex,
						);
					}
					prev_ex = Some(ex);
				},
				Elem::ExEnd(ex, end) => {
					if let Some(pex) = prev_ex {
						let start = ex.start();
						usurp_ops(
							self.cctx,
							self.file,
							&mut ex_stack,
							&mut op_stack,
							self.call_prec,
							self.call_op,
							(start - 1)..start,
							pex,
						);
					}
					prev_ex = Some(ex);
					break end;
				},
				Elem::Op(s, span) => match (std::mem::take(&mut prev_ex), s.string()) {
					(Some(pex), ".") => prev_ex = match self.toks.next() {
						Some(Token { kind: TokenKind::Identifier(name), range }) =>
							Some(WrittenExpression::NameQualified(Box::new(pex), name, range)),
						Some(Token { kind: TokenKind::IntegerLiteral(idx), range }) =>
							Some(WrittenExpression::IndexQualified(Box::new(pex), usize::try_from(idx).unwrap(), range)),
						Some(Token { range: next_range, .. }) => log::error(log::frame(String::from("`.` followed by non-identifier"))
							.with_annotation(String::from("encountered here"), Span::new(self.file, span.start..next_range.end))
						).emit_fatal(self.cctx, log::exit_code(40)),
						None => log::error(log::frame(String::from("unexpected end of file following `.`"))
							.with_annotation(String::from("encountered here"), Span::new(self.file, span))
						).emit_fatal(self.cctx, log::exit_code(41)),
					},
					(Some(pex), ":=") => {
						match &mut head {
							// if already there just assume first was intended, in the case where you
							// add an assignment without realizing one was already present
							Some(h) => {
								log::error(log::frame(String::from("multiple `:=`s in statement"))
									.with_annotation(String::from("previous `:=` here"), Span::new(self.file, h.1.clone()))
									.with_annotation(String::from("duplicate `:=` here"), Span::new(self.file, span.clone()))
								).emit(self.cctx);
								h.1 = span;
							}
							None => head = Some((usurp_all_ops(&mut ex_stack, &mut op_stack, pex), span)),
						}
					},
					(Some(pex), op_name) => {
						let prec = match self.binary_prec.get(op_name) {
							Some(ps) => ps,
							None => {
								log::error(log::frame(format!("unknown binary operator `{}`", s.string()))
									.with_annotation(String::from("encountered here"), Span::new(self.file, span))
								).emit(self.cctx);
								let head = head.map(|(e, _)| e);
								return (
									Some(WrittenStatement { head, expr: WrittenExpression::Error(0..0) }),
									self.next_reasoned().1,
								);
							},
						};
						usurp_ops(
							self.cctx,
							self.file,
							&mut ex_stack,
							&mut op_stack,
							*prec,
							s,
							(span.start as u32)..(span.end as u32),
							pex,
						);
					},
					(None, ":=") => {
						if op_stack.is_empty() {
							log::error(log::frame(String::from("unexpected `:=`"))
								.with_annotation(String::from("encountered here"), Span::new(self.file, span))
							).emit(self.cctx);
						} else if op_stack.len() == 1 && ex_stack.is_empty() {
							let op = op_stack.pop().unwrap();
							head = Some((WrittenExpression::Ident(op.name, op.span), span));
						} else {
							let span = op_stack.pop().unwrap().span;
							log::error(log::frame(String::from("trailing operators"))
								.with_annotation(String::from("encountered here"), Span::new(self.file, span))
							).emit(self.cctx);
							head = Some((WrittenExpression::Error(0..0), 0..0));
						}
					},
					(None, op_name) => {
						let prec = match self.unary_prec.get(op_name) {
							Some(p) => p,
							None => {
								log::error(log::frame(format!("unknown unary operator `{}`", s.string()))
									.with_annotation(String::from("encountered here"), Span::new(self.file, span))
								).emit(self.cctx);
								let head = head.map(|(e, _)| e);
								return (
									Some(WrittenStatement { head, expr: WrittenExpression::Error(0..0) }),
									self.next_reasoned().1,
								);
							},
						};
						op_stack.push(Op {
							kind: OpKind::Un,
							name: s,
							prec: *prec,
							span: (span.start as u32)..(span.end as u32),
						});
					},
				},
			}
		};

		let expr = match prev_ex {
			Some(pex) => Some(usurp_all_ops(&mut ex_stack, &mut op_stack, pex)),
			None if op_stack.is_empty() => None,
			None if op_stack.len() == 1 && ex_stack.is_empty() => {
				let op = op_stack.pop().unwrap();
				Some(WrittenExpression::Ident(op.name, op.span))
			},
			None => {
				let span = op_stack.pop().unwrap().span;
				log::error(log::frame(String::from("trailing operators"))
					.with_annotation(String::from("encountered here"), Span::new(self.file, span))
				).emit(self.cctx);
				Some(WrittenExpression::Error(0..0))
			},
		};
		let stmt = match (head, expr) {
			(None, None) => None,
			(Some((head, decl_span)), None) => {
				log::error(log::frame(String::from("unexpected end of statement following `:=`"))
					.with_annotation(String::from("expected expression following this"), Span::new(self.file, decl_span))
				).emit(self.cctx);
				Some(WrittenStatement { head: Some(head), expr: WrittenExpression::Error(0..0) })
			},
			(head, Some(expr)) => Some(WrittenStatement { head: head.map(|(e, _)| e), expr }),
		};
		(stmt, end_reason)
	}


	fn in_region(&mut self, expecting: Option<BracketKind>, starting: u32) -> (RegionInner<'cctx>, Range<u32>) {
		use RegionInner::*;

		let mut cur: Option<(Vec<WrittenStatement>, Delimiter)> = None;
		loop {
			let (stmt_maybe, end_reason) = self.next_reasoned();
			cur = match (cur, end_reason.kind) {
				(_, EndReasonKind::Bracket(encountered)) if encountered != expecting => log::error(
					log::frame(format!(
						"expected closing {}, encountered {}",
						CloseOrEOF(expecting), CloseOrEOF(encountered),
					))
					.with_annotation(String::from("encountered here"), Span::new(self.file, end_reason.span))
				).emit_fatal(self.cctx, log::exit_code(10)),

				(None, EndReasonKind::Bracket(_)) => break (
					Undelimited(stmt_maybe.map(Box::new)),
					starting..end_reason.span.end,
				),

				(Some((mut stmts, del)), EndReasonKind::Bracket(_)) => {
					if let Some(stmt) = stmt_maybe {
						stmts.push(stmt);
					}
					break (Delimited(stmts, del), starting..end_reason.span.end);
				},

				(None, EndReasonKind::Delimiter(cur_del)) => Some((
					match stmt_maybe {
						Some(stmt) => vec![stmt],
						None => Vec::new(),
					},
					cur_del,
				)),
				(Some((mut stmts, del)), EndReasonKind::Delimiter(cur_del)) => {
					if cur_del != del {
						log::error(
							log::frame(format!(
								"expected delimiter `{}`, encountered `{}`",
								del,
								cur_del
							))
							.with_annotation(String::from("encountered here"), Span::new(self.file, end_reason.span))
						).emit(self.cctx);
					}
					if let Some(stmt) = stmt_maybe {
						stmts.push(stmt);
					}
					Some((stmts, del))
				},

				(_, EndReasonKind::Keyword(word)) => log::error(
					log::frame(format!("unexpected keyword `{}`", word))
					.with_annotation(String::from("encountered here"), Span::new(self.file, end_reason.span))
				).emit_fatal(self.cctx, log::exit_code(11)),
			}
		}
	}

	fn in_words(&mut self, start_word: KeywordStart, starting: u32) -> (WrittenExpression<'cctx>, EndReason) {
		match start_word {
			KeywordStart::If => {
				let (stmt, end_reason) = self.next_reasoned();
				let stmt = if let Some(stmt) = stmt {
					stmt
				} else {
					// TODO make this message better
					log::error(log::frame(String::from("unexpected end of expression"))).emit(self.cctx);
					WrittenStatement {
						head: None,
						expr: WrittenExpression::Error(0..0),
					}
				};

				if let Some(head) = stmt.head {
					log::error(
						log::frame(String::from("unexpected assignment in `if` predicate"))
						.with_annotation(String::from("encountered here"), Span::new(self.file, head.range()))
					).emit(self.cctx);
				}
				match end_reason.kind {
					EndReasonKind::Keyword(KeywordDelim::Then) => (),
					EndReasonKind::Keyword(w) => {
						log::error(
							log::frame(format!("expected `then`, encountered `{}`", w))
							.with_annotation(String::from("encountered here"), Span::new(self.file, end_reason.span))
						).emit(self.cctx);
					},
					EndReasonKind::Delimiter(d) => {
						log::error(
							log::frame(format!("expected `then`, encountered `{}`", d))
							.with_annotation(String::from("encountered here"), Span::new(self.file, end_reason.span))
						).emit(self.cctx);
					},
					EndReasonKind::Bracket(b) => {
						log::error(
							log::frame(format!("expected `then`, encountered {}", CloseOrEOF(b)))
							.with_annotation(String::from("encountered here"), Span::new(self.file, end_reason.span))
						).emit(self.cctx);
					},
				}
				let pred = stmt.expr;

				let (stmt, end_reason) = self.next_reasoned();
				let stmt = if let Some(stmt) = stmt {
					stmt
				} else {
					// TODO make this message better
					log::error(log::frame(String::from("unexpected end of expression"))).emit(self.cctx);
					WrittenStatement {
						head: None,
						expr: WrittenExpression::Error(0..0),
					}
				};

				if let Some(head) = stmt.head {
					log::error(
						log::frame(String::from("unexpected assignment in `then` expression"))
						.with_annotation(String::from("encountered here"), Span::new(self.file, head.range()))
					).emit(self.cctx);
				}
				match end_reason.kind {
					EndReasonKind::Keyword(KeywordDelim::Else) => (),
					EndReasonKind::Keyword(w) => {
						log::error(
							log::frame(format!("expected `else`, encountered `{}`", w))
							.with_annotation(String::from("encountered here"), Span::new(self.file, end_reason.span))
						).emit(self.cctx);
					},
					EndReasonKind::Delimiter(d) => {
						log::error(
							log::frame(format!("expected `else`, encountered `{}`", d))
							.with_annotation(String::from("encountered here"), Span::new(self.file, end_reason.span))
						).emit(self.cctx);
					},
					EndReasonKind::Bracket(b) => {
						log::error(
							log::frame(format!("expected `else`, encountered {}", CloseOrEOF(b)))
							.with_annotation(String::from("encountered here"), Span::new(self.file, end_reason.span))
						).emit(self.cctx);
					},
				}
				let expr_true = stmt.expr;

				let (stmt, end_reason) = self.next_reasoned();
				let stmt = if let Some(stmt) = stmt {
					stmt
				} else {
					// TODO make this message better
					log::error(log::frame(String::from("unexpected end of expression"))).emit(self.cctx);
					WrittenStatement {
						head: None,
						expr: WrittenExpression::Error(0..0),
					}
				};

				if let Some(head) = stmt.head {
					log::error(
						log::frame(String::from("unexpected assignment in `else` expression"))
						.with_annotation(String::from("encountered here"), Span::new(self.file, head.range()))
					).emit(self.cctx);
				}
				let expr_false = stmt.expr;

				let parts = Box::new([pred, expr_true, expr_false]);
				(WrittenExpression::IfThenElse { parts, start: starting }, end_reason)
			},
		}
	}


	/// Read entire input stream into list of expressions and their separating delimiter.
	pub fn all(mut self) -> (RegionInner<'cctx>, Range<u32>) {
		self.in_region(None, 0)
	}
}
