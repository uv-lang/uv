//! Tasks and data which span many parts of the compilation process and so are
//! stored centrally.



use std::{
	cell::OnceCell,
	collections::{
	//	HashMap,
		HashSet,
	},
	marker::PhantomData,
	ops::Range,
	path::{
		Path,
	},
	sync::{
		Mutex,
		RwLock,
	},
};

use crate::{
	cache::{
		BlockedCache,
		LivePtr,
	},
	log::{
		self,
		Location,
	},
	repr::{
		self,
		unify,
	},
};



/// A file known about by the [`CompileContext`].
///
/// Opaque. Access to associated [`FileData`] must be requested through
/// [`CompileContext::with_file`].
#[derive(Clone, Copy, Debug, Eq, Hash, PartialEq)]
pub struct File<'cctx> {
	_marker: PhantomData<fn(&'cctx ()) -> &'cctx ()>,
	value: usize,
}

/// Backing data for a [`File`]. Contains name, content, and line numberings.
// Files longer than 2³² bytes (4 GB) are not currently supported.
pub struct FileData {
	/// Name of the file as retrieved
	pub name: String,
	/// Contents of the file
	// TODO maybe only get this back on error emit?
	pub string: String,
	/// Indices representing the starts of lines
	pub(crate) lines: Vec<u32>,
}

impl FileData {
	pub fn location_from_range(&self, range: Range<u32>) -> Location {
		let line = self.lines.binary_search(&range.start).unwrap_or_else(|i| i - 1);
		let line_start = self.lines[line];
		let char_off = self.string[(line_start as usize)..(range.start as usize)].chars().count();

		Location {
			file_name: &self.name,
			line_number: line,
			chars_into_line: char_off,
		}
	}
}

/// Area in a file, such as for snippets in compiler diagnostics.
#[derive(Clone, Debug)]
pub struct Span<'cctx> {
	/// File in which this is taken from
	pub file: File<'cctx>,
	/// Byte range within the file
	pub range: Range<u32>,
}

impl<'cctx> Span<'cctx> {
	pub fn new(file: File<'cctx>, range: Range<u32>) -> Self {
		Self { file, range }
	}

	pub fn through(mut self, other: Span<'cctx>) -> Self {
		debug_assert_eq!(self.file, other.file);
		debug_assert!(self.range.end <= other.range.start);
		self.range.end = other.range.end;
		self
	}
}



/// The data behind an [`Place`]. An internal representation, name, and span.
#[derive(Debug)]
pub struct PlaceData<'cctx> {
	pub name: OnceCell<&'cctx str>,
	pub expr: OnceCell<repr::Expr<'cctx>>,
	pub cached_type: OnceCell<repr::Expr<'cctx>>,
	pub span: Span<'cctx>,
}



/// An interned string.
#[derive(Clone, Copy, Debug, Eq, Hash, PartialEq)]
pub struct Identifier<'cctx> {
	_marker: PhantomData<fn(&'cctx ()) -> &'cctx ()>,
	ptr: LivePtr<'cctx, str>,
}

impl<'cctx> Identifier<'cctx> {
	#[allow(clippy::borrowed_box)]
	unsafe fn from_str(ptr: &Box<str>) -> Self {
		let ptr: *const str = &**ptr;
		let ptr: &'cctx str = &*ptr;
		Self {
			_marker: Default::default(),
			ptr: LivePtr::new(ptr),
		}
	}

	/// The string representation of this identifier. This should not be used for
	/// any logic, only for error messages.
	pub fn string(&self) -> &'cctx str {
		self.ptr.to_ref()
	}
}

pub struct KnownIdentifiers<'cctx> {
	pub colon: Identifier<'cctx>,
	//pub empty: Identifier<'cctx>,
	pub underscore: Identifier<'cctx>,
}


/*
/// Special operators which are needed to be known for parsing.
pub struct SpecialOperators<'cctx> {
	/// Function application.
	pub application: Identifier<'cctx>,
	/// Type ascription.
	pub ascription: Identifier<'cctx>,
}
*/


/// Context for the entire compilation, taking queries and storing
/// intermediates and results.
pub struct CompileContext<'cctx> {
	_marker: PhantomData<fn(&'cctx ()) -> &'cctx ()>,

	#[cfg(feature = "llvm")]
	llvm: inkwell::context::Context,
	log: Mutex<Box<dyn log::Log>>,

	strings: Mutex<HashSet<Box<str>>>,
	pub identifiers: KnownIdentifiers<'cctx>,

	pub(crate) files: RwLock<Vec<FileData>>,
	//files_by_name: Mutex<HashMap<PathBuf, File<'cctx>>>,
	pub places: BlockedCache<PlaceData<'cctx>, 4096>,
	//results: QueryResults<'cctx>,
}

/// An [`Expr`] which may be referred to and so requires linearity checking.
///
/// [`Expr`]: repr::Expr
pub type Place<'cctx> = LivePtr<'cctx, PlaceData<'cctx>>;

impl<'cctx>
CompileContext<'cctx> {
	/// Create an empty `CompileContext`.
	#[allow(clippy::new_ret_no_self)]
	#[inline]
	pub fn new<ContinuationResult, L: log::Log + 'static, F>(log: L, continuation: F) -> ContinuationResult
	where for<'new_cctx>
	      F: FnOnce(&'new_cctx CompileContext<'new_cctx>) -> ContinuationResult {
		let mut strings = HashSet::new();
		let identifiers = {
			let colon = strings.get_or_insert(":".into());
			let colon = unsafe { Identifier::from_str(colon) };
			//let empty = strings.get_or_insert("".into());
			//let empty = unsafe { Identifier::from_str(empty) };
			let underscore = strings.get_or_insert("_".into());
			let underscore = unsafe { Identifier::from_str(underscore) };
			KnownIdentifiers {
				colon,
				//empty,
				underscore,
			}
		};

		continuation(&CompileContext {
			_marker: Default::default(),

			#[cfg(feature = "llvm")]
			llvm: inkwell::context::Context::create(),
			log: Mutex::new(Box::new(log)),

			strings: Mutex::new(strings),
			identifiers,

			files: RwLock::new(Vec::new()),
			//files_by_name: Mutex::new(HashMap::new()),
			places: BlockedCache::new(),
			//results: QueryResults::new(),
		})
		// TODO why does this think that ContinuationResult could hold onto its
		// reference into cctx? can't put anything mutable or consuming afterwards
		// here and same goes for custom Drop impls. currently only an issue with
		// regards to BlockedCache bc it can't drop its members
	}


	/// Read a source file into context. In addition to the usual errors emittable
	/// on file reads, the file must also not exceed [`u32::MAX`] bytes in length.
	pub fn read_file(&'cctx self, name: String) -> std::io::Result<File<'cctx>> {
		let path = Path::new(&name).canonicalize()?;
		let string = std::fs::read_to_string(&path)?;

		self.new_file(name, string)
	}

	/// Construct a file which does not correspond to any file on the filesystem.
	pub fn new_file(&'cctx self, name: String, string: String) -> std::io::Result<File<'cctx>> {
		let max = match usize::try_from(u32::MAX) {
			Ok(m) => m,
			Err(_) => usize::MAX,
		};
		if string.len() > max {
			use std::io::{Error, ErrorKind};
			return Err(Error::new(ErrorKind::Other, "source files larger than 4 gigabytes are not supported"));
		}

		let mut lines = Vec::new();
		let mut was_just_newline = true;
		for (i, c) in string.char_indices() {
			if was_just_newline {
				lines.push(i as u32);
			}
			was_just_newline = c == '\n';
		}
		let idx: usize;
		{
			let mut lock = self.files.write().unwrap();
			idx = lock.len();
			lock.push(FileData { name, string, lines });
			std::mem::drop(lock);
		}

		Ok(File {
			_marker: Default::default(),
			value: idx,
		})
	}

	/// Access the data associated with a source file under shared ownership.
	pub fn with_file<ContinuationResult, F>(&'cctx self, idx: File<'cctx>, continuation: F) -> ContinuationResult
	where F: FnOnce(&FileData) -> ContinuationResult {
		continuation(&self.files.read().unwrap()[idx.value])
	}

	/// Get the length of a source file in bytes.
	pub fn file_len(&'cctx self, idx: File<'cctx>) -> u32 {
		self.with_file(idx, |f| f.string.len() as u32)
	}


	/// Write a diagnostic to this [`CompileContext`]'s log. [`Diagnostic::emit`]
	/// is equivalent and should be preferred.
	///
	/// [`Diagnostic::emit`]: log::Diagnostic::emit
	pub(crate) fn emit_diagnostic(&'cctx self, diagnostic: log::Diagnostic<'cctx>) -> std::io::Result<log::ErrorEmitted> {
		let mut log = self.log.lock().unwrap();
		log.write_diagnostic(self, diagnostic)
	}


	/// Identify a string with those of the same value, returning shared reference
	/// access to the data.
	pub fn intern(&'cctx self, string: Box<str>) -> Identifier<'cctx> {
		let mut strings = self.strings.lock().unwrap();
		let res: &str = strings.get_or_insert(string);
		let res: &'cctx str = unsafe { &*(res as *const _) };
		Identifier {
			_marker: PhantomData,
			ptr: LivePtr::new(res),
		}
	}

	pub fn intern_cloned(&'cctx self, string: &str) -> Identifier<'cctx> {
		let mut strings = self.strings.lock().unwrap();
		let res: &str = strings.get_or_insert_with(string, |s| Box::from(s));
		let res: &'cctx str = unsafe { &*(res as *const _) };
		Identifier {
			_marker: PhantomData,
			ptr: LivePtr::new(res),
		}
	}

	/*
	pub fn special_operators(&'cctx self) -> SpecialOperators<'cctx> {
		// could put this all in one lock of `strings` but not too important…
		SpecialOperators {
			application: self.intern(Box::from("")),
			ascription: self.intern(Box::from(":")),
		}
	}
	*/


	/// Create a new slot for a place.
	pub fn new_place(
		&'cctx self,
		name: Option<&'cctx str>,
		kind: Option<repr::Expr<'cctx>>,
		span: Span<'cctx>,
	) -> Place<'cctx> {
		// for common patterns we can provide more helpful names upstream
		// or even avoid making a new place entirely
		if let Some(repr::Expr::Use { place }) = kind {
			if let Some(name) = name {
				let _ = place.to_ref().name.set(name);
			}
			return place;
		} else if let Some(repr::Expr::Ascribe { ex, .. }) = &kind {
			if let repr::Expr::Use { place } = **ex {
				if let Some(name) = name {
					let _ = place.to_ref().name.set(name);
				}
			}
		}

		let name_cell = name.map(OnceCell::from).unwrap_or_default();
		let expr_cell = kind.map(OnceCell::from).unwrap_or_default();

		Place::new(self.places.insert(PlaceData {
			name: name_cell,
			expr: expr_cell,
			cached_type: OnceCell::new(),
			span,
		}))
	}

	/// Shortening of [`CompileContext::new_place`] for unnamed intermediate expressions
	pub fn place(&'cctx self, expr: repr::Expr<'cctx>, span: Span<'cctx>) -> Place<'cctx> {
		self.new_place(None, Some(expr), span)
	}


	/*
	#[inline(always)]
	fn place_index(&'cctx self, idx: Place<'cctx>) -> CacheIndex<'places> {
		// SAFETY:
		// this is only for lifetime maneuvers with self references within
		// `self.places`. the value is never changed since receiving it as a
		// `CacheIndex` and casting so the index will remain valid.
		unsafe { CacheIndex::new_unchecked(idx.value) }
	}
	*/

	/*
	pub fn with_place<ContinuationResult, F>(&'cctx self, idx: Place<'cctx>, continuation: F) -> ContinuationResult
	where F: FnOnce(&PlaceData<'cctx>) -> ContinuationResult {
		continuation(&self.places.read().unwrap()[self.place_index(idx)])
	}

	fn with_place_mut<ContinuationResult, F>(&'cctx self, idx: Place<'cctx>, continuation: F) -> ContinuationResult
	where F: FnOnce(&mut PlaceData<'cctx>) -> ContinuationResult {
		continuation(&mut self.places.write().unwrap()[self.place_index(idx)])
	}
	*/


	/*
	fn assert_expressions_acyclic_ctx(
		&'cctx self,
		lock: &std::sync::RwLockReadGuard<'_, CacheVec<PlaceData<'cctx>>>,
		cur: Linked<Place<'cctx>>,
		settled: &mut HashSet<Place<'cctx>>,
	) -> Result<(), !> {
		let mut deps = lock[self.place_index(cur.head)].expr.as_ref().unwrap().evaluation_dependencies();
		deps.retain(|dep| !settled.contains(dep));

		for (idx, in_use) in Linked::iter(Some(&cur)).enumerate() {
			if deps.contains(in_use) {
				let name = self.get_name(*in_use);
				let span = self.get_span(*in_use);
				let intermediate_names =
					Linked::iter(Some(&cur))
					.take(idx)
					.filter_map(|ex| self.get_name(*ex).map(|name| (*ex, name)))
					.collect::<Vec<_>>();

				if intermediate_names.is_empty() {
					log::error(
						match name {
							None => log::frame(String::from("definition for an unnamed expression depends on itself"))
								.with_annotation(String::from("here"), span),
							Some(name) => log::frame(format!("definition for `{}` depends on itself", name))
								.with_annotation(format!("`{}` defined here", name), span),
						}
					).emit_fatal(self, log::exit_code(50));
				}

				let err = match name {
					None => log::frame(String::from("definition cycle encountered for an unnamed expression …"))
						.with_annotation(String::from("here"), span),
					Some(name) => log::frame(format!("definition cycle encountered for `{}` …", name))
						.with_annotation(format!("`{}` defined here", name), span),
				};

				let mut err = log::error(err);

				for (ex, name) in intermediate_names.into_iter().rev() {
					let span = self.get_span(ex);
					let frame = log::frame(format!("… which depends on `{}` …", name))
						.with_annotation(format!("`{}` defined here", name), span);
					err = err.with_note(frame);
				}
				err = match name {
					None => err.with_note(log::frame(String::from("… which depends on the original expression"))),
					Some(name) => err.with_note(log::frame(format!("… which depends on `{}`", name))),
				};

				err.emit_fatal(self, log::exit_code(50));
			}
		}

		for dep in deps {
			self.assert_expressions_acyclic_ctx(lock, Linked { head: dep, rest: Some(&cur) }, settled)?;
		}

		settled.insert(cur.head);

		Ok(())
	}

	pub fn assert_expressions_acyclic(&'cctx self) -> Result<(), !> {
		let lock = self.places.read().unwrap();
		// expressions we know are acyclic
		// if you run into one of these in dependencies, you can chop that check early
		let mut settled = HashSet::new();
		for i in 0..lock.len() {
			let ex = Place { _marker: Default::default(), value: i };
			self.assert_expressions_acyclic_ctx(
				&lock,
				Linked { head: ex, rest: None },
				&mut settled,
			)?;
		}

		Ok(())
	}
	*/
}



/*
// probably make this a trait or struct with fn* eventually

/// Equivalent of `rustc`'s query system but worse. A compilation request which
/// may depend on some others.
#[derive(Clone, Copy, Debug, Eq, Hash, PartialEq)]
pub enum QueryKind<'cctx> {
	/*
	TokensFromFile {
		filename: Box<std::path::Path>,
	},
	WrittenExpressionFromFile {
		filename: Box<std::path::Path>,
	},
	ExpressionFromFile {
		filename: Box<std::path::Path>,
	},
	*/

	CheckType {
		place: Place<'cctx>,
		against_type: LivePtr<'cctx, repr::Expr<'cctx>>,
	},
	SynthType {
		place: Place<'cctx>,
	},
	Evaluate {
		place: Place<'cctx>,
	},
	#[cfg(feature = "llvm")]
	Codegen {
		place: Place<'cctx>,
	}
}

/*
pub struct QueryData<'cctx> {
	kind: QueryKind<'cctx>,
}
*/

impl<'cctx> QueryKind<'cctx> {
	fn direct_dependencies(&self) -> HashSet<QueryKind<'cctx>> {
		match self {
			QueryKind::CheckType { .. } => todo!(), //place.to_ref().expr.get().unwrap().check_dependencies(),
			QueryKind::SynthType { place } => place.to_ref().expr.get().unwrap().synth_dependencies(),
			QueryKind::Evaluate { place } => place.to_ref().expr.get().unwrap().evaluation_dependencies(),
			#[cfg(feature = "llvm")]
			QueryKind::Codegen { place } => place.to_ref().expr.get().unwrap().codegen_dependencies(),
		}
	}
}

struct QueryResults<'cctx> {
	evaluations: HashMap<Place<'cctx>, repr::Value<'cctx>>,
}

impl<'cctx> Default for QueryResults<'cctx> {
	fn default() -> Self {
		Self { evaluations: Default::default() }
	}
}

impl<'cctx> QueryResults<'cctx> {
	pub fn new() -> Self {
		Self::default()
	}
}

impl<'cctx> CompileContext<'cctx> {
	/// Prepare for obtaining the result of this query.
	///
	/// Checks that there are no dependency cycles required to resolve this query.
	pub fn prepare(&'cctx self, query: QueryKind<'cctx>) -> log::Result<()> {
		let mut to_prepare: HashMap<QueryKind<'cctx>, _> = HashMap::new();
		{
			let mut adding = HashSet::new();
			adding.insert(query);
			while !adding.is_empty() {
				let mut new_adding = HashSet::new();
				for q in adding {
					let deps = q.direct_dependencies();
					new_adding.extend(deps.iter().copied().filter(|d| *d != q && !to_prepare.contains_key(d)));
					to_prepare.insert(q, (deps, Cell::new(false)));
				}
				adding = new_adding
			}
			/*
			loop {
				let add_deps: Vec<_> = adding.iter().map(|q| (*q, (q.direct_dependencies(), Cell::new(false)))).collect();
				adding = add_deps.iter().flat_map(|(_, (deps, _))| deps).copied().collect();
				let prev_len = to_prepare.len();
				to_prepare.extend(add_deps.into_iter());
				if to_prepare.len() == prev_len {
					break;
				}
			}
			*/
		}

		let mut sequence = Vec::new();

		loop {
			for (_, (deps, taking)) in to_prepare.iter() {
				if !deps.iter().any(|dep| to_prepare.contains_key(dep)) {
					taking.set(true);
				}
			}

			let prev_len = to_prepare.len();
			sequence.extend(to_prepare.drain_filter(|_, (_, taking)| taking.get()).map(|(q, _)| q));
			if to_prepare.is_empty() {
				break;
			} else if to_prepare.len() == prev_len {
				// TODO better error message here
				return Err(log::error(log::frame(String::from("dependency loop encountered"))).emit(self));
			}
		}

		/*
		// TODO do we need to do anything here?
		for query in sequence {
			match query {
				QueryKind::CheckType { .. } => todo!(),
				QueryKind::SynthType { .. } => todo!(),
				QueryKind::Evaluate { place: _, eval_ctx: _ } => todo!(),
			}
		}
		*/

		Ok(())
	}
}
*/



/*
#[derive(Clone, Debug)]
enum EvaluationAction<'cctx> {
	Check(Place<'cctx>),
	Synth(Place<'cctx>),
	Reduce(Place<'cctx>),
	HeadReduce(Place<'cctx>),
	Apply(Place<'cctx>),
}
*/

#[derive(Clone, Debug, Default)]
pub struct EvaluationContext<'cctx> {
	synthing: HashSet<Place<'cctx>>,
	//evaluating: HashSet<Place<'cctx>>,
	reducing: HashSet<Place<'cctx>>,
	head_reducing: HashSet<Place<'cctx>>,
	applying: HashSet<Place<'cctx>>,
	filling_solutions: HashSet<Place<'cctx>>,
	unknown_dependency_checking: HashSet<Place<'cctx>>,
	//head_evaluating: HashSet<Place<'cctx>>,
	// #[cfg(feature = "llvm")]
	// codegenerating: HashSet<Place<'cctx>>,
}

impl<'cctx> EvaluationContext<'cctx> {
	pub fn new() -> Self {
		Self::default()
	}

	/*
	pub fn synth(&mut self, place: Place<'cctx>, bounds: &mut Vec<unify::Bound<'cctx>>) -> Option<repr::Expr<'cctx>> {
		if let Some(ty) = place.to_ref().cached_type.get() {
			return Some(ty.clone());
		}
		if self.synthing.contains(&place) {
			todo!()
		} else {
			self.synthing.insert(place);
			let res = place.to_ref().expr.get().unwrap().synth(self, bounds);
			self.synthing.remove(&place);
			if let Some(res) = &res {
				let _ = place.to_ref().cached_type.set(res.clone());
			}
			res
		}
	}
	*/

	/*
	pub fn evaluate(&mut self, place: Place<'cctx>, env: Option<&repr::Environment<repr::Value<'cctx>>>) -> Option<repr::Value<'cctx>> {
		if self.evaluating.contains(&place) {
			todo!()
		} else {
			self.evaluating.insert(place);
			let res = place.to_ref().expr.get().unwrap().clone().evaluate_ctx(self, env);
			self.evaluating.remove(&place);
			res
		}
	}
	*/
	
	pub fn reduce(&mut self, place: Place<'cctx>) -> log::Result<repr::Expr<'cctx>> {
		if self.reducing.contains(&place) {
			todo!()
		} else {
			self.reducing.insert(place);
			let mut res = place.to_ref().expr.get().unwrap().clone();
			res.reduce(self)?;
			self.reducing.remove(&place);
			Ok(res)
		}
	}
	
	pub fn head_reduce(&mut self, place: Place<'cctx>) -> log::Result<repr::Expr<'cctx>> {
		if self.head_reducing.contains(&place) {
			todo!()
		} else {
			self.head_reducing.insert(place);
			let mut res = place.to_ref().expr.get().unwrap().clone();
			res.head_reduce(self)?;
			self.head_reducing.remove(&place);
			Ok(res)
		}
	}

	pub fn apply(&mut self, place: Place<'cctx>, env: &repr::Environment<repr::Expr<'cctx>>) -> repr::Expr<'cctx> {
		if self.applying.contains(&place) {
			todo!()
		} else {
			self.applying.insert(place);
			let mut res = place.to_ref().expr.get().unwrap().clone();
			res.apply(self, env);
			self.applying.remove(&place);
			res
		}
	}

	/*
	pub fn fill_solutions<'expr>(&mut self, place: Place<'cctx>, solns: &unify::Unknowns<'cctx, 'expr>) -> repr::Expr<'cctx> {
		if self.filling_solutions.contains(&place) {
			todo!()
		} else {
			self.filling_solutions.insert(place);
			let mut res = place.to_ref().expr.get().unwrap().clone();
			res.fill_solutions(self, solns);
			self.filling_solutions.remove(&place);
			res
		}
	}
	*/

	pub fn unknown_deps<'expr>(&mut self, place: Place<'cctx>, extend: &mut HashSet<usize>) {
		if self.unknown_dependency_checking.contains(&place) {
			todo!()
		} else {
			self.unknown_dependency_checking.insert(place);
			let res = place.to_ref().expr.get().unwrap().clone();
			res.unknown_deps(self, extend);
			self.unknown_dependency_checking.remove(&place);
		}
	}

	/*
	pub fn head_evaluate(&mut self, place: Place<'cctx>, env: Option<&repr::Environment<repr::Expr<'cctx>>>) -> Option<repr::HeadNorm<'cctx>> {
		if self.head_evaluating.contains(&place) {
			eprintln!("cycle encountered aaaaa");
			todo!()
		} else {
			self.head_evaluating.insert(place);
			let res = place.to_ref().expr.get().unwrap().clone().head_evaluate(self, env);
			self.head_evaluating.remove(&place);
			res
		}
	}
	*/
}
