//! Compiler logging, warnings, and errors.
//!
//! Short names with the aim to make importing at the module level convenient.

// TODO eventually also write to a file, even notes when not verbose



use std::{
	fmt,
	io::{
		self,
		Write,
	},
	ops::Range,
};

use owo_colors::{
	AnsiColors,
	OwoColorize,
	Stream,
};

use crate::ctx::{
	CompileContext,
	//File,
	Span,
};



/// Level of significance of a diagnostic.
#[derive(Clone, Copy, Debug, Eq, PartialEq)]
pub enum Level {
	/// Compiler log, information which only matters when explicitly asked for verbose output
	Log,
	/// Suggestions for resolving issues
	Help,
	/// Additional information for an issue
	Note,
	/// A warning, something not rejected but potentially erroneous
	Warn,
	/// An error, something either not recoverable for code generation or explicitly rejected as a harsh lint
	Error,
}

/// Re-export of [`std::num::NonZeroI32::new`] for convenience.
pub fn exit_code(num: i32) -> Option<std::num::NonZeroI32> {
	std::num::NonZeroI32::new(num)
}



/// Type witnessing that an error has been emitted, intended for the `Err` side
/// of a `Result` above the level where you wish to add additional context.
#[derive(Debug)]
pub struct ErrorEmitted(());

/// Result specification, where in the `Err` case an error has been emitted.
pub type Result<T> = std::result::Result<T, ErrorEmitted>;

/// A labeled place in code to be used in a diagnostic.
#[derive(Debug)]
struct DiagnosticAnnotation<'cctx> {
	label: Box<str>,
	span: Span<'cctx>,
}

/// Each part of a diagnostic corresponding to a single header and message.
#[derive(Debug)]
pub struct DiagnosticFrame<'cctx> {
	message: Box<str>,
	annotations: Vec<DiagnosticAnnotation<'cctx>>,
	notes: Vec<Box<str>>,
}

impl<'cctx> DiagnosticFrame<'cctx> {
	/// A new diagnostic frame with a given header message.
	pub fn new(message: Box<str>) -> Self {
		Self { message, annotations: Vec::new(), notes: Vec::new() }
	}

	/// Add an annotation pointing to a place in code.
	pub fn with_annotation(mut self, label: String, span: Span<'cctx>) -> Self {
		self.annotations.push(DiagnosticAnnotation {
			label: label.into_boxed_str(),
			span
		});
		self
	}

	/// Add a note for additional information.
	pub fn with_note(mut self, note: String) -> Self {
		self.notes.push(note.into_boxed_str());
		self
	}
}

/// Shortcut for [`DiagnosticFrame::new`] for use when building errors. Takes a
/// header message.
pub fn frame<'cctx>(message: String) -> DiagnosticFrame<'cctx> {
	DiagnosticFrame::new(message.into_boxed_str())
}

/// The format for errors, warnings, and such emitted by the compiler in
/// response to user input.
#[must_use = "if this is implicitly dropped, then it is not being emitted"]
#[derive(Debug)]
pub struct Diagnostic<'cctx> {
	level: Level,
	main_frame: DiagnosticFrame<'cctx>,
	help_frames: Vec<DiagnosticFrame<'cctx>>,
}

impl<'cctx> Diagnostic<'cctx> {
	/// Create a new diagnostic at the given level of importance and with at least
	/// one frame.
	pub fn new(level: Level, main_frame: DiagnosticFrame<'cctx>) -> Self {
		Self { level, main_frame, help_frames: Vec::new() }
	}

	/// Add a `Note` level frame after the ones currently present.
	pub fn with_note(mut self, frame: DiagnosticFrame<'cctx>) -> Self {
		self.help_frames.push(frame);
		self
	}

	/// Emit this error to the [`CompileContext`]. This does not mean the error
	/// will be immediately output but will assuredly at some point be output.
	pub fn emit(self, cctx: &'cctx CompileContext<'cctx>) -> ErrorEmitted {
		cctx.emit_diagnostic(self).unwrap()
	}

	/// Emit this error to the [`CompileContext`] and prematurely exit compilation.
	pub fn emit_fatal(
		self,
		cctx: &'cctx CompileContext<'cctx>,
		exit_code: Option<std::num::NonZeroI32>
	) -> ! {
		cctx.emit_diagnostic(self).unwrap();
		eprintln!();
		std::process::exit(match exit_code {
			Some(code) => code.get(),
			None => -1i32,
		})
	}
}

/// Create a log diagnostic with the given first frame.
pub fn log(main_frame: DiagnosticFrame) -> Diagnostic {
	Diagnostic::new(Level::Log, main_frame)
}
/// Create a warn diagnostic with the given first frame.
pub fn warn(main_frame: DiagnosticFrame) -> Diagnostic {
	Diagnostic::new(Level::Warn, main_frame)
}
/// Create a error diagnostic with the given first frame.
pub fn error(main_frame: DiagnosticFrame) -> Diagnostic {
	Diagnostic::new(Level::Error, main_frame)
}



/*
pub struct FileLog {
	file: std::fs::File,
}

impl FileLog {
	pub fn new<P: AsRef<std::path::Path>>(path: P) -> io::Result<Self> {
		Ok( Self {
			file: std::fs::OpenOptions::new().create(true).append(true).open(path)?,
		})
	}
}

impl Log for FileLog {
	fn write(&mut self, level: Level, msg: &str) -> io::Result<()> {
		use io::Write;

		let prefix = match level {
			Level::Log => "log",
			Level::Warn => "warning",
			Level::Error => "error",
		};

		writeln!(self.file, "{}: {}", prefix, msg)
	}
}
*/



/// A printable location for labeling the quoted text of an annotation.
#[derive(Clone, Copy, Debug, Eq, Ord, PartialEq, PartialOrd)]
pub struct Location<'a> {
	/// Name of the file to display
	pub file_name: &'a str,
	/// Line number by index, beginning from zero
	pub line_number: usize,
	/// Characters from the start of the line, beginning from zero
	pub chars_into_line: usize,
}

impl<'a> fmt::Display for Location<'a> {
	fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
		write!(f, "{}:{}:{}",
			self.file_name,
			self.line_number + 1,
			self.chars_into_line + 1
		)
	}
}



/// A log to output diagnostics.
#[doc(notable_trait)]
pub trait Log {
	/// Write a [`Diagnostic`] in full.
	fn write_diagnostic<'cctx>(
		&mut self,
		cctx: &'cctx CompileContext<'cctx>,
		diagnostic: Diagnostic<'cctx>,
	) -> io::Result<ErrorEmitted>;
}

/// [`Log`] which outputs directly to `stderr`, using box drawing and geometric
/// shape characters to prettify the output, assuming a monospace font.
pub struct VisualLog {
	stderr: io::Stderr,
}

impl Default for VisualLog {
	fn default() -> Self {
		Self::new()
	}
}

impl VisualLog {
	/// Create a new [`VisualLog`] with a new `stderr` handle.
	pub fn new() -> Self {
		Self {
			stderr: io::stderr(),
		}
	}

	/*
	pub fn write(&mut self, level: Level, msg: &str) -> io::Result<()> {
		let prefix = match level {
			Level::Log => "log",
			Level::Help => "help",
			Level::Note => "note",
			Level::Warn => "warning",
			Level::Error => "error",
		};

		writeln!(self.stderr, "{}{}{}",
			prefix.bold().if_supports_color(Stream::Stderr, |prefix| prefix.color( match level {
				Level::Log => AnsiColors::BrightMagenta,
				Level::Help => AnsiColors::BrightCyan,
				Level::Note => AnsiColors::BrightGreen,
				Level::Warn => AnsiColors::BrightYellow,
				Level::Error => AnsiColors::BrightRed,
			})),
			": ".bold(),
			msg.bold(),
		)
	}

	/// Log string at `Level::Error` urgency and then exit with given exit code.
	///
	/// If `exit_code` is left as `None`, then the generic `-1` will be used.
	pub fn fatal(&mut self, msg: &str, exit_code: Option<std::num::NonZeroI32>) -> ! {
		self.write(Level::Error, msg).unwrap();
		std::process::exit(match exit_code {
			Some(code) => code.get(),
			None => -1i32,
		})
	}
	*/

	/// Write the separation between diagnostics (currently just a newline).
	fn write_separator(&mut self) -> io::Result<()> {
		writeln!(self.stderr)
	}

	/// Write the header message of a diagnostic.
	fn write_message(&mut self, level: Level, message: &str) -> io::Result<()> {
		let (prefix, color) = match level {
			Level::Log => ("log", AnsiColors::BrightMagenta),
			Level::Help => ("help", AnsiColors::BrightCyan),
			Level::Note => ("note", AnsiColors::BrightGreen),
			Level::Warn => ("warning", AnsiColors::BrightYellow),
			Level::Error => ("error", AnsiColors::BrightRed),
		};

		writeln!(self.stderr, "{}{}{}",
			prefix.bold().if_supports_color(Stream::Stderr, |prefix| prefix.color(color)),
			": ".bold(),
			message.bold(),
		)?;

		Ok(())
	}

	/// Write a combined annotation given some additional information.
	fn write_annotated_segment(
		&mut self,
		location: Location,
		quote: &str,
		span: Range<usize>,
		annotation: &str,
		notes: &[Box<str>],
		is_last_frame: bool,
		is_last_segment: bool,
	) -> io::Result<()> {
		let is_final = is_last_frame && is_last_segment;

		let line_number = location.line_number + 1;
		let number_width = line_number.ilog10() + 1;
		let padding = " ".repeat(number_width as usize);

		writeln!(self.stderr, "{}{} {}",
			padding.bold().if_supports_color(Stream::Stderr, |pad| pad.color(AnsiColors::BrightBlue)),
			" ┍━▶".bold().if_supports_color(Stream::Stderr, |arrow| arrow.color(AnsiColors::BrightBlue)),
			location
		)?;
		writeln!(self.stderr, "{}{}",
			padding.bold().if_supports_color(Stream::Stderr, |pad| pad.color(AnsiColors::BrightBlue)),
			" │ ".bold().if_supports_color(Stream::Stderr, |arrow| arrow.color(AnsiColors::BrightBlue)),
		)?;

		writeln!(self.stderr, "{}{}{}",
			line_number.bold().if_supports_color(Stream::Stderr, |pad| pad.color(AnsiColors::BrightBlue)),
			" │ ".bold().if_supports_color(Stream::Stderr, |arrow| arrow.color(AnsiColors::BrightBlue)),
			quote.replace('\t', "    "),
		)?;
		let tab_offset = 3 * quote.chars().filter(|c| *c == '\t').count();
		writeln!(self.stderr, "{}{}{}{}{}{}",
			padding.bold().if_supports_color(Stream::Stderr, |pad| pad.color(AnsiColors::BrightBlue)),
			" │ ".bold().if_supports_color(Stream::Stderr, |arrow| arrow.color(AnsiColors::BrightBlue)),
			" ".repeat(span.start as usize + tab_offset),
			"^".repeat((span.end - span.start) as usize).bold()
				.if_supports_color(Stream::Stderr, |arrow| arrow.color(AnsiColors::BrightRed)),
			" ".bold().if_supports_color(Stream::Stderr, |arrow| arrow.color(AnsiColors::BrightRed)),
			annotation.bold().if_supports_color(Stream::Stderr, |arrow| arrow.color(AnsiColors::BrightRed)),
		)?;

		if !notes.is_empty() {
			writeln!(self.stderr, "{}{}",
				padding.bold().if_supports_color(Stream::Stderr, |pad| pad.color(AnsiColors::BrightBlue)),
				" │ ".bold().if_supports_color(Stream::Stderr, |arrow| arrow.color(AnsiColors::BrightBlue)),
			)?;
		}

		let mut notes = notes.iter().peekable();
		while let Some(note) = notes.next() {
			let indicator = if is_final && notes.peek().is_none() { " ╘═" } else { " ╞═" };
			writeln!(self.stderr, "{}{} {}: {}",
				padding.bold().if_supports_color(Stream::Stderr, |pad| pad.color(AnsiColors::BrightBlue)),
				indicator.bold().if_supports_color(Stream::Stderr, |arrow| arrow.color(AnsiColors::BrightBlue)),
				"note".bold(),
				note,
			)?;
		}

		/*
		if !is_final {
			writeln!(self.stderr, "{}{}",
				padding.bold().if_supports_color(Stream::Stderr, |pad| pad.color(AnsiColors::BrightBlue)),
				" ┊ ".bold().if_supports_color(Stream::Stderr, |arrow| arrow.color(AnsiColors::BrightBlue)),
			)?;
		}
		*/
		let fade = if is_final { " │ " } else { " ┊ " };
		writeln!(self.stderr, "{}{}",
			padding.bold().if_supports_color(Stream::Stderr, |pad| pad.color(AnsiColors::BrightBlue)),
			fade.bold().if_supports_color(Stream::Stderr, |arrow| arrow.color(AnsiColors::BrightBlue)),
		)?;

		Ok(())
	}

	/// Write a single [`DiagnosticFrame`]
	fn write_frame<'cctx>(
		&mut self,
		cctx: &'cctx CompileContext<'cctx>,
		level: Level,
		frame: &DiagnosticFrame<'cctx>,
		is_last_frame: bool,
	) -> io::Result<()> {
		self.write_message(level, &frame.message)?;
		let mut annotations = frame.annotations.iter().peekable();
		while let Some(annotation) = annotations.next() {
			let span = (annotation.span.range.start as usize)..(annotation.span.range.end as usize);
			cctx.with_file(annotation.span.file, |file| {
				let loc = file.location_from_range(annotation.span.range.clone());
				let quote_start = file.lines[loc.line_number] as usize;
				let quote_end = match file.lines.get(loc.line_number + 1) {
					Some(i) => *i as usize - 1,
					None => if file.string.ends_with('\n') {
						file.string.len() - 1
					} else {
						file.string.len()
					},
				};
				let subspan = (span.start - quote_start)..(span.end - quote_start);
				let is_last_segment = annotations.peek().is_none();
				self.write_annotated_segment(
					loc,
					&file.string[quote_start..quote_end],
					subspan,
					&annotation.label,
					if is_last_segment { &frame.notes } else { &[] },
					is_last_frame,
					is_last_segment,
				)
			})?;
		}

		Ok(())
	}
}

impl Log for VisualLog {
	fn write_diagnostic<'cctx>(
		&mut self,
		cctx: &'cctx CompileContext<'cctx>,
		diagnostic: Diagnostic<'cctx>,
	) -> io::Result<ErrorEmitted> {
		self.write_separator()?;

		self.write_frame(cctx, diagnostic.level, &diagnostic.main_frame, diagnostic.help_frames.is_empty())?;
		let mut help_frames = diagnostic.help_frames.iter().peekable();
		while let Some(frame) = help_frames.next() {
			self.write_frame(cctx, Level::Note, frame, help_frames.peek().is_none())?;
		}

		Ok(ErrorEmitted(()))
	}
}



/// [`Log`] which outputs directly to `stderr`, without particular concern for
/// the visual layout of the message.
pub struct TextualLog {
	stderr: io::Stderr,
}

impl Default for TextualLog {
	fn default() -> Self {
		Self::new()
	}
}

impl TextualLog {
	/// Create a new [`VisualLog`] with a new `stderr` handle.
	pub fn new() -> Self {
		Self { stderr: io::stderr() }
	}

	/// Write the header message of a diagnostic.
	fn write_message(&mut self, level: Level, message: &str) -> io::Result<()> {
		let (prefix, color) = match level {
			Level::Log => ("log", AnsiColors::BrightMagenta),
			Level::Help => ("help", AnsiColors::BrightCyan),
			Level::Note => ("note", AnsiColors::BrightGreen),
			Level::Warn => ("warning", AnsiColors::BrightYellow),
			Level::Error => ("error", AnsiColors::BrightRed),
		};

		writeln!(self.stderr, "{}{}{}",
			prefix.bold().if_supports_color(Stream::Stderr, |prefix| prefix.color(color)),
			": ".bold(),
			message.bold(),
		)?;

		Ok(())
	}

	fn write_frame<'cctx>(
		&mut self,
		cctx: &'cctx CompileContext<'cctx>,
		level: Level,
		frame: &DiagnosticFrame<'cctx>,
	) -> io::Result<()> {
		self.write_message(level, &frame.message)?;
		for annotation in frame.annotations.iter() {
			cctx.with_file(annotation.span.file, |file| {
				let loc = file.location_from_range(annotation.span.range.clone());
				writeln!(self.stderr, " at {}: {}", loc, annotation.label)
			})?;
		}
		for note in frame.notes.iter() {
			writeln!(self.stderr, " note: {}", note)?;
		}

		Ok(())
	}
}

impl Log for TextualLog {
	fn write_diagnostic<'cctx>(
		&mut self,
		cctx: &'cctx CompileContext<'cctx>,
		diagnostic: Diagnostic<'cctx>,
	) -> io::Result<ErrorEmitted> {
		self.write_frame(cctx, diagnostic.level, &diagnostic.main_frame)?;
		for frame in diagnostic.help_frames {
			self.write_frame(cctx, Level::Note, &frame)?;
		}

		Ok(ErrorEmitted(()))
	}
}

